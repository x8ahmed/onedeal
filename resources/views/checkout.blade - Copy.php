@extends('layouts.master')
@section('main')
    <!-- banner-2 -->
    <div class="page-head_agile_info_w3l">

    </div>
    <!-- //banner-2 -->
    <!-- page -->
    <div class="services-breadcrumb">
        <div class="agile_inner_breadcrumb">
            <div class="container">
                <ul class="w3_short">
                    <li>
                        <a href="index.html">الرئيسية</a>
                        <!--<a href="index.html">Home</a>-->
                        <i>|</i>
                    </li>
                    <li>الدفع</li>
                    <!--<li>Checkout</li>-->
                </ul>
            </div>
        </div>
    </div>
    <!-- //page -->
    <form action="{{route('user_checkout')}}" enctype="multipart/form-data" method="post">
        {{csrf_field()}}
    <!-- checkout page -->
    <div class="privacy py-sm-5 py-4">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <span>الدفع</span>
                <!--<span>C</span>heckout-->
            </h3>
            <!-- //tittle heading -->
            <div class="checkout-right">
                <!--<h4 class="mb-sm-4 mb-3">Your shopping cart contains:-->
                <h4 class="mb-sm-4 mb-3" id="products_number">سلة المشتريات تحتوى على:
                    <span>3 منتجات</span>
                    <!--<span>3 Products</span>-->
                </h4>
                <div class="table-responsive">
                    <table class="timetable_sub" id="products">
                        <thead>
                        <!--<tr>-->
                        <!--<th>SL No.</th>-->
                        <!--<th>Product</th>-->
                        <!--<th>Quality</th>-->
                        <!--<th>Product Name</th>-->

                        <!--<th>Price</th>-->
                        <!--<th>Remove</th>-->
                        <!--</tr>-->
                        <tr>
                            <th>اسم المنتج</th>
                            <th>الكمية</th>
                            <th>السعر</th>
                            <th>الاجمالى</th>
                            <th>النص المراد كتابته</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="3">الاجمالى</th>

                            <th id="total_footer">الاجمالى</th>
                            <th>النص المراد كتابته</th>
                        </tr>
                        </tfoot>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>وسيلة الشحن</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <select required class="form-control" name="shipping_type" id="shipping" style="margin-bottom: 10px">
                                    @php $shippings = \App\Shipping::where('parent_id',0)->get(); @endphp
                                    <option selected disabled="disabled" data-shipping-price="" value="">اختر وسيلة الشحن</option>

                                @foreach($shippings as $shipping)
                                    <option value="{{$shipping->id}}" data-shipping-price="{{$shipping->price}}">{{$shipping->type}}</option>
                                    @endforeach
                                </select>
                                <select class="form-control" name="shipping_cities" id="cities" style="display: none">
                                    @php $shippings = \App\Shipping::where('parent_id',5)->get(); @endphp
                                    @foreach($shippings as $shipping)
                                        <option value="{{$shipping->id}}" data-shipping-price="{{$shipping->price}}">{{$shipping->type}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <h4 id="shipping_price"></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>الضريبة</h4>
                    </div>
                    <div class="col-md-6">
                        <h4>
                            @php $tax = \App\Tax::first(); @endphp
                            {{$tax->tax}}%
                        </h4>

                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>الاجمالى بعد الضريبة</h4>
                    </div>
                    <div class="col-md-6" id="total_after_tax">
                        <h4></h4>
                        <input type="hidden" name="total_after_tax" id="total_after_tax_input">

                    </div>
                </div>

                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>كود الخصم</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="coupon" id="coupon_value">
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-primary" id="coupon_submit">تحقق</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;display: none">
                    <div class="col-md-6">
                        <h4>الاجمالى بعد الخصم</h4>
                    </div>
                    <div class="col-md-6" id="total_after_coupon">
                        <h4></h4>

                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>وسيلة الدفع</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <select required class="form-control" name="payment_method" id="payment" style="margin-bottom: 10px">
                                        <option selected disabled="disabled" value="">اختر وسيلة الدفع</option>
                                        <option value="cash">نقدا فى الفرع</option>
                                        <option value="transfer">تحويل لاحد حسابات المؤسسة</option>
                                </select>
                                <select class="form-control" id="accounts" style="display: none">
                                    <option selected disabled="disabled">اختر الحساب</option>
                                    @php $accounts = \App\BillingAccount::all(); @endphp
                                    @foreach($accounts as $account)
                                        <option value="{{$account->id}}">{{$account['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6" id="receipt" style="display: none;">
                                @foreach($accounts as $account)
                                <div id="{{$account->id}}" style="display: none;">
                                        <p>رقم الحساب</p>
                                        <p>{{$account->account_number}}</p>
                                </div>
                                @endforeach
                                <label>ارفق صورة الايصال</label>
                                <input type="file" name="receipt" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="checkout-left">
                <div class="address_form_agile mt-sm-5 mt-4">
                    <!--<h4 class="mb-sm-4 mb-3">Add a new Details</h4>-->
                        <div class="creditly-wrapper wthree, w3_agileits_wrapper">

                        </div>
                    <div class="checkout-right-basket">
                        <button type="submit" class="btn btn-primary">تأكيد الشراء
                            <!--<a href="payment.html">Make a Payment-->
                            <!--<span class="far fa-hand-point-right"></span>-->
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="s_text" value="{{\App\SuggestedText::all()}}">
    <!-- //checkout page -->

    </form>
@endsection
@section('script')
    <script>
        var items = paypals.minicarts.cart.items();
        var total = 0;
        var total_after_shipping = 0;
            $('#products tbody').empty();
        $('#products_number span').empty();
        $('#products_number span').append('<span>'+items.length+' منتجات</span>');
        for (var x =0;x<items.length;x++){
            console.log(items[x]);
            $('#products').append('<tr class="rem1">\n' +
                '                            <td class="invert">'+items[x]._data.item_name+'</td>\n' +
                '                            <td class="invert">\n' +
                '                                <div class="quantity">\n' +
                '                                    <div class="quantity-select">\n' +
                // '                                        <div class="entry value-minus">&nbsp;</div>\n' +
                '                                        <div>\n' +
                '                                            <span>'+items[x]._data.quantity+'</span>\n' +
                '                                        </div>\n' +
                // '                                        <div class="entry value-plus active">&nbsp;</div>\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '                            </td>\n' +
                '                            <td class="invert">'+items[x]._data.amount+'</td>\n' +
                '                            <td class="invert">'+items[x]._data.quantity*items[x]._data.amount+'</td>\n' +
                '                            <td class="invert">' +
                '<select required class="form-control suggested_text" name="suggested_text[]"><option selected disabled value="">نصوص مقترحة</option></select>' +
                '<textarea style="display: none" class="form-control" name="user_text[]" rows="3" id="comment"></textarea>' +
                '</td>\n' +

                '                        </tr>');
            total+=items[x]._data.quantity*items[x]._data.amount;
            $('#products').append('' +
                '        <input type="hidden" name="product_id[]" value="'+items[x]._data.id+'">\n' +
                '            <input type="hidden" name="quantity[]" value="'+items[x]._data.quantity+'">');
        }//end for

        $('#total_footer').text(total);
        $('#total_after_tax h4').text(total+total*Number("{{$tax->tax}}")/100);
        $('#total_after_tax_input').val(total+total*Number("{{$tax->tax}}")/100);
        var total_after_tax = total+total*Number("{{$tax->tax}}")/100;
        console.log(paypals.minicarts.cart.items());
        var text = JSON.parse($('#s_text').val());
        for (var x =0;x<text.length;x++){
            $('.suggested_text').append('<option>'+text[x].text+'</option>');
        }//end for
        $('.suggested_text').append('<option value="none">اخرى</option>');
        $('.suggested_text').change(function () {
            if($(this).val()=='none'){$(this).hide();jQuery(this).siblings('textarea').first().show();}
        });
        $('#coupon_submit').click(function () {
            $.ajax({
                url:"{{route('check_coupon')}}",
                data:{coupon:$('#coupon_value').val()},
                success:function (response) {
                    if(response.status){
                        if (response.data.type == 'price'){
                            $('#total_after_coupon').parent().show();
                            $('#coupon_value').addClass('border-success');
                            $('#coupon_value').addClass('text-success');
                            $('#coupon_value').attr('readonly',true);
                            if (total_after_shipping){
                                $('#total_after_coupon h4').text(total_after_shipping-response.data.value);

                            }else {
                                $('#total_after_coupon h4').text(total_after_tax-response.data.value);

                            }
                            //console.log(total_after_tax-response.data.value);
                        }else if(response.data.type == 'percentage'){
                            $('#total_after_coupon').parent().show();
                            $('#coupon_value').addClass('border-success');
                            $('#coupon_value').addClass('text-success');
                            $('#coupon_value').attr('readonly',true);
                            if (total_after_shipping){
                                $('#total_after_coupon h4').text(total_after_shipping-(response.data.value/100)*total_after_tax);

                            }else {
                                $('#total_after_coupon h4').text(total_after_tax-(response.data.value/100)*total_after_tax);

                            }
                        }
                    }else {
                        $('#coupon_value').css('border-color','red');
                        $('#coupon_value').css('color','red');
                    }
                }
            });
        });
        $('#shipping').change(function () {
            if ($('#shipping option:selected').val()==5){
                $('#cities').show();
                $('#shipping_price').text('سعر الشحن'+$('#cities option:selected').data('shipping-price')+' ريال');
                $('#total_after_tax h4').text(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
                $('#total_after_tax_input').val(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
                total_after_shipping=0;
                total_after_shipping+=  total_after_tax+Number($('#cities option:selected').data('shipping-price'));
            }else {
                $('#shipping_price').text('سعر الشحن'+$('#shipping option:selected').data('shipping-price')+' ريال');
                $('#cities').hide();
                $('#total_after_tax h4').text(total_after_tax+Number($('#shipping option:selected').data('shipping-price')));
                $('#total_after_tax_input').val(total_after_tax+Number($('#shipping option:selected').data('shipping-price')));
                total_after_shipping=0;
                total_after_shipping+=  total_after_tax+Number($('#shipping option:selected').data('shipping-price'));

            }
        });
        $('#cities').change(function () {
            $('#shipping_price').text('سعر الشحن'+$('#cities option:selected').data('shipping-price')+' ريال');
            $('#total_after_tax h4').text(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
            $('#total_after_tax_input').val(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
            total_after_shipping=0;
            total_after_shipping+=  total_after_tax+Number($('#cities option:selected').data('shipping-price'));

        });
        $('#payment').change(function () {
            if ($('#payment option:selected').val() == 'transfer'){
                $('#accounts').show();
                $('#receipt').show();
            }
        });
        $('#accounts').change(function () {
            $('#'+$('#accounts option:selected').val()).show();
        });
    </script>

    @endsection
