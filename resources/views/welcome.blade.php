@extends('hepta.layouts.master')
@section('content')
    @php
    $slides = \App\Slider::all();
    @endphp
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Slider Start -->
        <div id="rs-slider" class="rs-slider rs-slider-one">
            <div class="bend niceties">
                <div id="nivoSlider" class="slides">
                    @foreach($slides as $slide)
                    <img src="{{asset($slide->image_path)}}" alt="{{$slide->title}}" title="#{{$slide->id}}" />
                    @endforeach
                </div>
            @foreach($slides as $slide)

                <!-- Slide 1 -->
                <div id="{{$slide->id}}" class="slider-direction">
                    <div class="display-table">
                        <div class="display-table-cell">
                            <div class="container">
                                <div class="slider-des">
                                    <h3 class="sl-sub-title">{{$slide->sub_title}}</h3>
                                    <h1 class="sl-title">{{$slide->title}}</h1>
                                    <div class="sl-desc margin-0">
                                        {!! $slide->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <!-- Slider End -->


        <!-- Services Start -->
        <section id="rs-services" class="rs-services-3 rs-service-style1 sec-color pt-100 pb-70">
            <div class="container">
                <div class="row rs-vertical-middle">
                    <div class="col-lg-4 col-md-12 mb-md-50">
                        <div class="service-title">
                            <h3>
                                {{\App\Site_setting::where('name','services_head')->first()->value}}
                            </h3>
                            <p>
                                {{\App\Site_setting::where('name','services_sub_head')->first()->value}}
                            </p>
                            <a href="#" class="readon">View Services</a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12">
                        <div class="row">
                            @php $services = \App\Feature::where([
                            ['type','service'],['show',1]
                            ])->get(); @endphp
                            @for($x=0;$x<count($services);$x++)
                                @if($x%2==0&&$x==0)
                            <div class="col-md-6 col-sm-12">
                                @endif
                                @if($x%2==0&&$x!=0)
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                @endif

                                <div class="common">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="icon-part">
                                                {{--<i data-icon="&#xe001;" class="icon-part"></i>--}}
                                                <i class="{{$services[$x]->picture}}" aria-hidden="true" class="icon-part"></i>

                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="text">
                                                <a href="javascript:void(0)"><h4>{{$services[$x]->head}}</h4></a>
                                                <p>{!!  $services[$x]->description!!}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                      @if($x==count($services)-1)
                                        </div>
                                       @endif
                            @endfor
                        </div>
                    </div>
                </div>
                <!-- 4th -->

            </div>
        </section>
        <!-- Services End -->

        <!-- Portfolio Start -->
        <section id="rs-portfolio2" class="rs-portfolio2 defutl-style sec-spacer">
            <div class="container">
                <div class="sec-title">
                    <h3>Our Latest Project</h3>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="20" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="3" data-md-device-nav="true" data-md-device-dots="false">
                            @php $projects = \App\Feature::where([
                            ['type','project'],['show',1]
                            ])->get(); @endphp
                            @foreach($projects as $project)
                            <div class="portfolio-item">
                                <div class="portfolio-img">
                                    <img src="{{$project->image_path}}" alt="{{$project->head}}" />
                                </div>
                                <div class="portfolio-content">
                                    <div class="display-table">
                                        <div class="display-table-cell">
                                            <h4 class="p-title"><a href="{{route('single_project',$project->id)}}">{{$project->head}}</a></h4>
                                            <a href="{{route('single_project',$project->id)}}" class="project-btn">View Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Portfolio End -->

        <!-- Expertise Area satar -->
        <div class="why-choose-us defult-style sec-color pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 mb-md-50">
                        <div class="video-section-area">
                            <div class="image-here">
                                <img src="{{asset(\App\Site_setting::where('name','why_choose_us_picture')->first()->value)}}" alt="">
                                <div class="video-icon-here">
                                    <a class="popup-videos animated pulse" href="{{\App\Site_setting::where('name','why_choose_us_video')->first()->value}}" title="Video Icon">
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                            <div class="overly-border"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 services-responsibiity">
                        <div class="service-res-inner">
                            <div class="sec-title">
                                <h3>{{\App\Site_setting::where('name','why_choose_us')->first()->value}}</h3>
                            </div>
                            @php $whys = \App\Feature::where([
                            ['type','why_choose_us'],['show','1']
                            ])->get(); @endphp
                            @foreach($whys as $why)
                            <div class="services-item">
                                <div class="service-mid-icon">
                                    <a href="#">
                                        <span class="service-mid-icon-container">
                                            <i class="{{$why->picture}}" aria-hidden="true" style="line-height: unset"></i>
                                        </span></a>
                                </div>
                                <div class="services-desc">
                                    <h3 class="services-title"><a href="javascript:void (0)">{{$why->head}}</a></h3>
                                    <p>{!! $why->description !!}</p>
                                </div>
                            </div>
                                @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Expertise Area end -->

        <!-- Team Start -->
        <section id="rs-team" class="rs-defult-team defult-style sec-spacer">
            <!-- Counter Up Section Start Here-->
            <div class="counter-top-area defult-style">
                <div class="container">
                    <div class="row rs-count">
                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration=".3s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="W" class="icon"></i>
                                <h3 class="rs-counter">{{\App\Site_setting::where('name','clients_no')->first()->value}}</h3>
                                <h4>Happy Client</h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->

                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6  wow fadeInUp" data-wow-duration=".7s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="C" class="icon"></i>
                                <h3 class="rs-counter">{{\App\Site_setting::where('name','projects_done')->first()->value}}</h3>
                                <h4>Project Done </h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->

                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration=".9s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="P" class="icon"></i>
                                <h3 class="rs-counter">{{\App\Site_setting::where('name','awards')->first()->value}}</h3>
                                <h4>Awards Won</h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->

                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="&#xe001;" class="icon"></i>
                                <h3 class="rs-counter">{{\App\Site_setting::where('name','exp')->first()->value}}</h3>
                                <h4 class="last">Experience Year</h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->
                    </div>
                </div>
            </div>
            <!-- Counter Down Section End Here-->
            <div class="container">
                <div class="sec-title">
                    <h3>Our Expert Advisor</h3>
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="3" data-md-device-nav="true" data-md-device-dots="false">
                  @php $teams = \App\Testmonial::where('type','team')->get(); @endphp
                  @foreach($teams as $team)
                  <!--team item start -->
                    <div class="team-item">
                        <div class="team-overlay">
                            <div class="team-img">
                                <img src="{{$team->image_path}}" alt="team Image" />
                            </div>
                            {{--<div class="team-content">--}}
                                {{--<ul class="team-social">--}}
                                    {{--<li><a href="#"><i class="fa fa-facebook"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-twitter"></i></a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-linkedin"></i></a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        </div>
                        <div class="team-info">
                            <a href="javascript:void (0)"><h4 class="title">{{$team->name}}</h4></a>
                            <span class="post">{{$team->title}}</span>
                        </div>
                    </div>
                    <!--team item end -->
                  @endforeach

                </div>
            </div>
        </section>
        <!-- Team end -->

        <!-- customer Start -->
        <section id="rs-customer" class="rs-defult-customer rs-customer3 gray-color">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6 col-sm-12 mb-sm-50">
                        <!-- Contact Start -->
                        <section id="rs-contact" class="rs-contact">
                            <div class="contact-bg">
                                <div class="contact-form">
                                    <div class="sec-title">
                                        <h3 class="contact-title">Need a Quick Query</h3>
                                    </div>
                                    {{--<div id="form-messages"></div>--}}
                                    <form  method="post" action="{{route('save_query')}}">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-field">
                                                    <input type="text" placeholder="Name" id="name" name="name" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-field">
                                                    <input type="email" placeholder="E-Mail" id="email" name="email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-field">
                                                    <input type="text" placeholder="Phone Number" id="phone_number" name="phone" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-field">
                                            <textarea placeholder="Your Message Here" rows="2" id="message" name="message" required></textarea>
                                        </div>
                                        <div class="form-button text-left">
                                            <button type="submit" class="readon">Submit Now</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                        <!-- Contact End -->
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="rs-carousel owl-carousel" data-loop="true" data-items="1" data-margin="30" data-autoplay="true" data-autoplay-timeout="7000" data-smart-speed="2000" data-dots="true" data-nav="false" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="1" data-md-device-nav="false" data-md-device-dots="true">
                        @php $teams = \App\Testmonial::where('type','testimonial')->get(); @endphp
                        @foreach($teams as $team)
                            <!-- Style 1 Start -->
                            <div class="customer-item">
                                <div class="customer-part">
                                    <div class="item-img">
                                        <img src="{{$team->image_path}}" alt="">
                                    </div>
                                    <div class="item-details">
                                        <p>{{$team->message}}</p>
                                    </div>
                                    <div class="item-author">
                                        <div class="item-name">
                                            {{$team->name}}
                                        </div>
                                        <div class="item-designation">{{$team->title}}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- Style 1 End -->
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- customar end -->

        <!-- Blog Start -->
        <section id="rs-blog" class="rs-blog sec-spacer">
            <div class="container">
                <div class="sec-title">
                    <h3>Latest News</h3>
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="3" data-md-device-nav="true" data-md-device-dots="false">
                    @php
                        $blogs = \App\Blog::all();
                    function strWordCut($string,$length)
                            {
                                $str_len = strlen($string);
                                $string = strip_tags($string);

                                if ($str_len > $length) {

                                    // truncate string
                                    $stringCut = substr($string, 0, $length-15);
                                    $string = $stringCut.'.....'.substr($string, $str_len-10, $str_len-1);
                                }
                                return $string;
                            }
                    @endphp
                    @foreach($blogs as $blog)
                    <div class="blog-item">
                        <div class="blog-img">
                            <img src="{{$blog->image_path}}" alt="Blog Image">
                            <div class="blog-img-content">
                                <div class="display-table">
                                    <div class="display-table-cell">
                                        <a class="blog-link" href="{{route('single_post',$blog->id)}}" title="Blog Link">
                                            <i class="fa fa-link"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-wrapper">
                            <div class="blog-meta">
                                <ul>
                                    {{--<li><i class="fa fa-user"></i><span>Admin</span></li>--}}
                                    <li><i class="fa fa-calendar"></i><span>{{$blog->dateOfPublich}}</span></li>
                                </ul>
                            </div>
                            <div class="blog-desc">
                                <a href="#">{{$blog->head}}</a>
                                <p>{!! strWordCut($blog->body,200) !!}</p>
                            </div>
                            <a href="{{route('single_post',$blog->id)}}" class="readon">Read More</a>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </section>
        <!-- Blog End -->

        <!-- Partner Start -->
        <div id="rs-defult-partner" class="rs-defult-partner sec-color pt-100 pb-100">
            <div class="container">
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="5" data-margin="30" data-autoplay="true" data-autoplay-timeout="8000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="2" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="5" data-md-device-nav="true" data-md-device-dots="false">
                    @php $clients = \App\Client::all(); @endphp
                    @foreach($clients as $client)
                    <div class="partner-item">
                        <a href="javascript:void (0)"><img src="{{$client->image_path}}" alt="Partner Image"></a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
        <!-- Partner End -->
    </div>

@endsection
