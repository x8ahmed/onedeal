@extends('layouts.master')
@section('main')
    {{--<!-- banner-2 -->--}}
    {{--<div class="page-head_agile_info_w3l">--}}

    {{--</div>--}}
    {{--<!-- //banner-2 -->--}}
    {{--<!-- page -->--}}
    {{--<div class="services-breadcrumb">--}}
        {{--<div class="agile_inner_breadcrumb">--}}
            {{--<div class="container">--}}
                {{--<ul class="w3_short">--}}
                    {{--<li>--}}
                        {{--<a href="index.html">الرئيسية</a>--}}
                        {{--<i>|</i>--}}
                    {{--</li>--}}
                    {{--<li>{{$product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- //page -->--}}

    <!-- Single Page -->
    <div class="banner-bootom-w3-agileits py-5">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <!--<span>S</span>ingle-->
                <!--<span>P</span>age</h3>-->
                <span>{{$product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</span>
                <!-- //tittle heading -->
                <div class="row">
                    <div class="col-5  single-right-left ">
                        <div class="grid images_3_of_2">
                            <div class="flexslider">
                                <ul class="slides">
                                    @php $product_pictures = \App\ProductPicture::where('product_id',$product->id)->get(); @endphp
                                    @foreach($product_pictures as $product_picture)
                                    <li data-thumb="{{asset($product_picture->picture)}}">
                                        <div class="thumb-image">
                                            {{--<div class="th-item" style="background-image: url({{asset($product_picture->picture)}})">--}}

                                            {{--</div>--}}
                                            <img src="{{asset($product_picture->picture)}}" data-imagezoom="true" class="img-fluid" alt="">
                                        </div>
                                    </li>
                                    @endforeach

                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-7 single-right-left simpleCart_shelfItem">
                        {{--<h3 class="mb-3">--}}
                            {{--لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية )  </h3>--}}
                        <!--<h3 class="mb-3">Samsung Galaxy J7 Prime (Gold, 16 GB) (3 GB RAM)</h3>-->
                        <p class="mb-3">
                            <span class="item_price">{{$product->price.' '.trans('index.price')}}</span>
                            <!--<del class="mx-2 font-weight-light">$280.00</del>-->
                            {{--<label>شحن مجانى</label>--}}
                        </p>
                        <div class="single-infoagile">
              {!! $product['description_'.\Illuminate\Support\Facades\Lang::getLocale()] !!}
                        </div>
                        {{--<div class="product-single-w3l">--}}
                            {{--<p class="my-3">--}}
                                {{--<i class="far fa-hand-point-left mr-2"></i>--}}
                                {{--ضمان سنة ضد عيوب الصناعة</p>--}}
                            {{--<!--<label>1 Year</label>Manufacturer Warranty</p>-->--}}
                            {{--<ul>--}}
                                {{--<li class="mb-1">--}}
                                    {{--3 GB RAM | 16 GB ROM | Expandable Upto 256 GB--}}
                                {{--</li>--}}
                                {{--<li class="mb-1">--}}
                                    {{--5.5 inch Full HD Display--}}
                                {{--</li>--}}
                                {{--<li class="mb-1">--}}
                                    {{--13MP Rear Camera | 8MP Front Camera--}}
                                {{--</li>--}}
                                {{--<li class="mb-1">--}}
                                    {{--3300 mAh Battery--}}
                                {{--</li>--}}
                                {{--<li class="mb-1">--}}
                                    {{--Exynos 7870 Octa Core 1.6GHz Processor--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                            {{--<p class="my-sm-4 my-3">--}}
                                {{--<i class="fas fa-retweet mr-3"></i>Net banking & Credit/ Debit/ ATM card--}}
                            {{--</p>--}}
                        {{--</div>--}}
                        <div class="occasion-cart">
                            <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                <form action="#" method="post">
                                    <fieldset id="{{$product->id}}">
                                        <input type="hidden" name="cmd" value="_cart" />
                                        <input type="hidden" name="add" value="1" />
                                        <input type="hidden" name="business" value=" " />
                                        <input type="hidden" name="id" value="{{$product->id}}" />
                                        <input type="hidden" name="item_name" value="{{$product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}" />
                                        <input type="hidden" name="amount" value="{{$product->price}}" />
                                        <input type="hidden" name="discount_amount" value="0.00" />
                                        <input type="hidden" name="currency_code" value="SAR" />
                                        <input type="hidden" name="return" value=" " />
                                        <input type="hidden" name="cancel_return" value=" " />
                                        <input type="submit" name="submit" value="اضف إلى العربة" class="btn btn-primary"/>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!-- //Single Page -->
@endsection
@section('script')
    <!-- imagezoom -->
    <script src="{{asset('site/js/imagezoom.js')}}"></script>
    <!-- //imagezoom -->

    

    <script src="{{asset('site/js/jquery.flexslider.js')}}"></script>
    <script>
        // Can also be used with $(document).ready()
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });
    </script>
    <!-- //FlexSlider-->
    @endsection