@extends('layouts.master')
@section('main')
    <form action="{{route('user_checkout')}}" enctype="multipart/form-data" method="post">
        {{csrf_field()}}
    <div class="container">
        <header class="page-header">
            <h1 class="page-title">{{trans('index.shopping_cart')}}</h1>
        </header>
        <input type="hidden" id="s_text" value="{{\App\Category::where('cat_type','text')->get()}}">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table table-shopping-cart">
                    <thead>
                    <tr>
                        <th>{{trans('index.chechout_product')}}</th>
                        <th>{{trans('index.chechout_title')}}</th>
                        <th>{{trans('index.chechout_price')}}</th>
                        <th>{{trans('index.chechout_quantity')}}</th>
                        <th>{{trans('index.chechout_suggested_text')}}</th>
                        <th>{{trans('index.chechout_notes')}}</th>
                        <th>{{trans('index.chechout_attachements')}}</th>
                        <th>{{trans('index.chechout_total')}}</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
                <div class="gap gap-small"></div>
            </div>
            <div class="col-md-12">
                <ul class="shopping-cart-total-list">
                    <li>
                        <span>{{trans('index.shipping')}}</span>
                        <span>
                                <select required class="form-control" name="shipping_type" id="shipping" style="margin-bottom: 10px">
                                    @php $shippings = \App\Shipping::where('parent_id',0)->get(); @endphp
                                    <option selected disabled="disabled" data-shipping-price="" value="">اختر وسيلة الشحن</option>

                                    @foreach($shippings as $shipping)
                                        <option value="{{$shipping->id}}" data-shipping-price="{{$shipping->price}}">{{$shipping->type}}</option>
                                    @endforeach
                                </select>
                                <select class="form-control" name="shipping_cities" id="cities" style="display: none">
                                    @php $shippings = \App\Shipping::where('parent_id',5)->get(); @endphp
                                                                @foreach($shippings as $shipping)
                                                                    <option value="{{$shipping->id}}" data-shipping-price="{{$shipping->price}}">{{$shipping->type}}</option>
                                                                @endforeach
                                </select>
                        </span>
                        <span>{{trans('index.price2')}}: <span id="shipping_price"></span></span>
                    </li>

                    <li>
                        @php $tax = \App\Tax::first(); @endphp
                        <span>{{trans('index.tax')}}</span>
                        <span>{{$tax->tax}}%</span>
                        <input type="hidden" value="{{$tax->tax}}" id="tax">
                    </li>
                    <li>
                        <span>{{trans('index.coupon')}}</span>
                        <span><input type="text" class="form-control" name="coupon" id="coupon_value"></span>
                        <span><button type="button" class="btn btn-primary" id="coupon_submit">{{trans('index.apply')}}</button></span>
                    </li>
                    <li>
                        <span>{{trans('index.payment_method')}}</span>
                        <span>
                               <select required class="form-control" name="payment_method" id="payment" style="margin-bottom: 10px">
                                        <option selected disabled="disabled" value="">اختر وسيلة الدفع</option>
                                        <option value="cash">نقدا فى الفرع</option>
                                        <option value="transfer">تحويل لاحد حسابات المؤسسة</option>
                                </select>
<div id="accounts_holder" style="display: none">
                                    <select class="form-control selectpicker" id="accounts" style="display: none">
                                    <option selected disabled="disabled">اختر الحساب</option>
                                        @php $accounts = \App\BillingAccount::all(); @endphp
                                        @foreach($accounts as $account)
                                            <option value="{{$account->id}}" data-thumbnail="{{asset($account->logo)}}">{{$account['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</option>
                                        @endforeach
                                </select>
</div>
                        </span>
                        <span>
                            <div class="col-md-12" id="receipt" style="display: none">

                                @foreach($accounts as $account)
                                    <div class="account_info" id="{{$account->id}}" style="display: none;">
                                        <p>رقم الحساب</p>
                                        <p>{{$account->account_number}}</p>
                                    </div>
                                @endforeach
                                <label>ارفق صورة الايصال</label>
                                <input type="file" name="receipt" class="form-control">
                            </div>
                        </span>
                    </li>
                    <li>
                        <span>{{trans('index.other_notes')}}</span>
                        <span><textarea class="form-control" rows="5" name="other_notes">{{trans('index.other_notes_description')}}</textarea></span>
                    </li>
                    <li><span>{{trans('index.chechout_total')}}</span><span id="total"></span>
                    </li>
                </ul><input type="submit" class="btn btn-primary" value="{{trans('index.chechout_checkout')}}">
            </div>
        </div>

    </div>
    <div class="gap"></div>
    <input type="hidden" id="products_total">
    <input type="hidden" id="total_after_tax" name="total_after_tax">
    </form>
    <!-- modal-->
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{trans('index.choosed_text')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="" id="selected_text_img" class="img-responsive">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('index.close')}}</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>
    <!-- modal-->


@endsection
@section('script')
    <script src="https://thdoan.github.io/bootstrap-select/js/bootstrap-select.js"></script>
    <script>

        var text = JSON.parse($('#s_text').val());
        @php $suggested_text_cats = \App\Category::where([['cat_type','text'],['parent_id','!=',0]])->get(); @endphp
        @php $suggested_text_parentcats = \App\Category::where([['cat_type','text'],['parent_id','=',0]])->get(); @endphp
        @foreach($suggested_text_cats as $suggested_text_cat)
                @php $cat_products = \App\SuggestedText::where('category_id',$suggested_text_cat->id)->get(); @endphp
        $('.suggested_text_td').append('<div class="s_cat s_cat_{{$suggested_text_cat->id}}" style="display:none;margin-top:5px"><select name="suggested_text[]" class="suggested_text selectpicker">\n');
        @foreach($cat_products as $cat_product)
                $('.suggested_text_td .suggested_text').append('' +
            '        <option style="color: white" data-thumbnail="{{asset($cat_product->text)}}" value="{{$cat_product->id}}">{{$cat_product->id}}</option>\n' +
            '    ');
        @endforeach
        $('.suggested_text_td').append('    </select></div>');
        console.log('{{$suggested_text_cat->name_ar}}');
        @endforeach

        @foreach($suggested_text_parentcats as $suggested_text_parentcat)
        @php $suggested_text_parentcat_childs = \App\Category::where([['cat_type','text'],['parent_id','=',$suggested_text_parentcat->id]])->get(); @endphp

        @foreach($suggested_text_parentcat_childs as $suggested_text_parentcat_child)
        $('.suggested_text_cats').append('<option value="{{$suggested_text_parentcat_child->id}}">{{$suggested_text_parentcat_child['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</option>');
        @endforeach
        // $('.suggested_text_cats').append('</optgroup>');

        console.log('{{$suggested_text_cat->name_ar}}');
                @endforeach
                $('.suggested_text_cats').change(function () {
                    console.log($('.suggested_text_cats option:selected').val());
                    $(this).siblings( ".s_cat").hide();
                    $(this).siblings( ".s_cat").children('select').attr('name','tttttttttttttttttt');

                    $(this).siblings( ".s_cat_"+$('.suggested_text_cats option:selected').val()).show();
                    $(this).siblings( ".s_cat_"+$('.suggested_text_cats option:selected').val()).children('select').attr('name','suggested_text[]');
                });

        {{--for (var x =0;x<text.length;x++){--}}
            {{--console.log(text[x]);--}}
            {{--$('.suggested_text_cats').append('<option style="color:white" data-thumbnail="{{asset("")}}'+text[x].text+'" value="'+text[x].id+'">'+text[x].id+'</option>');--}}
        {{--}//end for--}}
        $('.suggested_text').append('<option value="none">اخرى</option>');
        $('.suggested_text').change(function () {
            console.log($(this).parent('.s_cat'));
            if($(this).val()=='none'){$(this).parent('.s_cat').hide();$(this).parent('.s_cat').siblings('input[type="file"]').first().show();}
        });
        $('.suggested_text').change(function () {
            console.log($('.suggested_text option:selected').attr('data-thumbnail'));
            var src_img = $('.suggested_text option:selected').attr('data-thumbnail');
            $('#selected_text_img').attr('src',src_img);
            $('#myModal').modal();

        });


        $(document).ready(function () {
            $('.suggested_text .dropdown-toggle, .suggested_text .dropdown-menu').on('click', function (e) {
                e.stopPropagation();
            });

            $('.suggested_text .dropdown-toggle').click(function () {
                $('.suggested_text .dropdown-menu').toggleClass('active');
            });
        });

        // $('body').click(function () {
        //     $('.suggested_text .dropdown-menu').removeClass('active');
        // });
    </script>

    <script>
        var total = 0;
        var coupon = 0;
        var tax = Number($('#tax').val());

        var total_after_shipping = 0;
        var products_total = Number($('#products_total').val());
        $('#total').text(products_total);
        $('#total_footer').text(total);
        $('#total_after_tax h4').text(total+total*Number("{{$tax->tax}}")/100);
        $('#total_after_tax_input').val(total+total*Number("{{$tax->tax}}")/100);
        var total_after_tax = total+total*Number("{{$tax->tax}}")/100;

        $('#coupon_submit').click(function () {
            $.ajax({
                url:"{{route('check_coupon')}}",
                data:{coupon:$('#coupon_value').val()},
                success:function (response) {
                    if(response.status){
                        if (response.data.type == 'price'){
                            $('#total_after_coupon').parent().show();
                            $('#coupon_value').addClass('border-success');
                            $('#coupon_value').addClass('text-success');
                            $('#coupon_value').attr('readonly',true);
                            if (total_after_shipping){
                                $('#total_after_coupon h4').text(total_after_shipping-response.data.value);

                            }else {
                                $('#total_after_coupon h4').text(total_after_tax-response.data.value);

                            }
                            //console.log(total_after_tax-response.data.value);
                        }else if(response.data.type == 'percentage'){
                            $('#total_after_coupon').parent().show();
                            $('#coupon_value').addClass('border-success');
                            $('#coupon_value').addClass('text-success');
                            $('#coupon_value').attr('readonly',true);
                            if (total_after_shipping){
                                $('#total_after_coupon h4').text(total_after_shipping-(response.data.value/100)*total_after_tax);

                            }else {
                                $('#total_after_coupon h4').text(total_after_tax-(response.data.value/100)*total_after_tax);

                            }
                        }
                    }else {
                        $('#coupon_value').css('border-color','red');
                        $('#coupon_value').css('color','red');
                    }
                }
            });
        });

        $('#shipping').change(function () {
            if ($('#shipping option:selected').val()==5){
                $('#cities').show();
                $('#shipping_price').text($('#cities option:selected').data('shipping-price'));
                $('#total_after_tax h4').text(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
                $('#total_after_tax_input').val(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
                total_after_shipping=0;
                total_after_shipping+=  total_after_tax+Number($('#cities option:selected').data('shipping-price'));
                $('#total').text(products_total+Number($('#cities option:selected').data('shipping-price')));
                $('#total_after_tax').val(products_total+Number($('#cities option:selected').data('shipping-price'))+tax);
                total_after_tax = 0;
                total_after_tax = products_total+Number($('#cities option:selected').data('shipping-price'))+tax;
                $('#total').text(total_after_tax);

            }else {
                $('#shipping_price').text($('#shipping option:selected').data('shipping-price'));
                $('#cities').hide();
                $('#total_after_tax h4').text(total_after_tax+Number($('#shipping option:selected').data('shipping-price')));
                $('#total_after_tax_input').val(total_after_tax+Number($('#shipping option:selected').data('shipping-price')));
                total_after_shipping=0;
                total_after_shipping+=  total_after_tax+Number($('#shipping option:selected').data('shipping-price'));
                $('#total').text(products_total+Number($('#shipping option:selected').data('shipping-price')));
                $('#total_after_tax').val(products_total+Number($('#shipping option:selected').data('shipping-price'))+tax);
                total_after_tax = 0;
                total_after_tax = products_total+Number($('#shipping option:selected').data('shipping-price'))+tax;
                $('#total').text(total_after_tax);

            }
        });
        $('#cities').change(function () {
            $('#shipping_price').text($('#cities option:selected').data('shipping-price'));
            $('#total_after_tax h4').text(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
            $('#total_after_tax_input').val(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
            total_after_shipping=0;
            total_after_shipping+=  total_after_tax+Number($('#cities option:selected').data('shipping-price'));
            $('#total').text(products_total+Number($('#cities option:selected').data('shipping-price')));
            $('#total_after_tax').val(products_total+Number($('#cities option:selected').data('shipping-price'))+tax);
            total_after_tax = 0;
            total_after_tax = products_total+Number($('#cities option:selected').data('shipping-price'))+tax;
            $('#total').text(total_after_tax);
        });
        $('#payment').change(function () {
            if ($('#payment option:selected').val() == 'transfer'){
                $('#accounts_holder').show();
                // $('#accounts').show();
                $('#receipt').show();
            }else {
                $('#accounts_holder').hide();
                // $('#accounts').show();
                $('#receipt').hide();
            }
        });
        $('#accounts').change(function () {
            $('.account_info').hide();
            $('#'+$('#accounts option:selected').val()).show();
        });
        console.log(window.location.href.split('/')[window.location.href.split('/').length-1]);

    </script>

@endsection
@section('style')
    <link rel="stylesheet" href="https://thdoan.github.io/bootstrap-select/css/bootstrap-select.css">
@endsection