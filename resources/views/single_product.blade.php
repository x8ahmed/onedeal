@extends('layouts.master')
@section('main')
    @php $product_pictures =   isset($product)? \App\ProductPicture::where('product_id',$product->id)->get() : [0=>['picture'=>$text->text]] ; @endphp

    @php $product = isset($product)?$product:$text; @endphp


    <div class="container">
        <header class="page-header">
            <ol class="breadcrumb page-breadcrumb">
                <li>
                    <a href="{{route('index')}}">{{trans('home')}}</a>
                </li>
                <li><a href="{{route('tags',str_replace(' ','-',\App\Category::where('id',\App\Category::find($product->category_id)->parent_id)->first()['name_'.\Illuminate\Support\Facades\Lang::getLocale()]))}}">{{\App\Category::where('id',\App\Category::find($product->category_id)->parent_id)->first()['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</a>
                </li>
                <li><a href="{{route('tags',str_replace(' ','-',\App\Category::find($product->category_id)['name_'.\Illuminate\Support\Facades\Lang::getLocale()]))}}">{{\App\Category::find($product->category_id)['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</a>
                </li>
                <li class="active">{{$product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</li>
            </ol>
        </header>

        <div class="row">
            <div class="col-md-6">
                <div class="product-page-product-wrap jqzoom-stage jqzoom-stage-lg">
                    <div class="clearfix">
                        <a href="{{asset($product_pictures[0]['picture'])}}" id="jqzoom" data-rel="gal-1">
                            <img src="{{asset($product_pictures[0]['picture'])}}" alt="Image Alternative text" title="Image Title" />
                        </a>
                    </div>
                </div>
                <ul class="jqzoom-list">
                    @foreach($product_pictures as $product_picture)
                    <li>
                        <a class="{{($loop->first)?'zoomThumbActive':''}}" href="javascript:void(0)" data-rel="{gallery:'gal-1', smallimage: '{{asset($product_picture['picture'])}}', largeimage: '{{asset($product_picture['picture'])}}'}">
                            <img src="{{asset($product_picture['picture'])}}" alt="Image Alternative text" title="Image Title" />
                        </a>
                    </li>
                    @endforeach

                </ul>
            </div>
            @if(!isset($text))
            <div class="col-md-6">
                <div class="_box-highlight">

                    <h1>{{$product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</h1>
                    <p class="product-page-price">{{$product->price.trans('index.price')}}</p>
                    {{--<p class="text-muted text-sm">Free Shipping</p>--}}
                    <p class="product-page-desc-lg">{!! $product['description_'.\Illuminate\Support\Facades\Lang::getLocale()] !!}</p>

                    <ul class="product-page-actions-list">
                        @if($product->quantity !=0)
                            <button class="btn btn-lg btn-primary my-cart-btn"
                                    data-id="{{$product->id}}" data-name="{{$product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}"

                                    data-price="{{$product->price}}" data-quantity="1" data-image="{{asset($product->thumbnail)}}">
                                {{trans('index.add_to_cart')}}
                            </button>
                            @else
                            <button type="button" class="btn btn-lg btn-danger">{{trans('index.out_of_stock')}}</button>
                        @endif


                        {{--<li><a class="btn btn-lg btn-primary" href="#"><i class="fa fa-shopping-cart"></i>Add to Cart</a>--}}
                        {{--</li>--}}

                    </ul>
                    <div class="gap gap-small"></div>
                </div>
            </div>
            @endif
        </div>
        <div class="gap"></div>
        @if(!isset($text))
        <h3 class="widget-title">{{trans('index.similar_products')}}</h3>
        @php $similar_products = \App\Product::where('category_id',$product->category_id)->orderBy('id','desc')->take(12)->get() @endphp
        <div class="row">
            @foreach($similar_products as $similar_product)
            <div class="col-md-3">
                <div class="product product-sm-left ">
                    <ul class="product-labels"></ul>
                    <div class="product-img-wrap">
                        <img class="product-img" src="{{asset($similar_product->thumbnail)}}" alt="Image Alternative text" title="Image Title" />
                    </div>
                    <a class="product-link" href="{{route('single_product',$similar_product->id)}}"></a>
                    <div class="product-caption">

                        <h5 class="product-caption-title">{{$similar_product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</h5>
                        <div class="product-caption-price"><span class="product-caption-price-new">{{$similar_product->price.trans('index.price')}}</span>
                        </div>

                    </div>
                </div>
            </div>
            @endforeach

        </div>
            @endif

    </div>
    <div class="gap"></div>

@endsection
@section('script')


    @endsection