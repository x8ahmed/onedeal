@extends('layouts.master')
@section('main')


    <!-- checkout page -->
        <div class="privacy orderss py-sm-5 py-4">
            <div class="container py-xl-4 py-lg-2">
                <!-- tittle heading -->
                <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                    طلباتى
                    <!--<span>C</span>heckout-->
                </h3>
                <!-- //tittle heading -->
                <div class="checkout-right">
                    <!--<h4 class="mb-sm-4 mb-3">Your shopping cart contains:-->

                    <div class="table-responsive">
                        <table class="table timetable_sub table-bordered" id="products">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>حالة الطلب</th>
                                <th>وسيلة الشحن</th>
                                <th>وسيلة الدفع</th>
                                <th>صورة الايصال</th>
                                <th>الاجمالى</th>
                                <th colspan="4">المنتجات</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>اسم المنتج</th>
                                <th>الكمية</th>
                                <th>النص المطلوب</th>
                                <th>ملاحظات</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($orders as $order)
                                @php $order_products = \App\OrderProduct::where('order_id',$order->id)->get(); @endphp
                                @foreach($order_products as $order_product)
                                    <tr>
                                        @if($loop->first)
                                            <td rowspan="{{count($order_products)}}">{{$order->id}}</td>
                                            <td rowspan="{{count($order_products)}}">
                                                @if($order->status == 0)
                                                    {{'قيد المعالجة'}}
                                                @elseif($order->status == 1)
                                                    {{'جارى الشحن'}}
                                                @elseif($order->status == 2)
                                                    {{'تم الاستلام'}}
                                                @endif
                                            </td>
                                            <td rowspan="{{count($order_products)}}">{{\App\Shipping::find($order->shipping_type)->type}}</td>
                                            <td rowspan="{{count($order_products)}}">{{$order->payment_method}}</td>
                                            <td rowspan="{{count($order_products)}}">{!! ($order->receipt)?'<a href="'.asset($order->receipt).'" target="_blank">صورة الايصال</a>':'لا يوجد' !!}  </td>
                                            <td rowspan="{{count($order_products)}}">{{$order->total_after_coupon}}</td>
                                        @endif
                                        <td>{{\App\Product::find($order_product->product_id)->name_ar}}</td>
                                        <td>{{$order_product->quantity}}</td>
                                        <td>{!!  (is_numeric($order_product->text))?'<a href="'.asset(\App\SuggestedText::find($order_product->text)->text).'" target="_blank">النص</a>':'<a href="'.asset($order_product->text).'" target="_blank">النص</a>'!!}</td>
                                        <td>{{$order_product->notes}}</td>
                                        {{--<td><button onclick="JSalert({{$coupon->id}})"  id="{{$coupon->id}}"  class="btn btn-danger">حذف</button></td>--}}
                                    </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection
