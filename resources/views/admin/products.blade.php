@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                @foreach($products as $product)
                    <form action="{{route('deleteProduct',$product->id)}}" method="post" id="form-{{$product->id}}">
                        {{csrf_field()}}
                        {{method_field('delete')}}
                        <input type="hidden" name="class_name" value="{{get_class($product)}}">
                    </form>
            @endforeach
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">المنتجات</h4>
                            <a class="btn btn-primary pull-left" href="{{route('create_product')}}">اضف منتج</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم باللغة العربية</th>
                                        <th>الاسم باللغة الانجليزية</th>
                                        <th>السعر</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                        <th>نفاذ الكمية</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم باللغة العربية</th>
                                        <th>الاسم باللغة الانجليزية</th>
                                        <th>السعر</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                        <th>نفاذ الكمية</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>{{$product->id}}</td>
                                            <td>{{$product->name_ar}}</td>
                                            <td>{{$product->name_en}}</td>
                                            <td>{{$product->price}}</td>
                                            <td><a href="{{route('update_product',$product->id)}}" class="btn btn-primary">تعديل</a></td>
                                            <td><button onclick="JSalert({{$product->id}})"  id="{{$product->id}}"  class="btn btn-danger">حذف</button></td>
                                            @if($product->quantity != 0)
                                            <td><a href="{{route('change_product_quantity',[$product->id,0])}}" class="btn btn-warning">نفذت الكمية</a></td>
                                            @else
                                                <td><a href="{{route('change_product_quantity',[$product->id,1])}}" class="btn btn-primary">تجديد الكمية</a></td>
                                            @endif

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection