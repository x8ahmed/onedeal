@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                @foreach($ads as $ad)
                    <form action="{{route('deleteItem',$ad->id)}}" method="post" id="form-{{$ad->id}}">
                        {{csrf_field()}}
                        {{method_field('delete')}}
                        <input type="hidden" name="class_name" value="{{get_class($ad)}}">
                    </form>
            @endforeach
                    <!-- DOM dataTable -->
                    <div class="col-md-12">
                        <div class="widget">
                            <header class="widget-header">
                                <h4 class="widget-title">الصور الاعلانية</h4>
                                <a class="btn btn-primary pull-left" href="{{route('create_ad')}}">اضف صورة</a>
                            </header><!-- .widget-header -->
                            <hr class="widget-separator">
                            <div class="widget-body">
                                <div class="table-responsive">
                                    <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>الصورة</th>
                                            <th>حذف</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>الصورة</th>
                                            <th>حذف</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        @foreach($ads as $ad)
                                        <tr>
                                            <td>{{$ad->id}}</td>
                                            @php $arr = ['mkv','ogg','mp4','flac','webM']; @endphp
                                            @if(in_array(explode('.',$ad->picture)[count(explode('.',$ad->picture))-1],$arr))
                                            <td>

                                                <video height="100%" width="100%" controls autoplay loop muted playsinline>
                                                    <source src="{{asset($ad->picture)}}" type="video/mp4">
                                                    <source src="{{asset($ad->picture)}}" type="video/ogg">
                                                    <source src="{{asset($ad->picture)}}" type="video/webM">
                                                    <source src="{{asset($ad->picture)}}" type="video/flac">
                                                </video>
                                            </td>
                                                @else
                                                <td><img src="{{asset("$ad->picture")}}" class="img-responsive"></td>

                                            @endif
                                            <td><button onclick="JSalert({{$ad->id}})"  id="{{$ad->id}}"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- .widget-body -->
                        </div><!-- .widget -->
                    </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection