@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اضف تصنيف جديد</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="{{route('save_category')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="parent_id">اختار التصنيف الرئيسى</label>
                                    <div>
                                        <select class="form-control" id="parent_id" name="parent_id" required>
                                            <option value="0">بدون</option>
                                            @php $parents = \App\Category::where('parent_id',0)->get() @endphp
                                            @foreach($parents as $parent)
                                                <option value="{{$parent->id}}">{{$parent->name_ar}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="name_ar">الاسم باللغة العربية</label>
                                    <input type="text" name="name_ar" id="name_ar" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="name_en">الاسم باللغة الانجليزية</label>
                                    <input type="text" name="name_en" id="name_en" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="picture">صورة التصنيف</label>
                                    <input type="file" name="picture" id="picture" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="cat_type">نوع التصنيف</label>
                                    <div>
                                        <label>
                                            <input type="radio" name="cat_type" id="cat_type" value="product" required checked>
                                            منتج
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" name="cat_type" id="cat_type" value="text">
                                            نص
                                        </label>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection