@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget stats-widget">
                        <div class="widget-body clearfix">
                            <div class="pull-right">
                                <h3 class="widget-title text-primary"><span class="counter" data-plugin="counterUp">{{\Illuminate\Support\Facades\DB::table('users')->count()}}</span></h3>
                                <small class="text-color">المستخدمين</small>
                            </div>
                            <span class="pull-left big-icon watermark"><i class="fa fa-paperclip"></i></span>
                        </div>
                        <footer class="widget-footer bg-primary">
                            <small>العدد الكلى</small>
                            <span class="small-chart pull-left" data-plugin="sparkline" data-options="[4,3,5,2,1], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
                        </footer>
                    </div><!-- .widget -->
                </div>

                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<div class="widget stats-widget">--}}
                        {{--<div class="widget-body clearfix">--}}
                            {{--<div class="pull-right">--}}
                                {{--<h3 class="widget-title text-danger"><span class="counter" data-plugin="counterUp">{{\Illuminate\Support\Facades\DB::table('orders')->count()}}</span></h3>--}}
                                {{--<small class="text-color">الطلبات</small>--}}
                            {{--</div>--}}
                            {{--<span class="pull-left big-icon watermark"><i class="fa fa-ban"></i></span>--}}
                        {{--</div>--}}
                        {{--<footer class="widget-footer bg-danger">--}}
                            {{--<small>العدد الكلى</small>--}}
                            {{--<span class="small-chart pull-left" data-plugin="sparkline" data-options="[1,2,3,5,4], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>--}}
                        {{--</footer>--}}
                    {{--</div><!-- .widget -->--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<div class="widget stats-widget">--}}
                        {{--<div class="widget-body clearfix">--}}
                            {{--<div class="pull-right">--}}
                                {{--<h3 class="widget-title text-success"><span class="counter" data-plugin="counterUp">{{\Illuminate\Support\Facades\DB::table('products')->count()}}</span></h3>--}}
                                {{--<small class="text-color">المنتجات</small>--}}
                            {{--</div>--}}
                            {{--<span class="pull-left big-icon watermark"><i class="fa fa-unlock-alt"></i></span>--}}
                        {{--</div>--}}
                        {{--<footer class="widget-footer bg-success">--}}
                            {{--<small>العدد الكلى</small>--}}
                            {{--<span class="small-chart pull-left" data-plugin="sparkline" data-options="[2,4,3,4,3], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>--}}
                        {{--</footer>--}}
                    {{--</div><!-- .widget -->--}}
                {{--</div>--}}

                <div class="col-md-3 col-sm-6">
                    <div class="widget stats-widget">
                        <div class="widget-body clearfix">
                            <div class="pull-right">
                                <h3 class="widget-title text-warning"><span class="counter" data-plugin="counterUp">{{\Illuminate\Support\Facades\DB::table('categories')->count()}}</span></h3>
                                <small class="text-color">التصنيفات</small>
                            </div>
                            <span class="pull-left big-icon watermark"><i class="fa fa-file-text-o"></i></span>
                        </div>
                        <footer class="widget-footer bg-warning">
                            <small>العدد الكلى</small>
                            <span class="small-chart pull-left" data-plugin="sparkline" data-options="[5,4,3,5,2],{ type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
                        </footer>
                    </div><!-- .widget -->
                </div>
            </div><!-- .row -->

        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection