@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">تعديل منتج</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="{{route('save_update_product',$product->id)}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{method_field('put')}}
                                <div class="form-group">
                                    <label for="name_ar">الاسم باللغة العربية</label>
                                    <input type="text" value="{{$product->name_ar}}" name="name_ar" id="name_ar" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="name_en">الاسم باللغة الانجليزية</label>
                                    <input type="text" value="{{$product->name_en}}" name="name_en" id="name_en" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="description_ar">الوصف باللغة العربية</label>
                                    <textarea name="description_ar" id="description_ar" class="form-control" cols="30" rows="10">{{$product->description_ar}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="description_en">الوصف باللغة الانجليزية</label>
                                    <textarea name="description_en" id="description_en" class="form-control" cols="30" rows="10">{{$product->description_en}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="pictures">صور المنتج</label>
                                    <small class="text-danger">عند اضافة صور للمنتج سيتم حذف الصور السابقة تلقائيا</small>
                                    <input type="file" name="pictures[]" multiple id="pictures" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="thumbnail">الصورة المصغرة</label>
                                    <input type="file" name="thumbnail" id="thumbnail" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="category_id">اختار التصنيف </label>
                                    <div>
                                        <select class="form-control" id="category_id" name="category_id" required>
                                            @php $parents = \App\Category::where('parent_id','!=',0)->get(); @endphp
                                            @foreach($parents as $parent)
                                                <option value="{{$parent->id}}">{{$parent->name_ar}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><!-- .form-group -->
                                {{--<div class="form-group">--}}
                                    {{--<label for="tax">الضريبة</label>--}}
                                    {{--<input type="text" value="{{$product->tax}}" name="tax" id="tax" class="form-control" required>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label for="price">السعر</label>
                                    <input type="text" value="{{$product->price}}" name="price" id="price" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="barcode">رقم الباركود</label>
                                    <input type="text" value="{{$product->barcode}}" name="barcode" id="barcode" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="width">العرض</label>
                                    <input type="text" value="{{$product->width}}" name="width" id="width" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="height">الارتفاع</label>
                                    <input type="text" value="{{$product->height}}" name="height" id="height" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection
@section('style')

    <!-- include summernote css/js -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
@endsection
@section('script')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script>
        $('#description_ar').summernote({height:250});
        $('#description_en').summernote({height:250});

    </script>
@endsection
