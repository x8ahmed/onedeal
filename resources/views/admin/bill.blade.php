@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading bg-white">
                        <h4 class="panel-title">معلومات الفاتورة</h4>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="float: right" class="col-sm-8 col-xs-6">
                                <h4 class="fw-600 text-right">رقم الطلب {{$order->id+1000}}</h4>
                                <p class="text-right">التاريخ: {{$order->created_at}}</p>

                                <h4 class="m-t-lg fw-600">التفاصيل:</h4>
                                <div class="clearfix">
                                    <p class="pull-right">الاجمالى:</p>
                                    <p class="pull-left">{{$order->total_after_coupon}}</p>
                                </div>
                                {{--<div class="clearfix">--}}
                                    {{--<p class="pull-left">Bank Name:</p>--}}
                                    {{--<p class="pull-right">Profit Bank Europe</p>--}}
                                {{--</div>--}}
                                {{--<div class="clearfix">--}}
                                    {{--<p class="pull-left">Country:</p>--}}
                                    {{--<p class="pull-right">United Kingdom</p>--}}
                                {{--</div>--}}
                                {{--<div class="clearfix">--}}
                                    {{--<p class="pull-left">City:</p>--}}
                                    {{--<p class="pull-right">London E18BF</p>--}}
                                {{--</div>--}}
                                {{--<div class="clearfix">--}}
                                    {{--<p class="pull-left">Address:</p>--}}
                                    {{--<p class="pull-right">3 Goodman Street</p>--}}
                                {{--</div>--}}
                                {{--<div class="clearfix">--}}
                                    {{--<p class="pull-left">IBAN:</p>--}}
                                    {{--<p class="pull-right">KFH37784028476740</p>--}}
                                {{--</div>--}}
                                {{--<div class="clearfix">--}}
                                    {{--<p class="pull-left">SWIFT Code:</p>--}}
                                    {{--<p class="pull-right">BPT4E</p>--}}
                                {{--</div>--}}
                            </div>

                            <div style="float: right" class="col-sm-4 col-xs-6">
                                <h4 class="fw-600">العنوان</h4>
                                {{--<p><a href="#">www.google.com</a></p>--}}
                                <p>{{\App\User::find($order->user_id)->address}}</p>
                                <p>رقم الهاتف: {{\App\User::find($order->user_id)->phone}}</p>

                                <h4 class="m-t-lg fw-600">اسم المستلم:</h4>
                                <p>{{\App\User::find($order->user_id)->name}}</p>
                                {{--<p>Normand axis LTD<br>3 Goodman street</p>--}}
                            </div>
                        </div>

                        <div class="table-responsive m-h-lg">
                            <table class="table">
                                <tr>
                                    <th>#</th>
                                    <th>اسم المنتج</th>
                                    <th>صورة المنتج</th>
                                    <th>الكمية</th>
                                    <th>النص</th>
                                </tr>
                                @foreach($products as $product)
                                    <tr>
                                        <th>{{$product->id}}</th>
                                        <th>{{\App\Product::find($product->product_id)->name_ar}}</th>
                                        <th><img src="{{asset(\App\Product::find($product->product_id)->thumbnail)}}" class="img-responsive" style="width: 100px;height: 100px"> </th>

{{--                                        <th>{{\App\Product::find($product->product_id)->thumbnail}}</th>--}}
                                        <th>{{$product->quantity}}</th>
                                        @if(is_numeric($product->text))
                                            <th><img src="{{asset(\App\SuggestedText::find($product->text)->text)}}" class="img-responsive" style="width: 100px;"> </th>
                                        @else
                                            <th><img src="{{asset($product->text)}}" class="img-responsive" style="width: 100px;"> </th>

                                        @endif
                                    </tr>
                               @endforeach

                            </table>
                        </div>

                        <div class="row">
                            <div class="col-sm-3 col-sm-push-9">
                                {{--<p>Sub-Total amount: $4800</p>--}}
                                {{--<p>VAT: $35</p>--}}
                                {{--<p>Grand Total: <span class="text-primary">$4800</span></p>--}}
                                <div class="m-t-lg">
                                    {{--<button type="button" class="btn btn-md btn-primary m-r-lg">Send Invoice</button>--}}
                                    <button type="button" class="btn btn-md btn-default" onclick="window.print();">طباعة</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- .panel-body -->
                </div>
            </div><!-- .container-fluid -->
        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection
@section('script')
    <script>
        $( document ).ready(function() {
            $('aside').hide();
            $('nav').hide();
            $('footer').hide();
            console.log( "ready!" );
        });

    </script>
@endsection
