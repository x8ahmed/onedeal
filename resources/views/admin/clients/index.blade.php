@extends('admin.layouts.master')
@section('main')


    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title" style="margin-bottom: 15px;">العملاء <small>{{$clients->total()}}</small></h4>
                    <form action="{{route('clients')}}" method="get">
                        <div class="row">
                            <div class=" col-lg-12 col-md-6 pull-left">
                                    <a href="{{route('clients-create')}}" class=" btn btn-primary btn-sm">اضافة</a>
                            </div>

                        </div>
                    </form>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="table-responsive">
                        @if($clients->count() > 0 )
                            <table  class="table table-striped" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>اسم العميل</th>
                                    <th>لوجو الشركة</th>
                                    <th>ألتحكم</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $index => $client)
                                    <tr>
                                        <td>{{$index + 1}}</td>
                                        <td>{{$client->name}}</td>
                                        <td><img src="{{$client->image_path}}" class="img-thumbnail" style="width: 150px"></td>
                                        <td>
                                                <a href="{{route('clients-edit',$client->id)}}" class="btn btn-info btn-sm">تعديل</a>
                                                <form action="{{route('clients-destroy',$client->id)}}" method="post" style="display: inline-block">
                                                    {{csrf_field()}}
                                                    {{method_field('delete')}}
                                                    <button type="submit" class="btn btn-danger delete btn-sm">حذف</button>
                                                </form>

                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div style="text-align: center">{{$clients->appends(request()->query())->links()}}</div>

                        @else

                            <h3>لايوجد سجلات</h3>
                        @endif
                    </div>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </div>

@endsection