@extends('admin.layouts.master')
@section('main')

    <div class="row">
        <div class="col-md-12 pull-right">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">تعديل العملاء</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        {{--<small>--}}
                        {{--Individual form controls automatically receive some global styling. All textual <code>&lt;input&gt;</code>, <code>&lt;textarea&gt;</code>, and <code>&lt;select&gt;</code> elements with <code>.form-control</code> are set to <code>width: 100%;</code> by default. Wrap labels and controls in <code>.form-group</code> for optimum spacing.--}}
                        {{--</small>--}}
                    </div>
                    @include('admin.layouts.massege')
                    <form action="{{route('clients-update',$client->id)}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('put')}}

                        <div class="form-group">
                            <label>الاسم</label>
                            <input type="text" name="name" class="form-control" value="{{$client->name}}">
                        </div>

                        <div class="form-group">
                            <label>لوجو الشركة</label>
                            <input type="file" name="logo" class="form-control image">
                        </div>
                        <div class="form-group">
                            <img src="{{$client->image_path}}" style="width: 120px;" class="image-reviw"/>
                        </div>

                        <button type="submit" class="btn btn-primary btn-md">تعديل</button>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->



    </div><!-- .row -->
@endsection