@extends('admin.layouts.master')
@section('main')


    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title" style="margin-bottom: 15px;"> الصفحات <small>{{$pages->total()}}</small></h4>
                    <form action="{{route('pages')}}" method="get">
                        <div class="row">
                            <div class=" col-lg-12 col-md-6 pull-left">
                                    <a href="{{route('pages-create')}}" class=" btn btn-primary btn-sm">اضافة</a>
                            </div>

                        </div>
                    </form>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="table-responsive">
                        @if($pages->count() > 0 )
                            <table  class="table table-striped" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>  اسم الصفحة</th>
                                    <th> المحتوي</th>
                                    <th>ألتحكم</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pages as $index => $page)
                                    <tr>
                                        <td>{{$index + 1}}</td>
                                        <td>{{$page->name}}</td>
                                        <td>{!! $page->content !!}</td>
                                        <td>
                                                <a href="{{route('pages-edit',$page->id)}}" class="btn btn-info btn-sm">تعديل</a>
                                                <form action="{{route('pages-destroy',$page->id)}}" method="post" style="display: inline-block">
                                                    {{csrf_field()}}
                                                    {{method_field('delete')}}
                                                    <button type="submit" class="btn btn-danger delete btn-sm">حذف</button>
                                                </form>

                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div style="text-align: center">{{$pages->appends(request()->query())->links()}}</div>

                        @else

                            <h3>لايوجد سجلات</h3>
                        @endif
                    </div>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </div>

@endsection