@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                {{--@foreach($coupons as $coupon)--}}
                    {{--<form action="{{route('deleteItem',$coupon->id)}}" method="post" id="form-{{$coupon->id}}">--}}
                        {{--{{csrf_field()}}--}}
                        {{--{{method_field('delete')}}--}}
                        {{--<input type="hidden" name="class_name" value="{{get_class($coupon)}}">--}}
                    {{--</form>--}}
            {{--@endforeach--}}
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">الشحن</h4>
                            <a class="btn btn-primary pull-left" href="{{route('create_shipping')}}">اضف طريقة شحن</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>طريقة الشحن</th>
                                        <th>السعر</th>
                                        <th>تعديل</th>

                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>طريقة الشحن</th>
                                        <th>السعر</th>
                                        <th>تعديل</th>

                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($shippings as $shipping)
                                        <tr>
                                            <td>{{$shipping->id}}</td>
                                            <td>{{$shipping->type}}</td>
                                            <td>{{$shipping->price}}</td>
                                            <td><a href="{{route('update_shipping',$shipping->id)}}" class="btn btn-primary">تعديل</a> </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection