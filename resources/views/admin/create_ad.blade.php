@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اضف صورة اعلانيه</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="{{route('save_ad')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="picture">الصورة/الفيديو</label>
                                    <input type="file" name="picture" id="picture" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection