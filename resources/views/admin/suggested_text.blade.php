@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                @foreach($texts as $text)
                    <form action="{{route('deleteItem',$text->id)}}" method="post" id="form-{{$text->id}}">
                        {{csrf_field()}}
                        {{method_field('delete')}}
                        <input type="hidden" name="class_name" value="{{get_class($text)}}">
                    </form>
            @endforeach
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">النصوص المقترحة</h4>
                            <a class="btn btn-primary pull-left" href="{{route('create_text')}}">اضف نص مقترح</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>النص</th>
                                        <th>التصنيف</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>النص</th>
                                        <th>التصنيف</th>
                                        <th>حذف</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($texts as $text)
                                        <tr>
                                            <td>{{$text->id}}</td>
                                            <td><img src="{{asset($text->text)}}" class="img-responsive" style="width: 200px"></td>
                                            <td>{{(\App\Category::find($text->category_id))?\App\Category::find($text->category_id)->name_ar:'بدون'}}</td>
                                            <td><button onclick="JSalert({{$text->id}})"  id="{{$text->id}}"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection