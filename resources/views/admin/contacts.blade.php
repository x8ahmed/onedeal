@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                @foreach($contacts as $contact)
                    <form action="{{route('deleteItem',$contact->id)}}" method="post" id="form-{{$contact->id}}">
                        {{csrf_field()}}
                        {{method_field('delete')}}
                        <input type="hidden" name="class_name" value="{{get_class($contact)}}">
                    </form>
            @endforeach
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">بيانات التواصل</h4>
                            <a class="btn btn-primary pull-left" href="https://fontawesome.com/v4.7.0/" target="_blank">رابط الايقونات</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم</th>
                                        <th>القيمة</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم</th>
                                        <th>القيمة</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($contacts as $contact)
                                        <tr>
                                            <td>{{$contact->id}}</td>
                                            <form method="post" action="{{route('update_contact',$contact->id)}}">
                                                {{csrf_field()}}
                                            <td><input type="text" class="form-control" name="name" value="{{$contact->name}}"></td>
                                            <td><input type="text" class="form-control" name="value" value="{{$contact->value}}"></td>
                                            <td><button type="submit" class="btn btn-primary">تعديل</button> </td>
                                            </form>
                                            <td><button onclick="JSalert({{$contact->id}})"  id="{{$contact->id}}"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <h4 style="font-weight: bold">اضف جديد</h4>
                            <form method="post" action="{{route('save_contact')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="name">الاسم</label>
                                    <input type="text" name="name" id="name" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="value">القيمة</label>
                                    <input type="text" name="value" id="value" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection