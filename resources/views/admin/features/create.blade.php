@extends('admin.layouts.master')
@section('main')

<div class="row">
    <div class="col-md-12 pull-right">
        <div class="widget">
            <header class="widget-header">
                <h4 class="widget-title">اضافة المميزات</h4>
            </header><!-- .widget-header -->
            <hr class="widget-separator">
            <div class="widget-body">
                <div class="m-b-lg">
                    {{--<small>--}}
                    {{--Individual form controls automatically receive some global styling. All textual <code>&lt;input&gt;</code>, <code>&lt;textarea&gt;</code>, and <code>&lt;select&gt;</code> elements with <code>.form-control</code> are set to <code>width: 100%;</code> by default. Wrap labels and controls in <code>.form-group</code> for optimum spacing.--}}
                    {{--</small>--}}
                </div>
                @include('admin.layouts.massege')
                <form action="{{route('features-store')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    {{method_field('post')}}


                    <div class="form-group">
                    <label for="first_name">لاختيار صورة</label>
                    <input type="radio" name="image" value="image" class="dd">
                      <input type="file" class="form-control img-responsive img-thumbnail" name="image" id="image">
                    </div>

                    <div class="form-group">
                    <label for="first_name">لاختيار ايقونة</label>
                    <input type="radio" name="image" value="icon" class="dd">
                    <input type="text" class="form-control hidden" name="icon" id="icon">
                     </div>

                    <div class="form-group">
                        <label for="sub_title">النوع</label>
                            <select name="type" class="form-control" id="type">
                                <option value=""> اختر النوع</option>
                                <option value="about-us">about-us</option>
                                <option value="service">service</option>
                                <option value="project">project</option>
                                <option value="why_choose_us">why_choose_us</option>
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="first_name">العنوان </label>
                        <input type="text" class="form-control" name="head" value="{{old('head')}}">
                    </div>

                    <div class="form-group">
                        <label for="first_name">الوصف</label>
                        <textarea class="form-control ckeditor" name="description">
                            {{old('description')}}
                        </textarea>
                    </div>

                    <div class="form-group">
                        <label for="first_name">الظهور في الرئيسية</label>
                        <input type="radio" name="show" value="0">لا
                        <input type="radio"  name="show" value="1">نعم
                    </div>
                    <div id="project_form" class="hidden">
                        <h3>معلومات المشاريع</h3>
                        <div class="form-group">
                            <label for="sub_title">العميل</label>
                            <input type="text" class="form-control" name="client" value="{{old('client')}}">
                        </div>

                        <div class="form-group">
                            <label for="sub_title">تاريخ الانتهاء</label>
                            <input type="text" class="form-control" name="completed_date" value="{{old('completed_date')}}">
                        </div>

                        <div class="form-group">
                            <label for="sub_title">نوع التعاقد</label>
                            <input type="text" class="form-control" name="contract_type" value="{{old('contract_type')}}">
                        </div>

                        <div class="form-group">
                            <label for="sub_title">موقع العمل</label>
                            <input type="text" class="form-control" name="project_location" value="{{old('project_location')}}">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
    @endsection
@section('script')
    <script>
        $(document).ready(function () {
           $(document).on('change','#type',function () {
           var val =  $( "#type option:selected" ).val();

           if (val == 'project'){
               $('#project_form').removeClass('hidden');
           }else {
               $('#project_form').addClass('hidden');
           }

           });
        });
        $(document).on('click','.dd',function () {
            var radio = $("input[name='image']:checked").val();
            if (radio == 'icon'){
                $('#icon').removeClass('hidden');
                $('.file-caption-main').hide();
            }else {
                $('#icon').addClass('hidden');
                $('.file-caption-main').show();
            }
        });

    </script>
    @endsection