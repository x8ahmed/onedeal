@extends('admin.layouts.master')
@section('main')

    <div class="row">
        <div class="col-md-12 pull-right">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">تعديل العملاء</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        {{--<small>--}}
                        {{--Individual form controls automatically receive some global styling. All textual <code>&lt;input&gt;</code>, <code>&lt;textarea&gt;</code>, and <code>&lt;select&gt;</code> elements with <code>.form-control</code> are set to <code>width: 100%;</code> by default. Wrap labels and controls in <code>.form-group</code> for optimum spacing.--}}
                        {{--</small>--}}
                    </div>
                    @include('admin.layouts.massege')
                    <form action="{{route('blog-update',$blog->id)}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('put')}}

                        <div class="form-group">
                            <label>الصورة المصغرة</label>
                            <input type="file" name="thumbnail" class="form-control image">
                        </div>

                        <div class="form-group">
                            <img src="{{$blog->image_path}}" style="width: 120px;" class="image-reviw"/>
                        </div>
                        <div class="form-group">
                            <label> العنوان</label>
                            <input type="text" name="head" class="form-control" value="{{$blog->head}}">
                        </div>

                        <div class="form-group">
                            <label> المحتوي</label>
                            <textarea type="text" name="body" class="form-control ckeditor">{{$blog->body}}</textarea>
                        </div>

                        <div class="form-group">
                            <label> تاريخ النشر</label>
                            <input type="date" name="dateOfPublich" class="form-control" value="{{$blog->dateOfPublich}}">
                        </div>


                        <button type="submit" class="btn btn-primary btn-md">تعديل</button>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->



    </div><!-- .row -->
@endsection