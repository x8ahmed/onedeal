@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                @foreach($billing_accounts as $billing_account)
                    <form action="{{route('deleteItem',$billing_account->id)}}" method="post" id="form-{{$billing_account->id}}">
                        {{csrf_field()}}
                        {{method_field('delete')}}
                        <input type="hidden" name="class_name" value="{{get_class($billing_account)}}">
                    </form>
            @endforeach
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">ارقام الحسابات</h4>
                            <a class="btn btn-primary pull-left" href="{{route('create_billing_account')}}">اضف حساب جديد</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>اسم البنك</th>
                                        <th>Bank name</th>
                                        <th>رقم الحساب</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>اسم البنك</th>
                                        <th>Bank name</th>
                                        <th>رقم الحساب</th>
                                        <th>حذف</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($billing_accounts as $billing_account)
                                        <tr>
                                            <td>{{$billing_account->id}}</td>
                                            <td>{{$billing_account->name_ar}}</td>
                                            <td>{{$billing_account->name_en}}</td>
                                            <td>{{$billing_account->account_number}}</td>
                                            <td><button onclick="JSalert({{$billing_account->id}})"  id="{{$billing_account->id}}"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection