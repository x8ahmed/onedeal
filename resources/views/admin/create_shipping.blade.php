@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اضف وسيلة شحن</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="{{route('save_shipping')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="parent_id">وسيلة الشحن الرئيسية</label>
                                    <div>
                                        <select class="form-control" id="parent_id" name="parent_id" required>
                                            <option value="0" selected>بدون</option>
                                            @php $shiipings = \App\Shipping::where('parent_id',0)->get(); @endphp
                                            @foreach($shiipings as $shiiping)
                                            <option value="{{$shiiping->id}}">{{$shiiping->type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="type">وسيلة الشحن</label>
                                    <input type="text" name="type" id="type" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="price">السعر</label>
                                    <input type="text" name="price" id="price" class="form-control" required>
                                </div>
                                <div id="mapid"></div>
                                <input type="hidden" name="latitude" id="latitude">
                                <input type="hidden" name="longitude" id="longitude">


                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection
@section('style')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>
    <style>
        #mapid { height: 300px; }
    </style>

    <!-- Load Esri Leaflet from CDN -->
    <script src="https://unpkg.com/esri-leaflet@2.2.3/dist/esri-leaflet.js"
            integrity="sha512-YZ6b5bXRVwipfqul5krehD9qlbJzc6KOGXYsDjU9HHXW2gK57xmWl2gU6nAegiErAqFXhygKIsWPKbjLPXVb2g=="
            crossorigin=""></script>


    <!-- Load Esri Leaflet Geocoder from CDN -->
    <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.2.13/dist/esri-leaflet-geocoder.css"
          integrity="sha512-v5YmWLm8KqAAmg5808pETiccEohtt8rPVMGQ1jA6jqkWVydV5Cuz3nJ9fQ7ittSxvuqsvI9RSGfVoKPaAJZ/AQ=="
          crossorigin="">

@endsection
@section('script')
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
            integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
            crossorigin=""></script>
    <script src="https://unpkg.com/esri-leaflet-geocoder@2.2.13/dist/esri-leaflet-geocoder.js"
            integrity="sha512-zdT4Pc2tIrc6uoYly2Wp8jh6EPEWaveqqD3sT0lf5yei19BC1WulGuh5CesB0ldBKZieKGD7Qyf/G0jdSe016A=="
            crossorigin=""></script>
    <script>
        var mymap = L.map('mapid').setView([24.774265, 46.738586], 8);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox.streets'
        }).addTo(mymap);
        var marker = L.marker([24.774265, 46.738586],{draggable: true}).addTo(mymap);
        marker.on('dragend', function (e) {
            console.log(marker.getLatLng().lat);
            console.log(marker.getLatLng().lng);
            document.getElementById('latitude').value = marker.getLatLng().lat;
            document.getElementById('longitude').value = marker.getLatLng().lng;

            geocodeService.reverse().latlng(e.latlng).run(function(error, result) {
                L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();
            });
        });



    </script>
@endsection