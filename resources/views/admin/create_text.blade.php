@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اضف نص مقترح</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="{{route('save_text')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="text">النص</label>
                                    <input type="file" name="text" id="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="category_id">اختار التصنيف </label>
                                    <div>
                                        <select class="form-control" id="category_id" name="category_id" required>
                                            @php $parents = \App\Category::where([
                                            ['parent_id','!=',0],['cat_type','text']
                                            ])->get(); @endphp
                                            @foreach($parents as $parent)
                                                <option value="{{$parent->id}}">{{$parent->name_ar}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><!-- .form-group -->
                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection