@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                @foreach($categories as $category)
                    <form action="{{route('deleteItem',$category->id)}}" method="post" id="form-{{$category->id}}">
                        {{csrf_field()}}
                        {{method_field('delete')}}
                        <input type="hidden" name="class_name" value="{{get_class($category)}}">
                    </form>
                @php $sub_cats = \App\Category::where('parent_id',$category->id)->get(); @endphp
                @foreach($sub_cats as $sub_cat)
                        <form action="{{route('deleteItem',$sub_cat->id)}}" method="post" id="form-{{$sub_cat->id}}">
                            {{csrf_field()}}
                            {{method_field('delete')}}
                            <input type="hidden" name="class_name" value="{{get_class($sub_cat)}}">
                        </form>
                @endforeach

                @endforeach
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">التصنيفات</h4>
                            <a class="btn btn-primary pull-left" href="{{route('create_category')}}">اضف تصنيف جديد</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم باللغة العربية</th>
                                        <th>الاسم باللغة الانجليزية</th>
                                        <th>الصورة</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم باللغة العربية</th>
                                        <th>الاسم باللغة الانجليزية</th>
                                        <th>الصورة</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td>{{$category->id}}</td>
                                            <td>{{$category->name_ar}}</td>
                                            <td>{{$category->name_en}}</td>
                                            <td><img src="{{asset($category->picture)}}" class="img-responsive img-circle" style="width: 50px;height: 50px;"></td>
                                            <td><a href="{{route('update_category',$category->id)}}" class="btn btn-primary">تعديل</a></td>
                                            <td><button onclick="JSalert({{$category->id}})"  id="{{$category->id}}"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                        @php $sub_cats = \App\Category::where('parent_id',$category->id)->get(); @endphp
                                        @foreach($sub_cats as $sub_cat)
                                            <tr>
                                                <td>{{$category->id}}__</td>
                                                <td>{{$sub_cat->name_ar}}</td>
                                                <td>{{$sub_cat->name_en}}</td>
                                                <td><img src="{{asset($sub_cat->picture)}}" class="img-responsive img-circle" style="width: 50px;height: 50px;"></td>
                                                <td><a href="{{route('update_category',$sub_cat->id)}}" class="btn btn-primary">تعديل</a></td>
                                                <td><button onclick="JSalert({{$sub_cat->id}})"  id="{{$sub_cat->id}}"  class="btn btn-danger">حذف</button></td>
                                            </tr>
                                        @endforeach
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection