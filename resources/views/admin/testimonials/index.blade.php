@extends('admin.layouts.master')
@section('main')


    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title" style="margin-bottom: 15px;">فريق العمل <small>{{$testimonials->total()}}</small></h4>
                    <form action="{{route('testimonials')}}" method="get">
                        <div class="row">
                            <div class=" col-lg-12 col-md-6 pull-left">
                                    <a href="{{route('testimonials-create')}}" class=" btn btn-primary btn-sm">اضافة</a>
                            </div>

                        </div>
                    </form>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="table-responsive">
                        @if($testimonials->count() > 0 )
                            <table  class="table table-striped" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الاسم</th>
                                    <th> صورة العضو</th>
                                    <th> المسمي الوظيفي</th>
                                    <th>ألتحكم</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($testimonials as $index => $testimonial)
                                    <tr>
                                        <td>{{$index + 1}}</td>
                                        <td>{{$testimonial->name}}</td>
                                        <td><img src="{{$testimonial->image_path}}" class="img-thumbnail" style="width: 150px"></td>
                                        <td>{{$testimonial->title}}</td>
                                        <td>
                                                <a href="{{route('testimonials-edit',$testimonial->id)}}" class="btn btn-info btn-sm">تعديل</a>
                                                <form action="{{route('testimonials-destroy',$testimonial->id)}}" method="post" style="display: inline-block">
                                                    {{csrf_field()}}
                                                    {{method_field('delete')}}
                                                    <button type="submit" class="btn btn-danger delete btn-sm">حذف</button>
                                                </form>

                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div style="text-align: center">{{$testimonials->appends(request()->query())->links()}}</div>

                        @else

                            <h3>لايوجد سجلات</h3>
                        @endif
                    </div>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </div>

@endsection