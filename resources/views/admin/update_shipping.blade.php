@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">تعديل وسيلة شحن</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="{{route('save_update_shipping',$shipping->id)}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{method_field('put')}}
                                <div class="form-group">
                                    <label for="parent_id">وسيلة الشحن الرئيسية</label>
                                    <div>
                                        <select class="form-control" id="parent_id" name="parent_id" required>
                                            <option value="0" {{($shipping->parent_id==0)?'selected':''}}>بدون</option>
                                            @php $shiipings = \App\Shipping::where('parent_id',0)->get(); @endphp
                                            @foreach($shiipings as $shiiping)
                                                <option value="{{$shiiping->id}}" {{($shiiping->id==$shipping->parent_id)?'selected':''}}>{{$shiiping->type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="type">وسيلة الشحن</label>
                                    <input type="text" name="type" id="type" value="{{$shipping->type}}" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="price">السعر</label>
                                    <input type="text" name="price" id="price" value="{{$shipping->price}}" class="form-control" required>
                                </div>
                                <div id="mapid"></div>
                                <input type="hidden" name="latitude" id="latitude" value="{{$shipping->lat}}">
                                <input type="hidden" name="longitude" id="longitude" value="{{$shipping->lng}}">


                                <button type="submit" class="btn btn-primary btn-md">تعديل</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection
@section('style')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>
    <style>
        #mapid { height: 300px; }
    </style>
@endsection
@section('script')
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
            integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
            crossorigin=""></script>
    <script>
        var mymap = L.map('mapid').setView([24.774265, 46.738586], 8);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox.streets'
        }).addTo(mymap);
        var marker = L.marker([24.774265, 46.738586],{draggable: true}).addTo(mymap);
        marker.on('dragend', function (e) {
            console.log(marker.getLatLng().lat);
            console.log(marker.getLatLng().lng);
            document.getElementById('latitude').value = marker.getLatLng().lat;
            document.getElementById('longitude').value = marker.getLatLng().lng;
        });


    </script>
@endsection