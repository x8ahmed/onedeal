@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اعدادات الموقع</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="{{route('save_site_settings')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                @foreach($settings as $setting)
                                <div class="form-group">
                                    <label>{{$setting->display_name}}</label>
                                    @if($setting->input_type=='file'&&$setting->value!=null)
                                        <img src="{{asset($setting->value)}}" class="img-responsive img-thumbnail" style="width: 200px;display: block;">
                                    @endif
                                    <input type="{{$setting->input_type}}" value="{{$setting->value}}" name="{{$setting->name}}" class="form-control">
                                </div>
                                @endforeach
                                <button type="submit" class="btn btn-primary btn-md">تأكيد</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection