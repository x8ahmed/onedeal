@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                @foreach($coupons as $coupon)
                    <form action="{{route('deleteItem',$coupon->id)}}" method="post" id="form-{{$coupon->id}}">
                        {{csrf_field()}}
                        {{method_field('delete')}}
                        <input type="hidden" name="class_name" value="{{get_class($coupon)}}">
                    </form>
            @endforeach
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اكواد الخصم</h4>
                            <a class="btn btn-primary pull-left" href="{{route('create_coupon')}}">اضف كود</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الكود</th>
                                        <th>النوع</th>
                                        <th>القيمة</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>الكود</th>
                                        <th>النوع</th>
                                        <th>القيمة</th>
                                        <th>حذف</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($coupons as $coupon)
                                        <tr>
                                            <td>{{$coupon->id}}</td>
                                            <td>{{$coupon->name}}</td>
                                            <td>{{($coupon->type=='price')?'مبلغ ثابت':'خصم نسبة'}}</td>
                                            <td>{{$coupon->value}}</td>
                                            <td><button onclick="JSalert({{$coupon->id}})"  id="{{$coupon->id}}"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection