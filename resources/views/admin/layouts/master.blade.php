<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <link rel="shortcut icon" sizes="196x196" href="{{asset('assets/images/logo.png')}}">
    <title>Infinity - Bootstrap Admin Template</title>

    <link rel="stylesheet" href="{{asset('libs/bower/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css')}}">
    <!-- build:css ../assets/css/app.min.css -->
    <link rel="stylesheet" href="{{asset('libs/bower/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/bower/fullcalendar/dist/fullcalendar.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/bower/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/core.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('libs/bower/bootstrap-fileInput/fileinput.min.css')}}">
    <link rel="stylesheet" href="{{asset('libs/bower/bootstrap-fileInput/explorer/theme.css')}}">

    <link rel="stylesheet" href="{{asset('noty/noty.css')}}">
    <script src="{{asset('noty/noty.min.js')}}"></script>

    <link rel="stylesheet" href="{{asset('assets/css/rtl.css')}}">
    <!-- endbuild -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
    <script src="{{asset('libs/bower/breakpoints.js/dist/breakpoints.min.js')}}"></script>

    <script>
        Breakpoints();
    </script>
    @yield('style')
</head>

<body class="menubar-left menubar-unfold menubar-light theme-primary">
<!--============= start main area -->

<!-- APP NAVBAR ==========-->
<nav id="app-navbar" class="navbar navbar-inverse navbar-fixed-top primary">

    <!-- navbar header -->
    <div class="navbar-header">
        <button type="button" id="menubar-toggle-btn" class="navbar-toggle visible-xs-inline-block navbar-toggle-left hamburger hamburger--collapse js-hamburger">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-box"><span class="hamburger-inner"></span></span>
        </button>

        <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="zmdi zmdi-hc-lg zmdi-more"></span>
        </button>

        <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="zmdi zmdi-hc-lg zmdi-search"></span>
        </button>

        <a href="{{route('index')}}" class="navbar-brand">
            <span class="brand-icon"><i class="fa fa-gg"></i></span>
            <span class="brand-name">One Deal</span>
        </a>
    </div><!-- .navbar-header -->

    <div class="navbar-container container-fluid">
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-toolbar navbar-toolbar-left navbar-left">
                <li class="hidden-float hidden-menubar-top">
                    <a href="javascript:void(0)" role="button" id="menubar-fold-btn" class="hamburger hamburger--arrowalt is-active js-hamburger">
                        <span class="hamburger-box"><span class="hamburger-inner"></span></span>
                    </a>
                </li>
                <li>
                    <h5 class="page-title hidden-menubar-top hidden-float">لوحة التحكم</h5>
                </li>
            </ul>

            <ul class="nav navbar-toolbar navbar-toolbar-right navbar-right">
                {{--<li class="nav-item dropdown hidden-float">--}}
                    {{--<a href="javascript:void(0)" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">--}}
                        {{--<i class="zmdi zmdi-hc-lg zmdi-search"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}

                <li class="dropdown hamo">
                    {{--@php $orders = \App\Order::where('status',0)->orderBy('id','desc')->get(); @endphp--}}
                    {{--@if(count($orders))--}}
                    {{--<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-hc-lg zmdi-notifications"></i>--}}
                    {{--<span class="badge badge-danger">{{count($orders)}}</span>--}}
                    {{--</a>--}}
                    {{--<div class="media-group dropdown-menu animated flipInY">--}}
                        {{--@foreach($orders as $order)--}}
                        {{--<a href="{{route('orders')}}" class="media-group-item">--}}
                            {{--<div class="media">--}}
                                {{--<div class="media-left">--}}
                                    {{--<div class="avatar avatar-xs avatar-circle">--}}
                                        {{--<img src="../assets/images/221.jpg" alt="">--}}
                                        {{--<i class="status status-online"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="media-body">--}}
                                    {{--<h5 class="media-heading">طلب شراء جديد</h5>--}}
                                    {{--<small class="media-meta">{{\App\User::find($order->user_id)->name}}</small>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</a><!-- .media-group-item -->--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                        {{--@endif--}}
                </li>

                {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-hc-lg zmdi-settings"></i></a>--}}
                    {{--<ul class="dropdown-menu animated flipInY">--}}
                        {{--<li><a href="javascript:void(0)"><i class="zmdi m-r-md zmdi-hc-lg zmdi-account-box"></i>My Profile</a></li>--}}
                        {{--<li><a href="javascript:void(0)"><i class="zmdi m-r-md zmdi-hc-lg zmdi-balance-wallet"></i>Balance</a></li>--}}
                        {{--<li><a href="javascript:void(0)"><i class="zmdi m-r-md zmdi-hc-lg zmdi-phone-msg"></i>Connection<span class="label label-primary">3</span></a></li>--}}
                        {{--<li><a href="javascript:void(0)"><i class="zmdi m-r-md zmdi-hc-lg zmdi-info"></i>privacy</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0)" class="side-panel-toggle" data-toggle="class" data-target="#side-panel" data-class="open" role="button"><i class="zmdi zmdi-hc-lg zmdi-apps"></i></a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div><!-- navbar-container -->
</nav>
<!--========== END app navbar -->

<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light">
    <div class="app-user">
        <div class="media">
            <div class="media-left">
                <div class="avatar avatar-md avatar-circle">
                    <a href="javascript:void(0)"><img class="img-responsive" src="../assets/images/221.jpg" alt="avatar"/></a>
                </div><!-- .avatar -->
            </div>
            <div class="media-body">
                <div class="foldable">
                    <h5><a href="javascript:void(0)" class="username">{{\Auth::user()->name}}</a></h5>
                    <ul>
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <small>{{\Auth::user()->roles->first()->display_name}}</small>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu animated flipInY">
                                <li>
                                    <a class="text-color" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <span class="m-r-xs"><i class="fa fa-power-off"></i></span>
                                        <span>تسجيل الخروج</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- .media-body -->
        </div><!-- .media -->
    </div><!-- .app-user -->

    <div class="menubar-scroll">
        <div class="menubar-scroll-inner">
            <ul class="app-menu">

                <li>
                    <a href="{{route('dashboard')}}">
                        <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
                        <span class="menu-text">القائمة الرئيسية</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('site_settings')}}">
                        <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
                        <span class="menu-text">اعدادات الموقع</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('slider')}}">
                        <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
                        <span class="menu-text">الاسلايدر</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('features')}}">
                        <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
                        <span class="menu-text">المميزات</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('clients')}}">
                        <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
                        <span class="menu-text">اضافة عميل</span>
                    </a>
                </li>

                <li>
                    <a href="{{route('testimonials')}}">
                        <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
                        <span class="menu-text">اضافة فريق عمل</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('questions')}}">
                        <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
                        <span class="menu-text">اضافة اراء العملاء</span>
                    </a>
                </li>

                <li>
                    <a href="{{route('blog')}}">
                        <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
                        <span class="menu-text">المدونة</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('pages')}}">
                        <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
                        <span class="menu-text">الصفحات</span>
                    </a>
                </li>
            </ul><!-- .app-menu -->
        </div><!-- .menubar-scroll-inner -->
    </div><!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->

<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
    <div class="navbar-search-inner">
        <form action="#">
            <span class="search-icon"><i class="fa fa-search"></i></span>
            <input class="search-field" type="search" placeholder="search..."/>
        </form>
        <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
            <i class="fa fa-close"></i>
        </button>
    </div>
    <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
    @yield('main')

    @include('admin.layouts.session')

    <!-- APP FOOTER -->
    <div class="wrap p-t-0">
        <footer class="app-footer">
            <div class="clearfix">
                <ul class="footer-menu pull-right">
                    <li><a href="javascript:void(0)">Careers</a></li>
                    <li><a href="javascript:void(0)">Privacy Policy</a></li>
                    <li><a href="javascript:void(0)">Feedback <i class="fa fa-angle-up m-l-md"></i></a></li>
                </ul>
                <div class="copyright pull-left">Copyright RaThemes 2016 &copy;</div>
            </div>
        </footer>
    </div>
    <!-- /#app-footer -->
</main>
<!--========== END app main -->

<!-- APP CUSTOMIZER -->
<div id="app-customizer" class="app-customizer">
    <a href="javascript:void(0)"
       class="app-customizer-toggle theme-color"
       data-toggle="class"
       data-class="open"
       data-active="false"
       data-target="#app-customizer">
        <i class="fa fa-gear"></i>
    </a>
    <div class="customizer-tabs">
        <!-- tabs list -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#menubar-customizer" aria-controls="menubar-customizer" role="tab" data-toggle="tab">Menubar</a></li>
            <li role="presentation"><a href="#navbar-customizer" aria-controls="navbar-customizer" role="tab" data-toggle="tab">Navbar</a></li>
        </ul><!-- .nav-tabs -->

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane in active fade" id="menubar-customizer">
                <div class="hidden-menubar-top hidden-float">
                    <div class="m-b-0">
                        <label for="menubar-fold-switch">Fold Menubar</label>
                        <div class="pull-right">
                            <input id="menubar-fold-switch" type="checkbox" data-switchery data-size="small" />
                        </div>
                    </div>
                    <hr class="m-h-md">
                </div>
                <div class="radio radio-default m-b-md">
                    <input type="radio" id="menubar-light-theme" name="menubar-theme" data-toggle="menubar-theme" data-theme="light">
                    <label for="menubar-light-theme">Light</label>
                </div>

                <div class="radio radio-inverse m-b-md">
                    <input type="radio" id="menubar-dark-theme" name="menubar-theme" data-toggle="menubar-theme" data-theme="dark">
                    <label for="menubar-dark-theme">Dark</label>
                </div>
            </div><!-- .tab-pane -->
            <div role="tabpanel" class="tab-pane fade" id="navbar-customizer">
                <!-- This Section is populated Automatically By javascript -->
            </div><!-- .tab-pane -->
        </div>
    </div><!-- .customizer-taps -->
    <hr class="m-0">
    {{--<div class="customizer-reset">--}}
        {{--<button id="customizer-reset-btn" class="btn btn-block btn-outline btn-primary">Reset</button>--}}
        {{--<a href="https://themeforest.net/item/infinity-responsive-web-app-kit/16230780" class="m-t-sm btn btn-block btn-danger">Buy Now</a>--}}
    {{--</div>--}}
</div><!-- #app-customizer -->

<!-- SIDE PANEL -->
<div id="side-panel" class="side-panel">
    <div class="panel-header">
        <h4 class="panel-title">Friends</h4>
    </div>
    <div class="scrollable-container">
        <div class="media-group">
            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/221.jpg" alt="">
                            <i class="status status-online"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">John Doe</h5>
                        <small class="media-meta">active now</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/205.jpg" alt="">
                            <i class="status status-online"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">John Doe</h5>
                        <small class="media-meta">active now</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/206.jpg" alt="">
                            <i class="status status-online"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Adam Kiti</h5>
                        <small class="media-meta">active now</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/207.jpg" alt="">
                            <i class="status status-away"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Jane Doe</h5>
                        <small class="media-meta">away</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/208.jpg" alt="">
                            <i class="status status-away"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Sara Adams</h5>
                        <small class="media-meta">away</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/209.jpg" alt="">
                            <i class="status status-away"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Smith Doe</h5>
                        <small class="media-meta">away</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/219.jpg" alt="">
                            <i class="status status-away"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Dana Dyab</h5>
                        <small class="media-meta">away</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/210.jpg" alt="">
                            <i class="status status-offline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Jeffry Way</h5>
                        <small class="media-meta">2 hours ago</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/211.jpg" alt="">
                            <i class="status status-offline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Jane Doe</h5>
                        <small class="media-meta">5 hours ago</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/212.jpg" alt="">
                            <i class="status status-offline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Adam Khoury</h5>
                        <small class="media-meta">22 minutes ago</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/207.jpg" alt="">
                            <i class="status status-offline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Sara Smith</h5>
                        <small class="media-meta">2 days ago</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
                <div class="media">
                    <div class="media-left">
                        <div class="avatar avatar-xs avatar-circle">
                            <img src="../assets/images/211.jpg" alt="">
                            <i class="status status-offline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">Donia Dyab</h5>
                        <small class="media-meta">3 days ago</small>
                    </div>
                </div>
            </a><!-- .media-group-item -->
        </div>
    </div><!-- .scrollable-container -->
</div><!-- /#side-panel -->

<!-- build:js ../assets/js/core.min.js -->
<script src="{{asset('libs/bower/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('libs/bower/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('libs/bower/jQuery-Storage-API/jquery.storageapi.min.js')}}"></script>
<script src="{{asset('libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js')}}"></script>
<script src="{{asset('libs/bower/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<script src="{{asset('libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
<script src="{{asset('libs/bower/PACE/pace.min.js')}}"></script>
<!-- endbuild -->

<!-- build:js ../assets/js/app.min.js -->
<script src="{{asset('assets/js/library.js')}}"></script>
<script src="{{asset('assets/js/plugins.js')}}"></script>
<script src="{{asset('assets/js/app.js')}}"></script>
<!-- endbuild -->
<script src="{{asset('libs/bower/moment/moment.js')}}"></script>
<script src="{{asset('libs/bower/fullcalendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{asset('assets/js/fullcalendar.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    @if(\Illuminate\Support\Facades\Session::has('message'))
    swal("{{\Illuminate\Support\Facades\Session::get('message')}}","", "success");
    @endif
    function JSalert(id){
        swal({
            title: "هل انت متأكد؟",
            text: "البيانات الممسوحة لا يتم استرجاعها",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                swal("تم الحذف بنجاح", {
                    icon: "success",
                });
                document.getElementById('form-'+id).submit();
            } else {
                swal("لم يتم الحذف");
    }
    });
    }


</script>
<script  src="{{asset('libs/bower/bootstrap-fileInput/fileinput.min.js')}}"></script>
<script  src="{{asset('libs/bower/bootstrap-fileInput/explorer/theme.js')}}"></script>
<script src="{{asset('assets/js/script.js')}}"></script>
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>

<script>
    $(".image").change(function() {

        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.image-reviw').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    $('.delete').click(function (e) {

        var that = $(this)

        e.preventDefault();

        var n = new Noty({
            text: "تاكيد الحذف",
            type: "warning",
            killer: true,
            buttons: [
                Noty.button("نعم", 'btn btn-success mr-2', function () {
                    that.closest('form').submit();
                }),

                Noty.button("لا", 'btn btn-primary mr-2', function () {
                    n.close();
                })
            ]
        });

        n.show();


    });//end of del

    CKEDITOR.config.language = "ar";
</script>

<script>
    $("input[type='file']").fileinput({
        theme: "explorer",
        initialPreviewAsData:true,
        {{--method:'GET',--}}
        {{--uploadUrl: "{{asset('')}}"--}}
    });
</script>

@yield('script')

</body>
</html>