@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                {{--@foreach($coupons as $coupon)--}}
                    {{--<form action="{{route('deleteItem',$coupon->id)}}" method="post" id="form-{{$coupon->id}}">--}}
                        {{--{{csrf_field()}}--}}
                        {{--{{method_field('delete')}}--}}
                        {{--<input type="hidden" name="class_name" value="{{get_class($coupon)}}">--}}
                    {{--</form>--}}
            {{--@endforeach--}}
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">الطلبات</h4>
                            {{--<a class="btn btn-primary pull-left" href="{{route('create_coupon')}}">اضف كود</a>--}}
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>حالة الطلب</th>
                                        <th>وسيلة الشحن</th>
                                        <th>وسيلة الدفع</th>
                                        <th>صورة الايصال</th>
                                        <th>الاجمالى</th>
                                        <th>خيارات</th>
                                        <th colspan="5">المنتجات</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>اسم المنتج</th>
                                        <th>الكمية</th>
                                        <th>النص المطلوب</th>
                                        <th>ملاحظات</th>
                                        <th>المرفقات</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>حالة الطلب</th>
                                        <th>وسيلة الشحن</th>
                                        <th>وسيلة الدفع</th>
                                        <th>صورة الايصال</th>
                                        <th>الاجمالى</th>
                                        <th>خيارات</th>
                                        <th colspan="5">المنتجات</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($orders as $order)
                                        @php $order_products = \App\OrderProduct::where('order_id',$order->id)->get(); @endphp
                                        @foreach($order_products as $order_product)
                                        <tr>
                                            @if($loop->first)
                                                <td rowspan="{{count($order_products)}}"><a href="javascript:void(0)"  data-toggle="modal" data-target="#order_{{$order->id}}">{{$order->id}}</a></td>
                                                <!-- Modal -->
                                                <div id="order_{{$order->id}}" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title"></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p style="font-weight: bold">بيانات العميل</p>
                                                                @php $user = \App\User::find($order->user_id); @endphp
                                                                <p>الاسم: {{$user->name}}</p>
                                                                <p>رقم الهاتف: {{$user->phone}}</p>
                                                                <p>العنوان: {{$user->address}}</p>
                                                                <p style="font-weight: bold">ملاحظات اخرى</p>
                                                                <p>{{$order->other_notes}}</p>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- Modal -->
                                            <td rowspan="{{count($order_products)}}">
                                                @if($order->status == 0)
                                                    {{'قيد المعالجة'}}
                                                @elseif($order->status == 1)
                                                    {{'جارى الشحن'}}
                                                @elseif($order->status == 2)
                                                    {{'تم الاستلام'}}
                                                @endif
                                            </td>
                                                <td rowspan="{{count($order_products)}}">{{\App\Shipping::find($order->shipping_type)->type}}</td>
                                            <td rowspan="{{count($order_products)}}">{{$order->payment_method}}</td>
                                            <td rowspan="{{count($order_products)}}">{!! ($order->receipt)?'<a href="'.asset($order->receipt).'" target="_blank">صورة الايصال</a>':'لا يوجد' !!}  </td>
                                            <td rowspan="{{count($order_products)}}">{{$order->total_after_coupon}}</td>
                                            <td rowspan="{{count($order_products)}}">
                                                @if($order->status == 0)
                                                    <a href="{{route('start_shipping',[$order->id,1])}}" class="btn btn-primary">بدء الشحن</a>
                                                    <a href="{{route('bill',$order->id)}}" class="btn btn-info">طباعة الطلب</a>
                                                @elseif($order->status == 1)
                                                    <a href="{{route('start_shipping',[$order->id,2])}}" class="btn btn-primary">تاكيد الاستلام</a>
                                                    <a href="{{route('bill',$order->id)}}" class="btn btn-info">طباعة الطلب</a>

                                                @elseif($order->status == 2)
                                                    {{'تم الاستلام'}}
                                                    <a href="{{route('bill',$order->id)}}" class="btn btn-info">طباعة الطلب</a>

                                                @endif
                                            </td>
                                            @endif
                                            <td>{{\App\Product::find($order_product->product_id)->name_ar}}</td>
                                            <td>{{$order_product->quantity}}</td>
                                            <td>{!!  (is_numeric($order_product->text))?'<a href="'.asset(\App\SuggestedText::find($order_product->text)->text).'" target="_blank">النص</a>':'<a href="'.asset($order_product->text).'" target="_blank">النص</a>'!!}</td>
                                                <td>{{$order_product->notes}}</td>
                                                <td>@if($order_product->attachements)
                                                        <a href="{{asset($order_product->attachements)}}" target="_blank">المرفقات</a>
                                                    @endif
                                                </td>

                                            {{--<td><button onclick="JSalert({{$coupon->id}})"  id="{{$coupon->id}}"  class="btn btn-danger">حذف</button></td>--}}
                                        </tr>
                                    @endforeach
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection