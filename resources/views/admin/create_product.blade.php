@extends('admin.layouts.master')
@section('main')
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اضف منتج</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="{{route('save_product')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="name_ar">الاسم باللغة العربية</label>
                                    <input type="text" name="name_ar" id="name_ar" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="name_en">الاسم باللغة الانجليزية</label>
                                    <input type="text" name="name_en" id="name_en" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="description_ar">الوصف باللغة العربية</label>
                                <textarea name="description_ar" id="description_ar" class="form-control" cols="30" rows="10" data-plugin="summernote" data-options="{height: 250}"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="description_en">الوصف باللغة الانجليزية</label>
                                <textarea name="description_en" id="description_en" class="form-control" cols="30" rows="10" data-plugin="summernote" data-options="{height: 250}"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="pictures">صور المنتج</label>
                                    <input type="file" name="pictures[]" multiple id="pictures" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="thumbnail">الصورة المصغرة</label>
                                    <input type="file" name="thumbnail" id="thumbnail" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="category_id">اختار التصنيف </label>
                                    <div>
                                        <select class="form-control" id="category_id" name="category_id" required>
                                            @php $parents = \App\Category::where([
                                            ['parent_id','!=',0],['cat_type','product']

                                            ])->get(); @endphp
                                            @foreach($parents as $parent)
                                                <option value="{{$parent->id}}">{{$parent->name_ar}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><!-- .form-group -->
                                {{--<div class="form-group">--}}
                                    {{--<label for="tax">الضريبة</label>--}}
                                    {{--<input type="text" name="tax" id="tax" class="form-control" required>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label for="price">السعر</label>
                                    <input type="text" name="price" id="price" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="barcode">رقم الباركود</label>
                                    <input type="text" name="barcode" id="barcode" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="width">العرض</label>
                                    <input type="text" name="width" id="width" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="height">الارتفاع</label>
                                    <input type="text" name="height" id="height" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
@endsection