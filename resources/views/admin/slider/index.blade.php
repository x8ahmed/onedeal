@extends('admin.layouts.master')
@section('main')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title" style="margin-bottom: 15px;">الاسلايدر <small>{{$sliders->total()}}</small></h4>
                    <form action="{{route('slider')}}" method="get">
                        <div class="row">
                            {{--<div class="col-md-4 pull-right">--}}
                                {{--<input type="text" name="search" class="form-control" placeholder="بحث ..." value="{{request()->search}}">--}}
                            {{--</div>--}}
                            <div class=" col-lg-12 col-md-12 pull-left">
                                {{--<button type="submit" class="btn btn-primary btn-sm">بحث</button>--}}
                                    <a href="{{route('slider-create')}}" class=" btn btn-primary btn-sm"> <i class="fa fa-plus"></i>اضافة</a>
                          
                            </div>

                        </div>
                    </form>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="table-responsive">
                        @if($sliders->count() > 0 )
                            <table  class="table table-striped" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>

                                    <th>الصورة</th>
                                    <th>العنوان الفرعي</th>
                                    <th>العنوان الرئيسي</th>
                                    <th>الوصف</th>
                                    <th>التحكم</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sliders as $index => $slider)
                                    <tr>
                                        <td>{{$index + 1}}</td>
                                        <td><img src="{{$slider->image_path}}" class="img-thumbnail" style="width: 250px"></td>
                                        <td>{{$slider->sub_title}}</td>
                                        <td>{{$slider->title}}</td>
                                        <td>{!! $slider->description !!}</td>
                                        <td>
                                                <a href="{{route('slider-edit',$slider->id)}}" class="btn btn-info btn-sm">تعديل</a>
                                                <form action="{{route('slider-destroy',$slider->id)}}" method="post" style="display: inline-block">
                                                    {{csrf_field()}}
                                                    {{method_field('delete')}}
                                                    <button type="submit" class="btn btn-danger delete btn-sm">حذف</button>
                                                </form>

                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div style="text-align: center">{{$sliders->appends(request()->query())->links()}}</div>

                        @else

                            <h3>عذرا لايوجد سجلات</h3>
                        @endif
                    </div>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </div>

@endsection