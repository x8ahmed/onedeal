@extends('hepta.layouts.master')
@section('content')
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Breadcrumbs Start -->
        <div class="rs-breadcrumbs sec-color">
            <div class="breadcrumbs-image">
                <img src="{{asset('hepta/images/breadcrumbs/about.jpg')}}" alt="Breadcrumbs Image">
                <div class="breadcrumbs-inner">
                    <div class="container">
                        <div class="breadcrumbs-text">
                            <h1 class="breadcrumbs-title">About</h1>
                            <ul class="breadcrumbs-subtitle">
                                <li><a href="{{route('index')}}"><i class="fa fa-home"></i>  Home</a></li>
                                <li>About</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs End -->

        <!-- HOW WE WORK Start -->
        <div class="how-we-work defult-style pt-100 pb-100">
            <div class="container">
                <div class="work-sec-gallery">
                    <div class="row">
                        @php $about_uss = \App\Feature::where('type','about-us')->get() @endphp
                        @foreach($about_uss as $a)
                        <div class="col-md-4 mb-md-30">
                            <div class="work-column">
                                <div class="common-box">
                                    <img src="{{$a->image_path}}" alt="">
                                </div>
                                <div class="work-gallery-caption">
                                    <h4><a href="#">{{$a->head}}</a></h4>
                                    <p>
                                        {!! $a->description !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        {{--<div class="col-md-4">--}}
                            {{--<div class="work-column">--}}
                                {{--<div class="common-box">--}}
                                    {{--<img src="images/work/3.jpg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="work-gallery-caption">--}}
                                    {{--<h4><a href="#">Our Mission</a></h4>--}}
                                    {{--<p>--}}
                                        {{--Duis eleifend molestie leo, at mollis eros rutrum sit amet. Nam venenatis enim at magna euisei mod congue Mode.--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4 col-sm-6 hidden-sm">--}}
                            {{--<div class="work-column">--}}
                                {{--<div class="common-box">--}}
                                    {{--<img src="images/work/1.jpg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="work-gallery-caption">--}}
                                    {{--<h4><a href="#">What We Do</a></h4>--}}
                                    {{--<p>--}}
                                        {{--Duis eleifend molestie leo, at mollis eros rutrum sit amet. Nam venenatis enim at magna euisei mod congue Mode.--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- HOW WE WORK END -->

        <!-- About Section Start -->
        <section id="rs-about-4" class="rs-about-4 pb-240">
            <div class="row rs-vertical-middle sec-color">
                <div class="col-lg-6 col-md-12 padding-0">
                    <div class="title">
                        <h3>Our Success Story</h3>
                        <p class="mb-25">Suspendisse ex neque, sollicitudin in velit eu, luctus gravida nunc. Nulla pul-vinar risus sed metus euismod sodales ut sed nisi. Nulla posuere suscipit finibus. Quisque placerat vitae lacus ut scelerisque. Fusce luctus odio ac nibh luctus, in porttitor lacus egestas. Nunc erat libero. Free online Lorem Ipsum dummy text generator with great features.</p>
                        <p>Suspendisse ex neque, sollicitudin in velit eu, luctus gravida nunc. Quisque placerat vitae lacus ut scelerisque. Fusce luctus odio ac nibh luctus, in porttitor lacus egestas. Dummy text generator with great features.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 padding-0">
                    <div class="video-section-area">
                        <div class="image-here">
                            <img src="{{asset(\App\Site_setting::where('name','why_choose_us_picture')->first()->value)}}" alt="">
                            <div class="video-icon-here">
                                <a class="popup-videos animated pulse" href="{{\App\Site_setting::where('name','why_choose_us_video')->first()->value}}" title="Video Icon">
                                    <span></span>
                                </a>
                            </div>
                        </div>
                        <div class="overly-border"></div>
                    </div>
                </div>

            </div>
            <div class="row rs-vertical-middle">
                <div class="col-lg-6 col-md-12 hidden-md padding-0">
                    <div class="image-here">
                        <img src="{{asset('hepta/images/about/5.jpg')}}" alt="About Image">
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 services-responsibiity">
                    <div class="title pb-0">
                        <div class="sec-title">
                            <h3>{{\App\Site_setting::where('name','why_choose_us')->first()->value}}</h3>
                        </div>
                        @php $whys = \App\Feature::where([
                            ['type','why_choose_us']
                            ])->get(); @endphp
                        @foreach($whys as $why)
                            <div class="services-item">
                                <div class="service-mid-icon">
                                    <a href="#">
                                        <span class="service-mid-icon-container">
                                            <i class="{{$why->picture}}" aria-hidden="true" style="line-height: unset"></i>
                                        </span></a>
                                </div>
                                <div class="services-desc">
                                    <h3 class="services-title"><a href="javascript:void (0)">{{$why->head}}</a></h3>
                                    <p>{!! $why->description !!}</p>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </section>
        <!-- About Section End -->

        <!-- Partner Start -->
        <div id="rs-defult-partner" class="rs-defult-partner about-partner sec-color sec-spacer">
            <!-- Counter Up Section Start Here-->
            <div class="counter-top-area defult-style">
                <div class="container">
                    <div class="row rs-count">
                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration=".3s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="W" class="icon"></i>
                                <h3 class="rs-counter">{{\App\Site_setting::where('name','clients_no')->first()->value}}</h3>
                                <h4>Happy Client</h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->

                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6  wow fadeInUp" data-wow-duration=".7s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="C" class="icon"></i>
                                <h3 class="rs-counter">{{\App\Site_setting::where('name','projects_done')->first()->value}}</h3>
                                <h4>Project Done </h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->

                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration=".9s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="P" class="icon"></i>
                                <h3 class="rs-counter">{{\App\Site_setting::where('name','awards')->first()->value}}</h3>
                                <h4>Awards Won</h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->

                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="&#xe001;" class="icon"></i>
                                <h3 class="rs-counter">{{\App\Site_setting::where('name','exp')->first()->value}}</h3>
                                <h4 class="last">Experience Year</h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->
                    </div>
                </div>
            </div>
            <!-- Counter Down Section End Here-->

            <div class="container">
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="5" data-margin="30" data-autoplay="true" data-autoplay-timeout="8000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="2" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="5" data-md-device-nav="true" data-md-device-dots="false">
                    @php $clients = \App\Client::all(); @endphp
                    @foreach($clients as $client)
                        <div class="partner-item">
                            <a href="javascript:void (0)"><img src="{{$client->image_path}}" alt="Partner Image"></a>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        <!-- Partner End -->
    </div>
@endsection