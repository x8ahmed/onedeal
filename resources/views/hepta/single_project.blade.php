@extends('hepta.layouts.master')
@section('content')
    <!-- Main content Start -->
    <section class="main-content">
        <!-- Breadcrumbs Start -->
        <div class="rs-breadcrumbs sec-color">
            <div class="breadcrumbs-image">
                <img src="{{asset('hepta/images/breadcrumbs/project-slider.jpg')}}" alt="Breadcrumbs Image">
                <div class="breadcrumbs-inner">
                    <div class="container">
                        <div class="breadcrumbs-text">
                            <h1 class="breadcrumbs-title">{{$project->head}}</h1>
                            <ul class="breadcrumbs-subtitle">
                                <li><a href="{{route('index')}}"><i class="fa fa-home"></i>  Home</a></li>
                                <li>{{$project->head}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs End -->

        <!-- Project style Start -->
        <div id="rs-project-style" class="rs-project-style pt-100 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-12 mb-md-30">
                        <div class="project-desc">
                            <h3>PROJECT DESCRIPTION</h3>
                            {!! $project->description !!}
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-12">
                        <div class="project-img">
                            <img src="{{$project->image_path}}" alt="Project Image">
                        </div>
                        <div class="ps-informations">
                            <h4 class="info-title">Project Information</h4>
                            @php $info = \App\Project_information::where('project_id',$project->id)->first(); @endphp
                            <ul>
                                <li><span>Client:  </span>{{$info->client}}</li>
                                <li><span>Completed Date:  </span>{{$info->completed_date}}</li>
                                <li><span>Contract type: </span>{{$info->contract_type}}</li>
                                <li><span>Project Location: </span>{{$info->project_location}}</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Project Gallery End -->
    </section>
@endsection
