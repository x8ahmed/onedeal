@extends('hepta.layouts.master')
@section('content')
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Breadcrumbs Start -->
        <div class="rs-breadcrumbs sec-color">
            <div class="breadcrumbs-image">
                <img src="{{asset('hepta/images/breadcrumbs/project-slider.jpg')}}" alt="Breadcrumbs Image">
                <div class="breadcrumbs-inner">
                    <div class="container">
                        <div class="breadcrumbs-text">
                            <h1 class="breadcrumbs-title">{{$post->head}}</h1>
                            <ul class="breadcrumbs-subtitle">
                                <li><a href="{{route('index')}}"><i class="fa fa-home"></i>  Home</a></li>
                                {{--<li><a href="blog.html"> Blog</a></li>--}}
                                <li>{{$post->head}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs End -->

        <!-- Blog Detail Start -->
        <div class="rs-blog-details sec-spacer">
            <div class="container">
                <div class="full-width-blog">
                    <div class="h-img">
                        <img src="{{$post->image_path}}" class="img-responsive img-thumbnail" alt="Blog Image">
                    </div>

                    <div class="h-info">
                        <ul class="h-meta">

                            <li>
                                    <span class="p-date">
                                        <i class="fa fa-calendar"></i>{{$post->dateOfPublich}}
                                    </span>
                            </li>


                        </ul>
                    </div>

                    <div class="h-desc">
                        {!! $post->body !!}
                    </div>


                </div>
            </div>
        </div>
        <!-- Blog Detail End -->
    </div>
@endsection