<!DOCTYPE html>
<html lang="en">

<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>{{\App\Site_setting::where('name','site_name')->first()->value}}</title>
    <meta name="description" content="{{\App\Site_setting::where('name','site_description')->first()->value}}">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
{{--<!--    <link rel="apple-touch-icon" href="../../../../www.rstheme.com/products/html/hepta/apple-touch-icon.html">-->--}}
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('hepta/images/fav.png')}}">
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/css/bootstrap.min.css')}}">
    <!-- font-awesome css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/css/font-awesome.min.css')}}">
    <!-- animate css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/css/animate.css')}}">
    <!-- owl.carousel css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/css/owl.carousel.css')}}">
    <!-- slick css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/css/slick.css')}}">
    <!-- off canvas css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/css/off-canvas.css')}}">
    <!-- linea-font css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/fonts/linea-fonts.css')}}">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/inc/custom-slider/css/nivo-slider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/inc/custom-slider/css/preview.css')}}">
    <!-- magnific popup css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/css/magnific-popup.css')}}">
    <!-- Main Menu css -->
    <link rel="stylesheet" href="{{asset('hepta/css/rsmenu-main.css')}}">
    <!-- rsmenu transitions css -->
    <link rel="stylesheet" href="{{asset('hepta/css/rsmenu-transitions.css')}}">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/style.css')}}"> <!-- This stylesheet dynamically changed from style.less -->
    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="{{asset('hepta/css/responsive.css')}}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="defult-home">
<!-- Preloader area start here -->
<div id="loading">
    <div id="loading-center">
        <div id="loading-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
        </div>
    </div>
</div>
<!--End preloader here -->

<!--Header Start-->
<header id="rs-header" class="rs-header">
    <!-- Toolbar Start -->
    <div class="toolbar-area hidden-md">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="toolbar-contact">
                        <ul>
                            <li><i class="fa fa-envelope-o"></i><a href="mailto:{{\App\Site_setting::where('name','mail')->first()->value}}">{{\App\Site_setting::where('name','mail')->first()->value}}</a></li>

                            <li><i class="fa fa-phone"></i><a href="tel:{{\App\Site_setting::where('name','phone')->first()->value}}">{{\App\Site_setting::where('name','phone')->first()->value}}</a></li>

                            <li><i class="fa fa-location-arrow"></i><p>{{\App\Site_setting::where('name','location')->first()->value}}</p></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="toolbar-sl-share">
                        <ul>
                            <li><a href="{{\App\Site_setting::where('name','facebook')->first()->value}}"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{\App\Site_setting::where('name','twitter')->first()->value}}"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="{{\App\Site_setting::where('name','instegram')->first()->value}}"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="{{\App\Site_setting::where('name','youtube')->first()->value}}"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Toolbar End -->

    <!-- Header Menu Start -->
    <div class="menu-area rs-defult-header menu-sticky">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="logo-area">
                        <a href="{{route('index')}}"><img src="{{asset(\App\Site_setting::where('name','logo')->first()->value)}}" alt="logo"></a>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="main-menu">
                        <a class="rs-menu-toggle"><i class="fa fa-bars"></i>Menu</a>
                        <nav class="rs-menu">
                            <ul class="nav-menu">
                                <!-- Home -->
                                <li class="rs-mega-menu current-menu-item"><a class="active" href="{{route('index')}}">Home</a></li>
                                <!-- End Home -->
                                
                                <li class="menu-item"><a href="{{route('about')}}">About</a></li>
                                <li class="menu-item"><a href="{{route('projects')}}">Projects</a></li>
                                <li class="menu-item"><a href="{{route('services')}}">Services</a></li>


                            </ul>
                        </nav>
                    </div>
                    <div class="appointment-cart hidden-md">
                        <ul class="cart">
                            <style>
                                .cart li:before{display: none !important;}
                            </style>
                            {{--<li class="search"><i class="fa fa-search"></i></li>--}}
                            <li><a id="nav-expander" class="nav-expander"><i class="fa fa-bars fa-lg white"></i></a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Menu End -->

    <!-- Canvas Menu start -->
    <nav class="right_menu_togle">
        <div class="close-btn"><span id="nav-close" class="text-center"><i class="fa fa-close"></i></span></div>
        <div class="canvas-logo">
            <a href="{{route('index')}}"><img src="{{asset(\App\Site_setting::where('name','logo')->first()->value)}}" alt="logo"></a>
        </div>
        <ul class="sidebarnav_menu list-unstyled main-menu">
            <!--Home Menu Start-->
            <li><a href="{{route('index')}}">Home</a></li>
            <!--Home Menu End-->

            <!--About Menu Start-->
            <li><a href="{{route('about')}}">About</a></li>
            <!--About Menu End-->

            <!--Services Menu Start-->
            <li><a href="{{route('services')}}">Services</a></li>
            <!--Services Menu End-->

            <!--Blog Menu Star-->
            <li><a href="{{route('projects')}}">Projects</a></li>
            <!--Blog Menu End-->

        </ul>
        <div class="canvas-contact">
            <h5 class="canvas-contact-title">Contact Info</h5>
            <ul class="contact">
                <li><i class="fa fa-map-marker"></i><a href="javascript:void (0)">{{\App\Site_setting::where('name','location')->first()->value}}</a></li>
                <li><i class="fa fa-phone"></i><a href="javascript:void (0)">{{\App\Site_setting::where('name','phone')->first()->value}}</a></li>
                <li><i class="fa fa-envelope-o"></i><a href="javascript:void (0)">{{\App\Site_setting::where('name','mail')->first()->value}}</a></li>
            </ul>
            <ul class="social">
                <li><a href="{{\App\Site_setting::where('name','facebook')->first()->value}}"><i class="fa fa-facebook"></i></a></li>
                <li><a href="{{\App\Site_setting::where('name','twitter')->first()->value}}"><i class="fa fa-twitter"></i></a></li>
                <li><a href="{{\App\Site_setting::where('name','instegram')->first()->value}}"><i class="fa fa-instagram"></i></a></li>
                <li><a href="{{\App\Site_setting::where('name','youtube')->first()->value}}"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div>
    </nav>
    <!-- Canvas Menu end -->
</header>
<!--Header End-->

@yield('content')

<!-- Footer Start -->
<footer id="rs-footer" class="rs-footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 mb-md-30">
                    <div class="about-widget">
                        <a href="{{route('index')}}">
                            <img src="{{asset(\App\Site_setting::where('name','logo_footer')->first()->value)}}" alt="Footer Logo">
                        </a>
                        <ul class="footer-address">
                            <li><i class="fa fa-map-marker"></i><a href="javascript:void (0)">{{\App\Site_setting::where('name','location')->first()->value}}</a></li>
                            <li><i class="fa fa-phone"></i><a href="javascript:void (0)">{{\App\Site_setting::where('name','phone')->first()->value}}</a></li>
                            <li><i class="fa fa-envelope-o"></i><a href="javascript:void (0)">{{\App\Site_setting::where('name','mail')->first()->value}}</a></li>
                            {{--<li><i class="fa fa-clock-o"></i><p class="mb-0">Opening Hours: 8.30 AM – 7.00 PM</p></li>--}}
                        </ul>
                        <ul class="social-links">
                            <li><a href="{{\App\Site_setting::where('name','facebook')->first()->value}}"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{\App\Site_setting::where('name','twitter')->first()->value}}"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="{{\App\Site_setting::where('name','instegram')->first()->value}}"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="{{\App\Site_setting::where('name','youtube')->first()->value}}"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 mb-md-30">
                    <h5 class="footer-title">RECENT POSTS</h5>
                    <div class="recent-post-widget">
                        @php $posts = \App\Blog::orderBy('id','desc')->take(3)->get(); @endphp
                        @foreach($posts as $post)
                        <div class="post-item mb-30">
                            <div class="post-image">
                                <img src="{{$post->image_path}}" alt="{{$post->head}}">
                            </div>
                            <div class="post-desc">
                                <a href="#">{{$post->head}}</a>
                                <span><i class="fa fa-calendar"></i> {{$post->dateOfPublich}} </span>
                            </div>
                        </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="ft-bottom-right">
                <div class="footer-bottom-share">
                    <ul>
                        <li class="active"><a href="{{route('index')}}">Home</a></li>
                        <li><a href="{{route('about')}}">About</a></li>
                        <li><a href="{{route('projects')}}">Projects</a></li>
                        <li><a href="{{route('services')}}">Services</a></li>
                    </ul>
                </div>
            </div>
            <div class="copyright">
                <p>&copy; {{date('Y')}} All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->

<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>

<!-- modernizr js -->
<script src="{{asset('hepta/js/modernizr-2.8.3.min.js')}}"></script>
<!-- jquery latest version -->
<script src="{{asset('hepta/js/jquery.min.js')}}"></script>
<!-- bootstrap js -->
<script src="{{asset('hepta/js/bootstrap.min.js')}}"></script>
<!-- Menu js -->
<script src="{{asset('hepta/js/rsmenu-main.js')}}"></script>
<!-- op nav js -->
<script src="{{asset('hepta/js/jquery.nav.js')}}"></script>
<!-- owl.carousel js -->
<script src="{{asset('hepta/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('hepta/js/slick.min.js')}}"></script>
<!-- isotope.pkgd.min js -->
<script src="{{asset('hepta/js/isotope.pkgd.min.js')}}"></script>
<!-- imagesloaded.pkgd.min js -->
<script src="{{asset('hepta/js/imagesloaded.pkgd.min.js')}}"></script>
<!-- wow js -->
<script src="{{asset('hepta/js/wow.min.js')}}"></script>

<!-- Skill bar js -->
<script src="{{asset('hepta/js/skill.bars.jquery.js')}}"></script>
<script src="{{asset('hepta/js/jquery.counterup.min.js')}}"></script>
<!-- counter top js -->
<script src="{{asset('hepta/js/waypoints.min.js')}}"></script>
<!-- video js -->
<script src="{{asset('hepta/js/jquery.mb.YTPlayer.min.js')}}"></script>
<!-- magnific popup -->
<script src="{{asset('hepta/js/jquery.magnific-popup.min.js')}}"></script>
<!-- Nivo slider js -->
<script src="{{asset('hepta/inc/custom-slider/js/jquery.nivo.slider.js')}}"></script>
<!-- plugins js -->
<script src="{{asset('hepta/js/plugins.js')}}"></script>
<!-- contact form js -->
<script src="{{asset('hepta/js/contact.form.js')}}"></script>
<!-- main js -->
<script src="{{asset('hepta/js/main.js')}}"></script>
</body>

<!-- Mirrored from rstheme.com/products/html/hepta/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Mar 2019 12:49:25 GMT -->
</html>