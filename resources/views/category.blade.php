@extends('layouts.master')
@section('main')
    <div class="container">
        <header class="page-header">
            <h1 class="page-title">{{$cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</h1>
            <ol class="breadcrumb page-breadcrumb">
                <li><a href="{{route('index')}}">{{trans('index.home')}}</a>
                </li>
                @php
                if ($cat->parent_id !=0){
                   $p_name = \App\Category::find($cat->parent_id)['name_'.\Illuminate\Support\Facades\Lang::getLocale()];
                }
                @endphp
                @if($cat->parent_id !=0)
                <li><a href="{{route('tags',str_replace(' ', '-',$p_name))}}">{{$p_name}}</a>
                </li>
                @endif
                <li class="active">{{$cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</li>
            </ol>
            <ul class="category-selections clearfix">

                <li><span class="category-selections-sign">{{trans('index.all_categories')}}</span>
                    @php $parent_cats = \App\Category::where('parent_id',0)->get(); @endphp
                    <select class="category-selections-select" id="agileinfo-nav_search">
                        @foreach($parent_cats as $parent_cat)
                        <optgroup label="{{$parent_cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}">
                            @php $parent_cats_childs = \App\Category::where('parent_id',$parent_cat->id)->get(); @endphp
                            @foreach($parent_cats_childs as $child)

                            <option value="{{str_replace(' ', '-',$child['name_'.\Illuminate\Support\Facades\Lang::getLocale()])}}">{{$child['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</option>

                                @endforeach
                        </optgroup>
                        @endforeach

                    </select>
                </li>
            </ul>
        </header>
        <div class="row" data-gutter="15">
            @if(isset($products))
            @foreach($products as $product)
            <div class="col-md-3">
                <div class="product ">
                    <ul class="product-labels"></ul>
                    <div class="product-img-wrap">
                        <img class="product-img-primary" src="{{asset($product->thumbnail)}}" alt="Image Alternative text" title="Image Title" />
                        <img class="product-img-alt" src="{{asset($product->thumbnail)}}" alt="Image Alternative text" title="Image Title" />
                    </div>
                    <a class="product-link" href="{{route('single_product',$product->id)}}"></a>
                    <div class="product-caption">

                        <h5 class="product-caption-title">{{$product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</h5>
                        <div class="product-caption-price"><span class="product-caption-price-new">{{$product->price.trans('index.price')}}</span>
                        </div>

                    </div>
                </div>
            </div>
            @endforeach
            @endif

                @if(isset($texts))
                    @foreach($texts as $text)
                        <div class="col-md-3">
                            <div class="product ">
                                <ul class="product-labels"></ul>
                                <div class="product-img-wrap">
                                    <img class="product-img-primary" src="{{asset($text->text)}}" alt="Image Alternative text" title="Image Title" />
                                    <img class="product-img-alt" src="{{asset($text->text)}}" alt="Image Alternative text" title="Image Title" />
                                </div>
                                <a class="product-link" href="{{route('single_product',[$text->id,'text'])}}"></a>
                                <div class="product-caption">

                                    {{--<h5 class="product-caption-title">{{$product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</h5>--}}
                                    {{--<div class="product-caption-price"><span class="product-caption-price-new">{{$product->price.trans('index.price')}}</span>--}}
                                    </div>

                                </div>
                            </div>
                    @endforeach
                @endif
        </div>


        <div class="row">

            <div class="col-md-6">
                <nav>
                    @if(isset($products))
                    <ul class="pagination category-pagination pull-right">
                        @for($x=1;$x<$products->lastPage();$x++)
                        <li class="{{($x==1)?"active":""}}"><a href="{{\Illuminate\Support\Facades\URL::current().'?page='.$x}}">{{$x}}</a>
                        </li>
                        @endfor
                        <li class="last"><a href="{{\Illuminate\Support\Facades\URL::current().'?page='.$products->lastPage()}}"><i class="fa fa-long-arrow-right"></i></a>
                        </li>
                    </ul>
                    @endif

                        @if(isset($texts))
                            <ul class="pagination category-pagination pull-right">
                                @for($x=1;$x<$texts->lastPage();$x++)
                                    <li class="{{($x==1)?"active":""}}"><a href="{{\Illuminate\Support\Facades\URL::current().'?page='.$x}}">{{$x}}</a>
                                    </li>
                                @endfor
                                <li class="last"><a href="{{\Illuminate\Support\Facades\URL::current().'?page='.$texts->lastPage()}}"><i class="fa fa-long-arrow-right"></i></a>
                                </li>
                            </ul>
                        @endif
                </nav>
            </div>
        </div>
    </div>
    <div class="gap"></div>

@endsection