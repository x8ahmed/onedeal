@extends('layouts.master')
@section('main')

    <!-- top Products -->
    <div class="ads-grid py-sm-5 py-4 orderss">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l mb-lg-5 mb-sm-4 mb-3">
                <!--<span>O</span>ur-->
                <!--<span>N</span>ew-->
                <!--<span>P</span>roducts-->
                نتائج البحث
            </h3>
            <!-- //tittle heading -->
            <div class="row">
                <!-- product left -->
                <div class="agileinfo-ads-display col-lg-12">
                    <div class="wrapper">
                        <!-- first section -->
                        <div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
                            <!--<h3 class="heading-tittle text-center font-italic">New Brand Mobiles</h3>-->
                            <div class="row">
                                @if(count($products))
                                @foreach($products as $product)
                                        <div class="col-md-3">
                                            <div class="product ">
                                                <ul class="product-labels"></ul>
                                                <div class="product-img-wrap">
                                                    <img class="product-img-primary" src="{{asset($product->thumbnail)}}" alt="Image Alternative text" title="Image Title" />
                                                    <img class="product-img-alt" src="{{asset($product->thumbnail)}}" alt="Image Alternative text" title="Image Title" />
                                                </div>
                                                <a class="product-link" href="{{route('single_product',$product->id)}}"></a>
                                                <div class="product-caption">

                                                    <h5 class="product-caption-title">{{$product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]}}</h5>
                                                    <div class="product-caption-price"><span class="product-caption-price-new">{{$product->price.trans('index.price')}}</span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                            @endforeach
                                    @else

                                    <div class="col-md-12 alert alert-warning text-center" style="padding: 20px;">
                                        <strong>عفوا!</strong> لا توجد نتائج مطابقة
                                    </div>
                                    @endif

                            </div>
                        </div>

                    </div>
                </div>
                <!-- //product left -->


            </div>
        </div>
    </div>
    <!-- //top products -->
@endsection