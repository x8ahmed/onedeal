@extends('layouts.master')

@section('main')
    <div class="container-fluid auth-form">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <div class="card login-box">
                    <div class="card-header">{{trans('index.update_profile')}}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('update_user_profile') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{trans('index.name')}}</label>

                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ \Auth::user()->name }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{trans('index.email_m')}}</label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ \Auth::user()->email }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{trans('index.password')}}</label>

                                <div class="col-md-8">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                    <span class="text-danger">{{trans('index.password_change')}}</span>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{trans('index.password_confirmation')}}</label>

                                <div class="col-md-8">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                    <span class="text-danger">{{trans('index.password_change')}}</span>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">{{trans('index.address')}}</label>

                                <div class="col-md-8">
                                    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ \Auth::user()->address }}" required autofocus>
                                    <a href="javascript:void (0)" class="view-map">
                                        <i class="fa fa-map-marker"></i>
                                    </a>
                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                    <div class="modal-body" id="register_map" style="position: absolute;margin-top: 5px;z-index: 999;height: 250px;width:100%;">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">{{trans('index.mobile')}}</label>

                                <div class="col-md-8">
                                    <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ \Auth::user()->phone }}" required autofocus>

                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <input type="hidden" name="latitude" id="latitude" value="{{Auth::user()->latitude}}">
                            <input type="hidden" name="longitude" id="longitude" value="{{Auth::user()->longitude}}">

                            <div class="form-group row mb-0">
                                <button type="submit" class="btn btn-primary">
                                    {{trans('index.update_profile')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@section('script')
    <script>
        var register_map = L.map('register_map').setView([24.774265, 46.738586], 8);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox.streets'
        }).addTo(register_map);
        @if (Auth::user()->latitude)
        var register_marker = L.marker(['{{Auth::user()->latitude}}', '{{Auth::user()->longitude}}'],{draggable: true}).addTo(register_map);

        @else
        var register_marker = L.marker([24.774265, 46.738586],{draggable: true}).addTo(register_map);

        @endif
        register_marker.on('dragend', function (e) {
            console.log(register_marker.getLatLng().lat);
            console.log(register_marker.getLatLng().lng);
            document.getElementById('latitude').value = register_marker.getLatLng().lat;
            document.getElementById('longitude').value = register_marker.getLatLng().lng;
        });


    </script>
    <script>
        $('#register_map').fadeOut();
        $('.view-map').click(function () {
            $('#register_map').fadeToggle();
        });
    </script>
@endsection
