<?php

return [

    'header_text' => 'كل شىء تحتاجه هو تذكار',
    'price' => 'ريال',
    'language' => 'اللغة',
    'sign_in' => 'تسجيل الدخول',
    'register' => 'تسجيل جديد',
    'search' => 'بحث',
    'all_categories' => 'جميع التصنيفات',
    'home' => 'الرئيسية',
    'new_products' => 'احدث المنتجات',
    'add_to_cart' => 'اضف الى العربة',
    'email' => 'البريد الالكترونى/الهاتف',
    'password' => 'كلمة المرور',
    'remember_me' => 'تذكرنى',
    'shop_now' => 'تسوق الان',
    'similar_products' => 'منتجات مشابهة',
    'shipping' => 'وسيلة الشحن',
    'price2' => 'رسوم الشحن',
    'tax' => 'الضريبة',
    'coupon' => 'كود الخصم',
    'apply' => 'تحقق',
    'payment_method' => 'وسيلة الدفع',
    'shopping_cart' => 'عربة التسوق',
    'chechout_product' => 'المنتج',
    'chechout_price' => 'السعر',
    'chechout_title' => 'الاسم',
    'chechout_suggested_text' => 'النص المقترح',
    'chechout_total' => 'الاجمالى',
    'chechout_quantity' => 'الكمية',
    'chechout_checkout' => 'تاكيد الشراء',
    'chechout_notes' => 'ملاحظات',
    'chechout_attachements' => 'مرفقات',
    'other_notes' => 'ملاحظات اخرى',
    'other_notes_description' => 'اذا كنت تود الشحن الى عنوان اخر او يوجد اى ملاحظات اضافية',
    'hq' => 'المقر الرئيسى',
    'copyright' => 'حقوق الملكية © تذكار '.date('Y').'.  تصميم وبرمجة kw4s.com',
    'choosed_text' => 'النص المختار',
    'close' => 'اغلاق',
    'name' => 'الاسم',
    'email_m' => 'البريد الالكترونى',
    'password_confirmation' => 'تأكيد كلمة المرور',
    'address' => 'العنوان',
    'mobile' => 'رقم الجوال',
    'my_orders' => 'طلباتى',
    'logout' => 'تسجيل الخروج',
    'update_profile' => 'تعديل الحساب',
    'password_change' => 'اذا لم ترد تغيير كلمة المرور اتركها فارغة',
    'profile_update_successfully' => 'تم تحديث البيانات بنجاح',
    'address_placeholder'=>'الدولة/المدينة/الشارع/الرقم البريدى',
    'new_text'=>'احدث النصوص',
    'out_of_stock'=>'غير متوفر',








];
