<?php

return [

    'header_text' => 'Everything You Need is tezkar',
    'price' => 'SR',
    'language' => 'Language',
    'sign_in' => 'Sign in',
    'register' => 'Register',
    'search' => 'Search',
    'all_categories' => 'All Categories',
    'home' => 'Home',
    'new_products' => 'New Products',
    'add_to_cart' => 'Add to cart',
    'email' => 'Email/phone',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'shop_now' => 'SHOP NOW',
    'similar_products' => 'Similar Products',
    'shipping' => 'Shipping',
    'price2' => 'shipping fees',
    'tax' => 'Tax',
    'coupon' => 'Coupon',
    'apply' => 'Apply',
    'payment_method' => 'Payment',
    'shopping_cart' => 'Shopping cart',
    'chechout_product' => 'product',
    'chechout_price' => 'price',
    'chechout_title' => 'Title',
    'chechout_suggested_text' => 'Suggested Text',
    'chechout_total' => 'Total',
    'chechout_quantity' => 'Quantity',
    'chechout_checkout' => 'Checkout',
    'chechout_notes' => 'notes',
    'chechout_attachements' => 'Attachements',
    'other_notes' => 'other notes',
    'other_notes_description' => 'if you want to deliver to another address or anything else',
    'hq' => 'Headquarter',
    'copyright' => 'Copyright © tezkar '.date('Y').'. All rights reseved',
    'choosed_text' => 'Selected text',
    'close' => 'Close',
    'name' => 'Name',
    'email_m' => 'Email',
    'password_confirmation' => 'Password Confirmation',
    'address' => 'Address',
    'mobile' => 'Mobile',
    'my_orders' => 'My orders',
    'logout' => 'Logout',
    'update_profile' => 'update profile',
    'password_change' => 'Leave it empty if you do not want to change password',
    'profile_update_successfully' => 'profile updated successfully',
    'address_placeholder' => 'country/city/street/zip code',
    'new_text' => 'New text',
    'out_of_stock'=>'out of stock',









];
