<?php

use Illuminate\Database\Seeder;

class ShippingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('shippings')->insert([
            'type' => 'استلام من المقر الرئيسى',
            'parent_id' => 0,
            'price' => 0,
        ]);

        DB::table('shippings')->insert([
            'type' => 'توصيل داخل الجبيل الصناعية',
            'parent_id' => 0,
            'price' => 0,
        ]);
        DB::table('shippings')->insert([
            'type' => 'توصيل للجبيل البلد',
            'parent_id' => 0,
            'price' => 0,
        ]);
        DB::table('shippings')->insert([
            'type' => 'توصيل لمدن ومناطق المنطقة الشرقية',
            'parent_id' => 0,
            'price' => 0,
        ]);
        DB::table('shippings')->insert([
            'type' => 'باقى مدن المملكة',
            'parent_id' => 0,
            'price' => 0,
        ]);
        $cities = \App\City::all();
        foreach ($cities as $city){
            DB::table('shippings')->insert([
                'type' => $city->nameAr,
                'parent_id' => 5,
                'price' => 0,
            ]);
        }
    }
}
