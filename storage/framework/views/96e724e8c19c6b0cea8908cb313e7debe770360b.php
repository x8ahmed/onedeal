<?php $__env->startSection('main'); ?>

    <div class="row">
        <div class="col-md-12 pull-right">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">تعديل اسلايدر</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        
                        
                        
                    </div>
                    <?php echo $__env->make('admin.layouts.massege', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <form action="<?php echo e(route('features-update',$feature->id)); ?>" method="post" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('put')); ?>



                        <div class="form-group">
                            <label for="first_name">لاختيار صورة</label>
                            <input type="radio" name="image" value="image" class="dd">
                            <input type="file" class="form-control img-responsive img-thumbnail image" name="image" id="image">
                        </div>
                        <div class="form-group">
                            <img src="<?php echo e($feature->image_path); ?>" style="width: 120px;" class="image-reviw"/>
                        </div>
                        <div class="form-group">
                            <label for="first_name">لاختيار ايقونة</label>
                            <input type="radio" name="image" value="icon" class="dd">
                            <input type="text" class="form-control hidden" name="icon" id="icon" value="<?php echo e($feature->picture); ?>">
                        </div>

                        <div class="form-group">
                            <label for="sub_title">النوع</label>
                            <select name="type" class="form-control" id="type">
                                <option value=""> اختر النوع</option>
                                <option value="about-us" <?php echo e($feature->type == 'about-us' ? 'selected' : ''); ?>>about-us</option>
                                <option value="service" <?php echo e($feature->type == 'service' ? 'selected' : ''); ?>>service</option>
                                <option value="project" <?php echo e($feature->type == 'project' ? 'selected' : ''); ?>>project</option>
                                <option value="why_choose_us" <?php echo e($feature->type == 'why_choose_us' ? 'selected' : ''); ?>>why_choose_us</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="first_name">العنوان </label>
                            <input type="text" class="form-control" name="head" value="<?php echo e($feature->head); ?>">
                        </div>

                        <div class="form-group">
                            <label for="first_name">الوصف</label>
                            <textarea class="form-control ckeditor" name="description" >
                            <?php echo e($feature->description); ?>

                            </textarea>
                        </div>

                        <div class="form-group">
                            <label for="first_name">الظهور في الرئيسية</label>
                            <input type="radio" name="show" value="0" <?php echo e($feature->show == '0' ? 'checked' : ''); ?>>لا
                            <input type="radio"  name="show" value="1" <?php echo e($feature->show == '1' ? 'checked' : ''); ?>>نعم
                        </div>
                        <div id="project_form" class="hidden">
                            <h3>معلومات المشاريع</h3>
                            <div class="form-group">
                                <label for="sub_title">العميل</label>
                                <input type="text" class="form-control" name="client" value="">
                            </div>

                            <div class="form-group">
                                <label for="sub_title">تاريخ الانتهاء</label>
                                <input type="text" class="form-control" name="completed_date" value="<?php echo e(old('completed_date')); ?>">
                            </div>

                            <div class="form-group">
                                <label for="sub_title">نوع التعاقد</label>
                                <input type="text" class="form-control" name="contract_type" value="<?php echo e(old('contract_type')); ?>">
                            </div>

                            <div class="form-group">
                                <label for="sub_title">موقع العمل</label>
                                <input type="text" class="form-control" name="project_location" value="<?php echo e(old('project_location')); ?>">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-md">تعديل</button>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $(document).ready(function () {
            $(document).on('change','#type',function () {
                var val =  $( "#type option:selected" ).val();

                if (val == 'project'){
                    $('#project_form').removeClass('hidden');
                }else {
                    $('#project_form').addClass('hidden');
                }

            });
        });
        $(document).on('click','.dd',function () {
            var radio = $("input[name='image']:checked").val();
            if (radio == 'icon'){
                $('#icon').removeClass('hidden');
                $('.file-caption-main').hide();
            }else {
                $('#icon').addClass('hidden');
                $('.file-caption-main').show();
            }
        });

    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>