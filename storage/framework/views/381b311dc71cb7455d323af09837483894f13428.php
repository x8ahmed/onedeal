
<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اضف نص مقترح</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="<?php echo e(route('save_text')); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="text">النص</label>
                                    <input type="file" name="text" id="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="category_id">اختار التصنيف </label>
                                    <div>
                                        <select class="form-control" id="category_id" name="category_id" required>
                                            <?php $parents = \App\Category::where([
                                            ['parent_id','!=',0],['cat_type','text']
                                            ])->get(); ?>
                                            <?php $__currentLoopData = $parents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($parent->id); ?>"><?php echo e($parent->name_ar); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div><!-- .form-group -->
                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>