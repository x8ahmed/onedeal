
<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <?php $__currentLoopData = $ads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <form action="<?php echo e(route('deleteItem',$ad->id)); ?>" method="post" id="form-<?php echo e($ad->id); ?>">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('delete')); ?>

                        <input type="hidden" name="class_name" value="<?php echo e(get_class($ad)); ?>">
                    </form>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <!-- DOM dataTable -->
                    <div class="col-md-12">
                        <div class="widget">
                            <header class="widget-header">
                                <h4 class="widget-title">الصور الاعلانية</h4>
                                <a class="btn btn-primary pull-left" href="<?php echo e(route('create_ad')); ?>">اضف صورة</a>
                            </header><!-- .widget-header -->
                            <hr class="widget-separator">
                            <div class="widget-body">
                                <div class="table-responsive">
                                    <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>الصورة</th>
                                            <th>حذف</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>الصورة</th>
                                            <th>حذف</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $__currentLoopData = $ads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($ad->id); ?></td>
                                            <?php $arr = ['mkv','ogg','mp4','flac','webM']; ?>
                                            <?php if(in_array(explode('.',$ad->picture)[count(explode('.',$ad->picture))-1],$arr)): ?>
                                            <td>

                                                <video height="100%" width="100%" controls autoplay loop muted playsinline>
                                                    <source src="<?php echo e(asset($ad->picture)); ?>" type="video/mp4">
                                                    <source src="<?php echo e(asset($ad->picture)); ?>" type="video/ogg">
                                                    <source src="<?php echo e(asset($ad->picture)); ?>" type="video/webM">
                                                    <source src="<?php echo e(asset($ad->picture)); ?>" type="video/flac">
                                                </video>
                                            </td>
                                                <?php else: ?>
                                                <td><img src="<?php echo e(asset("$ad->picture")); ?>" class="img-responsive"></td>

                                            <?php endif; ?>
                                            <td><button onclick="JSalert(<?php echo e($ad->id); ?>)"  id="<?php echo e($ad->id); ?>"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- .widget-body -->
                        </div><!-- .widget -->
                    </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>