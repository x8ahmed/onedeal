
<?php $__env->startSection('main'); ?>
    <form action="<?php echo e(route('user_checkout')); ?>" enctype="multipart/form-data" method="post">
        <?php echo e(csrf_field()); ?>

    <div class="container">
        <header class="page-header">
            <h1 class="page-title"><?php echo e(trans('index.shopping_cart')); ?></h1>
        </header>
        <input type="hidden" id="s_text" value="<?php echo e(\App\Category::where('cat_type','text')->get()); ?>">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table table-shopping-cart">
                    <thead>
                    <tr>
                        <th><?php echo e(trans('index.chechout_product')); ?></th>
                        <th><?php echo e(trans('index.chechout_title')); ?></th>
                        <th><?php echo e(trans('index.chechout_price')); ?></th>
                        <th><?php echo e(trans('index.chechout_quantity')); ?></th>
                        <th><?php echo e(trans('index.chechout_suggested_text')); ?></th>
                        <th><?php echo e(trans('index.chechout_notes')); ?></th>
                        <th><?php echo e(trans('index.chechout_attachements')); ?></th>
                        <th><?php echo e(trans('index.chechout_total')); ?></th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
                <div class="gap gap-small"></div>
            </div>
            <div class="col-md-12">
                <ul class="shopping-cart-total-list">
                    <li>
                        <span><?php echo e(trans('index.shipping')); ?></span>
                        <span>
                                <select required class="form-control" name="shipping_type" id="shipping" style="margin-bottom: 10px">
                                    <?php $shippings = \App\Shipping::where('parent_id',0)->get(); ?>
                                    <option selected disabled="disabled" data-shipping-price="" value="">اختر وسيلة الشحن</option>

                                    <?php $__currentLoopData = $shippings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shipping): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($shipping->id); ?>" data-shipping-price="<?php echo e($shipping->price); ?>"><?php echo e($shipping->type); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <select class="form-control" name="shipping_cities" id="cities" style="display: none">
                                    <?php $shippings = \App\Shipping::where('parent_id',5)->get(); ?>
                                                                <?php $__currentLoopData = $shippings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shipping): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($shipping->id); ?>" data-shipping-price="<?php echo e($shipping->price); ?>"><?php echo e($shipping->type); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                        </span>
                        <span><?php echo e(trans('index.price2')); ?>: <span id="shipping_price"></span></span>
                    </li>

                    <li>
                        <?php $tax = \App\Tax::first(); ?>
                        <span><?php echo e(trans('index.tax')); ?></span>
                        <span><?php echo e($tax->tax); ?>%</span>
                        <input type="hidden" value="<?php echo e($tax->tax); ?>" id="tax">
                    </li>
                    <li>
                        <span><?php echo e(trans('index.coupon')); ?></span>
                        <span><input type="text" class="form-control" name="coupon" id="coupon_value"></span>
                        <span><button type="button" class="btn btn-primary" id="coupon_submit"><?php echo e(trans('index.apply')); ?></button></span>
                    </li>
                    <li>
                        <span><?php echo e(trans('index.payment_method')); ?></span>
                        <span>
                               <select required class="form-control" name="payment_method" id="payment" style="margin-bottom: 10px">
                                        <option selected disabled="disabled" value="">اختر وسيلة الدفع</option>
                                        <option value="cash">نقدا فى الفرع</option>
                                        <option value="transfer">تحويل لاحد حسابات المؤسسة</option>
                                </select>
<div id="accounts_holder" style="display: none">
                                    <select class="form-control selectpicker" id="accounts" style="display: none">
                                    <option selected disabled="disabled">اختر الحساب</option>
                                        <?php $accounts = \App\BillingAccount::all(); ?>
                                        <?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($account->id); ?>" data-thumbnail="<?php echo e(asset($account->logo)); ?>"><?php echo e($account['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
</div>
                        </span>
                        <span>
                            <div class="col-md-12" id="receipt" style="display: none">

                                <?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="account_info" id="<?php echo e($account->id); ?>" style="display: none;">
                                        <p>رقم الحساب</p>
                                        <p><?php echo e($account->account_number); ?></p>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <label>ارفق صورة الايصال</label>
                                <input type="file" name="receipt" class="form-control">
                            </div>
                        </span>
                    </li>
                    <li>
                        <span><?php echo e(trans('index.other_notes')); ?></span>
                        <span><textarea class="form-control" rows="5" name="other_notes"><?php echo e(trans('index.other_notes_description')); ?></textarea></span>
                    </li>
                    <li><span><?php echo e(trans('index.chechout_total')); ?></span><span id="total"></span>
                    </li>
                </ul><input type="submit" class="btn btn-primary" value="<?php echo e(trans('index.chechout_checkout')); ?>">
            </div>
        </div>

    </div>
    <div class="gap"></div>
    <input type="hidden" id="products_total">
    <input type="hidden" id="total_after_tax" name="total_after_tax">
    </form>
    <!-- modal-->
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?php echo e(trans('index.choosed_text')); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="" id="selected_text_img" class="img-responsive">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(trans('index.close')); ?></button>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- modal-->


<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="https://thdoan.github.io/bootstrap-select/js/bootstrap-select.js"></script>
    <script>

        var text = JSON.parse($('#s_text').val());
        <?php $suggested_text_cats = \App\Category::where([['cat_type','text'],['parent_id','!=',0]])->get(); ?>
        <?php $suggested_text_parentcats = \App\Category::where([['cat_type','text'],['parent_id','=',0]])->get(); ?>
        <?php $__currentLoopData = $suggested_text_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $suggested_text_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $cat_products = \App\SuggestedText::where('category_id',$suggested_text_cat->id)->get(); ?>
        $('.suggested_text_td').append('<div class="s_cat s_cat_<?php echo e($suggested_text_cat->id); ?>" style="display:none;margin-top:5px"><select name="suggested_text[]" class="suggested_text selectpicker">\n');
        <?php $__currentLoopData = $cat_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                $('.suggested_text_td .suggested_text').append('' +
            '        <option style="color: white" data-thumbnail="<?php echo e(asset($cat_product->text)); ?>" value="<?php echo e($cat_product->id); ?>"><?php echo e($cat_product->id); ?></option>\n' +
            '    ');
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        $('.suggested_text_td').append('    </select></div>');
        console.log('<?php echo e($suggested_text_cat->name_ar); ?>');
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php $__currentLoopData = $suggested_text_parentcats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $suggested_text_parentcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php $suggested_text_parentcat_childs = \App\Category::where([['cat_type','text'],['parent_id','=',$suggested_text_parentcat->id]])->get(); ?>

        <?php $__currentLoopData = $suggested_text_parentcat_childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $suggested_text_parentcat_child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        $('.suggested_text_cats').append('<option value="<?php echo e($suggested_text_parentcat_child->id); ?>"><?php echo e($suggested_text_parentcat_child['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></option>');
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        // $('.suggested_text_cats').append('</optgroup>');

        console.log('<?php echo e($suggested_text_cat->name_ar); ?>');
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                $('.suggested_text_cats').change(function () {
                    console.log($('.suggested_text_cats option:selected').val());
                    $(this).siblings( ".s_cat").hide();
                    $(this).siblings( ".s_cat").children('select').attr('name','tttttttttttttttttt');

                    $(this).siblings( ".s_cat_"+$('.suggested_text_cats option:selected').val()).show();
                    $(this).siblings( ".s_cat_"+$('.suggested_text_cats option:selected').val()).children('select').attr('name','suggested_text[]');
                });

        
            
            
        
        $('.suggested_text').append('<option value="none">اخرى</option>');
        $('.suggested_text').change(function () {
            console.log($(this).parent('.s_cat'));
            if($(this).val()=='none'){$(this).parent('.s_cat').hide();$(this).parent('.s_cat').siblings('input[type="file"]').first().show();}
        });
        $('.suggested_text').change(function () {
            console.log($('.suggested_text option:selected').attr('data-thumbnail'));
            var src_img = $('.suggested_text option:selected').attr('data-thumbnail');
            $('#selected_text_img').attr('src',src_img);
            $('#myModal').modal();

        });


        $(document).ready(function () {
            $('.suggested_text .dropdown-toggle, .suggested_text .dropdown-menu').on('click', function (e) {
                e.stopPropagation();
            });

            $('.suggested_text .dropdown-toggle').click(function () {
                $('.suggested_text .dropdown-menu').toggleClass('active');
            });
        });

        // $('body').click(function () {
        //     $('.suggested_text .dropdown-menu').removeClass('active');
        // });
    </script>

    <script>
        var total = 0;
        var coupon = 0;
        var tax = Number($('#tax').val());

        var total_after_shipping = 0;
        var products_total = Number($('#products_total').val());
        $('#total').text(products_total);
        $('#total_footer').text(total);
        $('#total_after_tax h4').text(total+total*Number("<?php echo e($tax->tax); ?>")/100);
        $('#total_after_tax_input').val(total+total*Number("<?php echo e($tax->tax); ?>")/100);
        var total_after_tax = total+total*Number("<?php echo e($tax->tax); ?>")/100;

        $('#coupon_submit').click(function () {
            $.ajax({
                url:"<?php echo e(route('check_coupon')); ?>",
                data:{coupon:$('#coupon_value').val()},
                success:function (response) {
                    if(response.status){
                        if (response.data.type == 'price'){
                            $('#total_after_coupon').parent().show();
                            $('#coupon_value').addClass('border-success');
                            $('#coupon_value').addClass('text-success');
                            $('#coupon_value').attr('readonly',true);
                            if (total_after_shipping){
                                $('#total_after_coupon h4').text(total_after_shipping-response.data.value);

                            }else {
                                $('#total_after_coupon h4').text(total_after_tax-response.data.value);

                            }
                            //console.log(total_after_tax-response.data.value);
                        }else if(response.data.type == 'percentage'){
                            $('#total_after_coupon').parent().show();
                            $('#coupon_value').addClass('border-success');
                            $('#coupon_value').addClass('text-success');
                            $('#coupon_value').attr('readonly',true);
                            if (total_after_shipping){
                                $('#total_after_coupon h4').text(total_after_shipping-(response.data.value/100)*total_after_tax);

                            }else {
                                $('#total_after_coupon h4').text(total_after_tax-(response.data.value/100)*total_after_tax);

                            }
                        }
                    }else {
                        $('#coupon_value').css('border-color','red');
                        $('#coupon_value').css('color','red');
                    }
                }
            });
        });

        $('#shipping').change(function () {
            if ($('#shipping option:selected').val()==5){
                $('#cities').show();
                $('#shipping_price').text($('#cities option:selected').data('shipping-price'));
                $('#total_after_tax h4').text(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
                $('#total_after_tax_input').val(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
                total_after_shipping=0;
                total_after_shipping+=  total_after_tax+Number($('#cities option:selected').data('shipping-price'));
                $('#total').text(products_total+Number($('#cities option:selected').data('shipping-price')));
                $('#total_after_tax').val(products_total+Number($('#cities option:selected').data('shipping-price'))+tax);
                total_after_tax = 0;
                total_after_tax = products_total+Number($('#cities option:selected').data('shipping-price'))+tax;
                $('#total').text(total_after_tax);

            }else {
                $('#shipping_price').text($('#shipping option:selected').data('shipping-price'));
                $('#cities').hide();
                $('#total_after_tax h4').text(total_after_tax+Number($('#shipping option:selected').data('shipping-price')));
                $('#total_after_tax_input').val(total_after_tax+Number($('#shipping option:selected').data('shipping-price')));
                total_after_shipping=0;
                total_after_shipping+=  total_after_tax+Number($('#shipping option:selected').data('shipping-price'));
                $('#total').text(products_total+Number($('#shipping option:selected').data('shipping-price')));
                $('#total_after_tax').val(products_total+Number($('#shipping option:selected').data('shipping-price'))+tax);
                total_after_tax = 0;
                total_after_tax = products_total+Number($('#shipping option:selected').data('shipping-price'))+tax;
                $('#total').text(total_after_tax);

            }
        });
        $('#cities').change(function () {
            $('#shipping_price').text($('#cities option:selected').data('shipping-price'));
            $('#total_after_tax h4').text(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
            $('#total_after_tax_input').val(total_after_tax+Number($('#cities option:selected').data('shipping-price')));
            total_after_shipping=0;
            total_after_shipping+=  total_after_tax+Number($('#cities option:selected').data('shipping-price'));
            $('#total').text(products_total+Number($('#cities option:selected').data('shipping-price')));
            $('#total_after_tax').val(products_total+Number($('#cities option:selected').data('shipping-price'))+tax);
            total_after_tax = 0;
            total_after_tax = products_total+Number($('#cities option:selected').data('shipping-price'))+tax;
            $('#total').text(total_after_tax);
        });
        $('#payment').change(function () {
            if ($('#payment option:selected').val() == 'transfer'){
                $('#accounts_holder').show();
                // $('#accounts').show();
                $('#receipt').show();
            }else {
                $('#accounts_holder').hide();
                // $('#accounts').show();
                $('#receipt').hide();
            }
        });
        $('#accounts').change(function () {
            $('.account_info').hide();
            $('#'+$('#accounts option:selected').val()).show();
        });
        console.log(window.location.href.split('/')[window.location.href.split('/').length-1]);

    </script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="https://thdoan.github.io/bootstrap-select/css/bootstrap-select.css">
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>