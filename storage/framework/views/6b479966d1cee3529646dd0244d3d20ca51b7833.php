<?php $__env->startSection('content'); ?>
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Breadcrumbs Start -->
        <div class="rs-breadcrumbs sec-color">
            <div class="breadcrumbs-image">
                <img src="<?php echo e(asset('hepta/images/breadcrumbs/service2.jpg')); ?>" alt="Breadcrumbs Image">
                <div class="breadcrumbs-inner">
                    <div class="container">
                        <div class="breadcrumbs-text">
                            <h1 class="breadcrumbs-title">Services </h1>
                            <ul class="breadcrumbs-subtitle">
                                <li><a href="<?php echo e(route('index')); ?>"><i class="fa fa-home"></i>  Home</a></li>
                                <li>Services </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs End -->

        <!-- Services Start -->
        <section id="rs-services" class="rs-services-3 gray-color pt-100 pb-200">
            <div class="container">
                <div class="row">
                    <?php $services = \App\Feature::where([
                            ['type','service']
                            ])->get(); ?>
                    <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="common">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="icon-part">
                                        <i class="<?php echo e($service->picture); ?>" aria-hidden="true" class="icon-part"></i>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="text">
                                        <a href="javascript:void (0)"><h4><?php echo e($service->head); ?></h4></a>
                                        <p><?php echo $service->description; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </section>
        <!-- Services End -->

        <!-- customer Start -->
        <section id="rs-customer" class="rs-defult-customer rs-customer3 gray-color">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6 col-sm-12 mb-sm-50">
                        <!-- Contact Start -->
                        <section id="rs-contact" class="rs-contact">
                            <div class="contact-bg">
                                <div class="contact-form">
                                    <div class="sec-title">
                                        <h3 class="contact-title">Need a Quick Query</h3>
                                    </div>
                                    
                                    <form  method="post" action="<?php echo e(route('save_query')); ?>">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-field">
                                                    <input type="text" placeholder="Name" id="name" name="name" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-field">
                                                    <input type="email" placeholder="E-Mail" id="email" name="email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-field">
                                                    <input type="text" placeholder="Phone Number" id="phone_number" name="phone" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-field">
                                            <textarea placeholder="Your Message Here" rows="2" id="message" name="message" required></textarea>
                                        </div>
                                        <div class="form-button text-left">
                                            <button type="submit" class="readon">Submit Now</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                        <!-- Contact End -->
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="rs-carousel owl-carousel" data-loop="true" data-items="1" data-margin="30" data-autoplay="true" data-autoplay-timeout="7000" data-smart-speed="2000" data-dots="true" data-nav="false" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="1" data-md-device-nav="false" data-md-device-dots="true">
                        <?php $teams = \App\Testmonial::where('type','testimonial')->get(); ?>
                        <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <!-- Style 1 Start -->
                                <div class="customer-item">
                                    <div class="customer-part">
                                        <div class="item-img">
                                            <img src="<?php echo e($team->image_path); ?>" alt="">
                                        </div>
                                        <div class="item-details">
                                            <p><?php echo e($team->message); ?></p>
                                        </div>
                                        <div class="item-author">
                                            <div class="item-name">
                                                <?php echo e($team->name); ?>

                                            </div>
                                            <div class="item-designation"><?php echo e($team->title); ?></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Style 1 End -->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- customar end -->

        <!-- Partner Start -->
        <div id="rs-defult-partner" class="rs-defult-partner sec-color pt-100 pb-100">
            <div class="container">
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="5" data-margin="30" data-autoplay="true" data-autoplay-timeout="8000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="2" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="5" data-md-device-nav="true" data-md-device-dots="false">
                    <?php $clients = \App\Client::all(); ?>
                    <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="partner-item">
                            <a href="javascript:void (0)"><img src="<?php echo e($client->image_path); ?>" alt="Partner Image"></a>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </div>
        <!-- Partner End -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('hepta.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>