<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget stats-widget">
                        <div class="widget-body clearfix">
                            <div class="pull-right">
                                <h3 class="widget-title text-primary"><span class="counter" data-plugin="counterUp"><?php echo e(\Illuminate\Support\Facades\DB::table('users')->count()); ?></span></h3>
                                <small class="text-color">المستخدمين</small>
                            </div>
                            <span class="pull-left big-icon watermark"><i class="fa fa-paperclip"></i></span>
                        </div>
                        <footer class="widget-footer bg-primary">
                            <small>العدد الكلى</small>
                            <span class="small-chart pull-left" data-plugin="sparkline" data-options="[4,3,5,2,1], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
                        </footer>
                    </div><!-- .widget -->
                </div>

                
                    
                        
                            
                                
                                
                            
                            
                        
                        
                            
                            
                        
                    
                

                
                    
                        
                            
                                
                                
                            
                            
                        
                        
                            
                            
                        
                    
                

                <div class="col-md-3 col-sm-6">
                    <div class="widget stats-widget">
                        <div class="widget-body clearfix">
                            <div class="pull-right">
                                <h3 class="widget-title text-warning"><span class="counter" data-plugin="counterUp"><?php echo e(\Illuminate\Support\Facades\DB::table('categories')->count()); ?></span></h3>
                                <small class="text-color">التصنيفات</small>
                            </div>
                            <span class="pull-left big-icon watermark"><i class="fa fa-file-text-o"></i></span>
                        </div>
                        <footer class="widget-footer bg-warning">
                            <small>العدد الكلى</small>
                            <span class="small-chart pull-left" data-plugin="sparkline" data-options="[5,4,3,5,2],{ type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
                        </footer>
                    </div><!-- .widget -->
                </div>
            </div><!-- .row -->

        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>