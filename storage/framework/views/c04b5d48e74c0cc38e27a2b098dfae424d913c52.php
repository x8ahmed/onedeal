<?php $__env->startSection('main'); ?>

<div class="row">
    <div class="col-md-12 pull-right">
        <div class="widget">
            <header class="widget-header">
                <h4 class="widget-title">اضافة المميزات</h4>
            </header><!-- .widget-header -->
            <hr class="widget-separator">
            <div class="widget-body">
                <div class="m-b-lg">
                    
                    
                    
                </div>
                <?php echo $__env->make('admin.layouts.massege', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <form action="<?php echo e(route('features-store')); ?>" method="post" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('post')); ?>



                    <div class="form-group">
                    <label for="first_name">لاختيار صورة</label>
                    <input type="radio" name="image" value="image" class="dd">
                      <input type="file" class="form-control img-responsive img-thumbnail" name="image" id="image">
                    </div>

                    <div class="form-group">
                    <label for="first_name">لاختيار ايقونة</label>
                    <input type="radio" name="image" value="icon" class="dd">
                    <input type="text" class="form-control hidden" name="icon" id="icon">
                     </div>

                    <div class="form-group">
                        <label for="sub_title">النوع</label>
                            <select name="type" class="form-control" id="type">
                                <option value=""> اختر النوع</option>
                                <option value="about-us">about-us</option>
                                <option value="service">service</option>
                                <option value="project">project</option>
                                <option value="why_choose_us">why_choose_us</option>
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="first_name">العنوان </label>
                        <input type="text" class="form-control" name="head" value="<?php echo e(old('head')); ?>">
                    </div>

                    <div class="form-group">
                        <label for="first_name">الوصف</label>
                        <textarea class="form-control ckeditor" name="description">
                            <?php echo e(old('description')); ?>

                        </textarea>
                    </div>

                    <div class="form-group">
                        <label for="first_name">الظهور في الرئيسية</label>
                        <input type="radio" name="show" value="0">لا
                        <input type="radio"  name="show" value="1">نعم
                    </div>
                    <div id="project_form" class="hidden">
                        <h3>معلومات المشاريع</h3>
                        <div class="form-group">
                            <label for="sub_title">العميل</label>
                            <input type="text" class="form-control" name="client" value="<?php echo e(old('client')); ?>">
                        </div>

                        <div class="form-group">
                            <label for="sub_title">تاريخ الانتهاء</label>
                            <input type="text" class="form-control" name="completed_date" value="<?php echo e(old('completed_date')); ?>">
                        </div>

                        <div class="form-group">
                            <label for="sub_title">نوع التعاقد</label>
                            <input type="text" class="form-control" name="contract_type" value="<?php echo e(old('contract_type')); ?>">
                        </div>

                        <div class="form-group">
                            <label for="sub_title">موقع العمل</label>
                            <input type="text" class="form-control" name="project_location" value="<?php echo e(old('project_location')); ?>">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
    <?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $(document).ready(function () {
           $(document).on('change','#type',function () {
           var val =  $( "#type option:selected" ).val();

           if (val == 'project'){
               $('#project_form').removeClass('hidden');
           }else {
               $('#project_form').addClass('hidden');
           }

           });
        });
        $(document).on('click','.dd',function () {
            var radio = $("input[name='image']:checked").val();
            if (radio == 'icon'){
                $('#icon').removeClass('hidden');
                $('.file-caption-main').hide();
            }else {
                $('#icon').addClass('hidden');
                $('.file-caption-main').show();
            }
        });

    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>