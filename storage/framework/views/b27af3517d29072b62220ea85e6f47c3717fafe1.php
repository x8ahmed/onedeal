<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading bg-white">
                        <h4 class="panel-title">معلومات الفاتورة</h4>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div style="float: right" class="col-sm-8 col-xs-6">
                                <h4 class="fw-600 text-right">رقم الطلب <?php echo e($order->id+1000); ?></h4>
                                <p class="text-right">التاريخ: <?php echo e($order->created_at); ?></p>

                                <h4 class="m-t-lg fw-600">التفاصيل:</h4>
                                <div class="clearfix">
                                    <p class="pull-right">الاجمالى:</p>
                                    <p class="pull-left"><?php echo e($order->total_after_coupon); ?></p>
                                </div>
                                
                                    
                                    
                                
                                
                                    
                                    
                                
                                
                                    
                                    
                                
                                
                                    
                                    
                                
                                
                                    
                                    
                                
                                
                                    
                                    
                                
                            </div>

                            <div style="float: right" class="col-sm-4 col-xs-6">
                                <h4 class="fw-600">العنوان</h4>
                                
                                <p><?php echo e(\App\User::find($order->user_id)->address); ?></p>
                                <p>رقم الهاتف: <?php echo e(\App\User::find($order->user_id)->phone); ?></p>

                                <h4 class="m-t-lg fw-600">اسم المستلم:</h4>
                                <p><?php echo e(\App\User::find($order->user_id)->name); ?></p>
                                
                            </div>
                        </div>

                        <div class="table-responsive m-h-lg">
                            <table class="table">
                                <tr>
                                    <th>#</th>
                                    <th>اسم المنتج</th
                                    ><th>الكمية</th>
                                    <th>النص</th>
                                </tr>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <th><?php echo e($product->id); ?></th>
                                        <th><?php echo e(\App\Product::find($product->product_id)->name_ar); ?></th>
                                        <th><?php echo e($product->quantity); ?></th>
                                        <th><?php echo e($product->text); ?></th>
                                    </tr>
                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </table>
                        </div>

                        <div class="row">
                            <div class="col-sm-3 col-sm-push-9">
                                
                                
                                
                                <div class="m-t-lg">
                                    
                                    <button type="button" class="btn btn-md btn-default" onclick="window.print();">طباعة</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- .panel-body -->
                </div>
            </div><!-- .container-fluid -->
        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $( document ).ready(function() {
            $('aside').hide();
            $('nav').hide();
            $('footer').hide();
            console.log( "ready!" );
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>