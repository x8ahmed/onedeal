<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <?php $__currentLoopData = $coupons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coupon): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <form action="<?php echo e(route('deleteItem',$coupon->id)); ?>" method="post" id="form-<?php echo e($coupon->id); ?>">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('delete')); ?>

                        <input type="hidden" name="class_name" value="<?php echo e(get_class($coupon)); ?>">
                    </form>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اكواد الخصم</h4>
                            <a class="btn btn-primary pull-left" href="<?php echo e(route('create_coupon')); ?>">اضف كود</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الكود</th>
                                        <th>النوع</th>
                                        <th>القيمة</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>الكود</th>
                                        <th>النوع</th>
                                        <th>القيمة</th>
                                        <th>حذف</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php $__currentLoopData = $coupons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coupon): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($coupon->id); ?></td>
                                            <td><?php echo e($coupon->name); ?></td>
                                            <td><?php echo e(($coupon->type=='price')?'مبلغ ثابت':'خصم نسبة'); ?></td>
                                            <td><?php echo e($coupon->value); ?></td>
                                            <td><button onclick="JSalert(<?php echo e($coupon->id); ?>)"  id="<?php echo e($coupon->id); ?>"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>