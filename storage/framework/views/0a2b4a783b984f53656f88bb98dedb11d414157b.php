<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">الضريبة</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="<?php echo e(route('save_tax')); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="tax">الضريبة</label>
                                    <input type="text" name="tax" id="tax" value="<?php echo e((isset($tax->tax))?$tax->tax:0); ?>" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>