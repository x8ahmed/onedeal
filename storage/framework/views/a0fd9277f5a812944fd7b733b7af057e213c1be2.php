
<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اضف كود جديد</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="<?php echo e(route('save_coupon')); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="type">اختار نوع الخصم</label>
                                    <div>
                                        <select class="form-control" id="type" name="type" required>
                                            <option value="percentage">نسبة</option>
                                            <option value="price">مبلغ ثابت</option>
                                        </select>
                                    </div>
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="name">رقم الكود</label>
                                    <input type="text" name="name" id="name" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="value">النسبة او المبلغ</label>
                                    <input type="text" name="value" id="value" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>