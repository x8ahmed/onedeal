
<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اضف حساب جديد</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="<?php echo e(route('save_billing_account')); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="name_ar">اسم البنك باللغة العربية</label>
                                    <input type="text" name="name_ar" id="name_ar" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="name_en">اسم البنك باللغة الانجليزية</label>
                                    <input type="text" name="name_en" id="name_en" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="account_number">رقم الحساب</label>
                                    <input type="text" name="account_number" id="account_number" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="logo">شعار البنك</label>
                                    <input type="file" name="logo" id="logo" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>