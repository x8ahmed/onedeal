<?php $__env->startSection('main'); ?>

    <!-- top Products -->
    <div class="ads-grid py-sm-5 py-4">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <!--<span>O</span>ur-->
                <!--<span>N</span>ew-->
                <!--<span>P</span>roducts-->
                <span>نتائج البحث</span>
            </h3>
            <!-- //tittle heading -->
            <div class="row">
                <!-- product left -->
                <div class="agileinfo-ads-display col-lg-12">
                    <div class="wrapper">
                        <!-- first section -->
                        <div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
                            <!--<h3 class="heading-tittle text-center font-italic">New Brand Mobiles</h3>-->
                            <div class="row">
                                <?php if(count($products)): ?>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-md-4 product-men mt-5">
                                        <div class="men-pro-item simpleCart_shelfItem">
                                            <div class="men-thumb-item text-center" style="overflow: hidden;height: 200px">
                                                <img src="<?php echo e(asset($product->thumbnail)); ?>" alt="<?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?>">
                                                <div class="men-cart-pro">
                                                    <div class="inner-men-cart-pro">
                                                        <a href="single.html" class="link-product-add-cart">التفاصيل</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-info-product text-center border-top mt-4">
                                                <h4 class="pt-1">
                                                    <a href="single.html"><?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></a>
                                                </h4>
                                                <div class="info-product-price my-2">
                                                    <span class="item_price"><?php echo e($product->price.' '.trans('index.price')); ?></span>
                                                    <!--<del>$280.00</del>-->
                                                </div>
                                                <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                                    <form action="#" method="post">
                                                        <fieldset id="<?php echo e($product->id); ?>">
                                                            <input type="hidden" name="cmd" value="_cart" />
                                                            <input type="hidden" name="add" value="1" />
                                                            <input type="hidden" name="business" value=" " />
                                                            <input type="hidden" name="id" value="<?php echo e($product->id); ?>" />
                                                            <input type="hidden" name="item_name" value="<?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?>" />
                                                            <input type="hidden" name="amount" value="<?php echo e($product->price); ?>" />
                                                            <input type="hidden" name="discount_amount" value="0.00" />
                                                            <input type="hidden" name="currency_code" value="SAR" />
                                                            <input type="hidden" name="return" value=" " />
                                                            <input type="hidden" name="cancel_return" value=" " />
                                                            <input type="submit" name="submit" value="اضف إلى العربة" class="button btn"/>
                                                        </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>

                                    <div class="col-md-12 alert alert-warning text-center" style="padding: 20px;">
                                        <strong>عفوا!</strong> لا توجد نتائج مطابقة
                                    </div>
                                    <?php endif; ?>

                            </div>
                        </div>

                    </div>
                </div>
                <!-- //product left -->


            </div>
        </div>
    </div>
    <!-- //top products -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>