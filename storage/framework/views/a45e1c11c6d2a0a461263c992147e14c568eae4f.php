
<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                
                    
                        
                        
                        
                    
            
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">الطلبات</h4>
                            
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>حالة الطلب</th>
                                        <th>وسيلة الشحن</th>
                                        <th>وسيلة الدفع</th>
                                        <th>صورة الايصال</th>
                                        <th>الاجمالى</th>
                                        <th>خيارات</th>
                                        <th colspan="5">المنتجات</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>اسم المنتج</th>
                                        <th>الكمية</th>
                                        <th>النص المطلوب</th>
                                        <th>ملاحظات</th>
                                        <th>المرفقات</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>حالة الطلب</th>
                                        <th>وسيلة الشحن</th>
                                        <th>وسيلة الدفع</th>
                                        <th>صورة الايصال</th>
                                        <th>الاجمالى</th>
                                        <th>خيارات</th>
                                        <th colspan="5">المنتجات</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php $order_products = \App\OrderProduct::where('order_id',$order->id)->get(); ?>
                                        <?php $__currentLoopData = $order_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <?php if($loop->first): ?>
                                                <td rowspan="<?php echo e(count($order_products)); ?>"><a href="javascript:void(0)"  data-toggle="modal" data-target="#order_<?php echo e($order->id); ?>"><?php echo e($order->id); ?></a></td>
                                                <!-- Modal -->
                                                <div id="order_<?php echo e($order->id); ?>" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title"></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p style="font-weight: bold">بيانات العميل</p>
                                                                <?php $user = \App\User::find($order->user_id); ?>
                                                                <p>الاسم: <?php echo e($user->name); ?></p>
                                                                <p>رقم الهاتف: <?php echo e($user->phone); ?></p>
                                                                <p>العنوان: <?php echo e($user->address); ?></p>
                                                                <p style="font-weight: bold">ملاحظات اخرى</p>
                                                                <p><?php echo e($order->other_notes); ?></p>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- Modal -->
                                            <td rowspan="<?php echo e(count($order_products)); ?>">
                                                <?php if($order->status == 0): ?>
                                                    <?php echo e('قيد المعالجة'); ?>

                                                <?php elseif($order->status == 1): ?>
                                                    <?php echo e('جارى الشحن'); ?>

                                                <?php elseif($order->status == 2): ?>
                                                    <?php echo e('تم الاستلام'); ?>

                                                <?php endif; ?>
                                            </td>
                                                <td rowspan="<?php echo e(count($order_products)); ?>"><?php echo e(\App\Shipping::find($order->shipping_type)->type); ?></td>
                                            <td rowspan="<?php echo e(count($order_products)); ?>"><?php echo e($order->payment_method); ?></td>
                                            <td rowspan="<?php echo e(count($order_products)); ?>"><?php echo ($order->receipt)?'<a href="'.asset($order->receipt).'" target="_blank">صورة الايصال</a>':'لا يوجد'; ?>  </td>
                                            <td rowspan="<?php echo e(count($order_products)); ?>"><?php echo e($order->total_after_coupon); ?></td>
                                            <td rowspan="<?php echo e(count($order_products)); ?>">
                                                <?php if($order->status == 0): ?>
                                                    <a href="<?php echo e(route('start_shipping',[$order->id,1])); ?>" class="btn btn-primary">بدء الشحن</a>
                                                    <a href="<?php echo e(route('bill',$order->id)); ?>" class="btn btn-info">طباعة الطلب</a>
                                                <?php elseif($order->status == 1): ?>
                                                    <a href="<?php echo e(route('start_shipping',[$order->id,2])); ?>" class="btn btn-primary">تاكيد الاستلام</a>
                                                    <a href="<?php echo e(route('bill',$order->id)); ?>" class="btn btn-info">طباعة الطلب</a>

                                                <?php elseif($order->status == 2): ?>
                                                    <?php echo e('تم الاستلام'); ?>

                                                    <a href="<?php echo e(route('bill',$order->id)); ?>" class="btn btn-info">طباعة الطلب</a>

                                                <?php endif; ?>
                                            </td>
                                            <?php endif; ?>
                                            <td><?php echo e(\App\Product::find($order_product->product_id)->name_ar); ?></td>
                                            <td><?php echo e($order_product->quantity); ?></td>
                                            <td><?php echo (is_numeric($order_product->text))?'<a href="'.asset(\App\SuggestedText::find($order_product->text)->text).'" target="_blank">النص</a>':'<a href="'.asset($order_product->text).'" target="_blank">النص</a>'; ?></td>
                                                <td><?php echo e($order_product->notes); ?></td>
                                                <td><?php if($order_product->attachements): ?>
                                                        <a href="<?php echo e(asset($order_product->attachements)); ?>" target="_blank">المرفقات</a>
                                                    <?php endif; ?>
                                                </td>

                                            
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>