<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <form action="<?php echo e(route('deleteItem',$category->id)); ?>" method="post" id="form-<?php echo e($category->id); ?>">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('delete')); ?>

                        <input type="hidden" name="class_name" value="<?php echo e(get_class($category)); ?>">
                    </form>
                <?php $sub_cats = \App\Category::where('parent_id',$category->id)->get(); ?>
                <?php $__currentLoopData = $sub_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <form action="<?php echo e(route('deleteItem',$sub_cat->id)); ?>" method="post" id="form-<?php echo e($sub_cat->id); ?>">
                            <?php echo e(csrf_field()); ?>

                            <?php echo e(method_field('delete')); ?>

                            <input type="hidden" name="class_name" value="<?php echo e(get_class($sub_cat)); ?>">
                        </form>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">التصنيفات</h4>
                            <a class="btn btn-primary pull-left" href="<?php echo e(route('create_category')); ?>">اضف تصنيف جديد</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم باللغة العربية</th>
                                        <th>الاسم باللغة الانجليزية</th>
                                        <th>الصورة</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم باللغة العربية</th>
                                        <th>الاسم باللغة الانجليزية</th>
                                        <th>الصورة</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($category->id); ?></td>
                                            <td><?php echo e($category->name_ar); ?></td>
                                            <td><?php echo e($category->name_en); ?></td>
                                            <td><img src="<?php echo e(asset($category->picture)); ?>" class="img-responsive img-circle" style="width: 50px;height: 50px;"></td>
                                            <td><a href="<?php echo e(route('update_category',$category->id)); ?>" class="btn btn-primary">تعديل</a></td>
                                            <td><button onclick="JSalert(<?php echo e($category->id); ?>)"  id="<?php echo e($category->id); ?>"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                        <?php $sub_cats = \App\Category::where('parent_id',$category->id)->get(); ?>
                                        <?php $__currentLoopData = $sub_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($category->id); ?>__</td>
                                                <td><?php echo e($sub_cat->name_ar); ?></td>
                                                <td><?php echo e($sub_cat->name_en); ?></td>
                                                <td><img src="<?php echo e(asset($sub_cat->picture)); ?>" class="img-responsive img-circle" style="width: 50px;height: 50px;"></td>
                                                <td><a href="<?php echo e(route('update_category',$sub_cat->id)); ?>" class="btn btn-primary">تعديل</a></td>
                                                <td><button onclick="JSalert(<?php echo e($sub_cat->id); ?>)"  id="<?php echo e($sub_cat->id); ?>"  class="btn btn-danger">حذف</button></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>