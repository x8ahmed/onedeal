<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <form action="<?php echo e(route('deleteProduct',$product->id)); ?>" method="post" id="form-<?php echo e($product->id); ?>">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('delete')); ?>

                        <input type="hidden" name="class_name" value="<?php echo e(get_class($product)); ?>">
                    </form>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">المنتجات</h4>
                            <a class="btn btn-primary pull-left" href="<?php echo e(route('create_product')); ?>">اضف منتج</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم باللغة العربية</th>
                                        <th>الاسم باللغة الانجليزية</th>
                                        <th>السعر</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>الاسم باللغة العربية</th>
                                        <th>الاسم باللغة الانجليزية</th>
                                        <th>السعر</th>
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($product->id); ?></td>
                                            <td><?php echo e($product->name_ar); ?></td>
                                            <td><?php echo e($product->name_en); ?></td>
                                            <td><?php echo e($product->price); ?></td>
                                            <td><a href="<?php echo e(route('update_product',$product->id)); ?>" class="btn btn-primary">تعديل</a></td>
                                            <td><button onclick="JSalert(<?php echo e($product->id); ?>)"  id="<?php echo e($product->id); ?>"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>