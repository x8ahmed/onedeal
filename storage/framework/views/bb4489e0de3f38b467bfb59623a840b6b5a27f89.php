<?php $__env->startSection('main'); ?>


    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title" style="margin-bottom: 15px;">العملاء <small><?php echo e($clients->total()); ?></small></h4>
                    <form action="<?php echo e(route('clients')); ?>" method="get">
                        <div class="row">
                            <div class=" col-lg-12 col-md-6 pull-left">
                                    <a href="<?php echo e(route('clients-create')); ?>" class=" btn btn-primary btn-sm">اضافة</a>
                            </div>

                        </div>
                    </form>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="table-responsive">
                        <?php if($clients->count() > 0 ): ?>
                            <table  class="table table-striped" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>اسم العميل</th>
                                    <th>لوجو الشركة</th>
                                    <th>ألتحكم</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($index + 1); ?></td>
                                        <td><?php echo e($client->name); ?></td>
                                        <td><img src="<?php echo e($client->image_path); ?>" class="img-thumbnail" style="width: 150px"></td>
                                        <td>
                                                <a href="<?php echo e(route('clients-edit',$client->id)); ?>" class="btn btn-info btn-sm">تعديل</a>
                                                <form action="<?php echo e(route('clients-destroy',$client->id)); ?>" method="post" style="display: inline-block">
                                                    <?php echo e(csrf_field()); ?>

                                                    <?php echo e(method_field('delete')); ?>

                                                    <button type="submit" class="btn btn-danger delete btn-sm">حذف</button>
                                                </form>

                                        </td>


                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div style="text-align: center"><?php echo e($clients->appends(request()->query())->links()); ?></div>

                        <?php else: ?>

                            <h3>لايوجد سجلات</h3>
                        <?php endif; ?>
                    </div>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>