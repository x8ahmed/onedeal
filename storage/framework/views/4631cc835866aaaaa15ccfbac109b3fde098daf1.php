<?php $__env->startSection('main'); ?>
    <!-- banner-2 -->
    <div class="page-head_agile_info_w3l">

    </div>
    <!-- //banner-2 -->
    <!-- page -->
    <div class="services-breadcrumb">
        <div class="agile_inner_breadcrumb">
            <div class="container">
                <ul class="w3_short">
                    <li>
                        <a href="index.html">الرئيسية</a>
                        <i>|</i>
                    </li>
                    <li><?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- //page -->

    <!-- Single Page -->
    <div class="banner-bootom-w3-agileits py-5">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <!--<span>S</span>ingle-->
                <!--<span>P</span>age</h3>-->
                <span><?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></span>
                <!-- //tittle heading -->
                <div class="row">
                    <div class="col-lg-5 col-md-8 single-right-left ">
                        <div class="grid images_3_of_2">
                            <div class="flexslider">
                                <ul class="slides">
                                    <?php $product_pictures = \App\ProductPicture::where('product_id',$product->id)->get(); ?>
                                    <?php $__currentLoopData = $product_pictures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_picture): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li data-thumb="<?php echo e(asset($product_picture->picture)); ?>">
                                        <div class="thumb-image">
                                            <img src="<?php echo e(asset($product_picture->picture)); ?>" data-imagezoom="true" class="img-fluid" alt="">
                                        </div>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-7 single-right-left simpleCart_shelfItem">
                        
                            
                        <!--<h3 class="mb-3">Samsung Galaxy J7 Prime (Gold, 16 GB) (3 GB RAM)</h3>-->
                        <p class="mb-3">
                            <span class="item_price"><?php echo e($product->price.' '.trans('index.price')); ?></span>
                            <!--<del class="mx-2 font-weight-light">$280.00</del>-->
                            
                        </p>
                        <div class="single-infoagile">
              <?php echo $product['description_'.\Illuminate\Support\Facades\Lang::getLocale()]; ?>

                        </div>
                        
                            
                                
                                
                            
                            
                                
                                    
                                
                                
                                    
                                
                                
                                    
                                
                                
                                    
                                
                                
                                    
                                
                            
                            
                                
                            
                        
                        <div class="occasion-cart">
                            <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                <form action="#" method="post">
                                    <fieldset id="<?php echo e($product->id); ?>">
                                        <input type="hidden" name="cmd" value="_cart" />
                                        <input type="hidden" name="add" value="1" />
                                        <input type="hidden" name="business" value=" " />
                                        <input type="hidden" name="id" value="<?php echo e($product->id); ?>" />
                                        <input type="hidden" name="item_name" value="<?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?>" />
                                        <input type="hidden" name="amount" value="<?php echo e($product->price); ?>" />
                                        <input type="hidden" name="discount_amount" value="0.00" />
                                        <input type="hidden" name="currency_code" value="SAR" />
                                        <input type="hidden" name="return" value=" " />
                                        <input type="hidden" name="cancel_return" value=" " />
                                        <input type="submit" name="submit" value="اضف إلى العربة" class="button btn"/>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!-- //Single Page -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <!-- imagezoom -->
    <script src="<?php echo e(asset('site/js/imagezoom.js')); ?>"></script>
    <!-- //imagezoom -->

    <!-- flexslider -->
    <link rel="stylesheet" href="<?php echo e(asset('site/css/flexslider.css')); ?>" type="text/css" media="screen" />

    <script src="<?php echo e(asset('site/js/jquery.flexslider.js')); ?>"></script>
    <script>
        // Can also be used with $(document).ready()
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });
    </script>
    <!-- //FlexSlider-->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>