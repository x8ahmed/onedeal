
<?php $__env->startSection('main'); ?>
    <div class="container">
        <header class="page-header">
            <h1 class="page-title"><?php echo e($cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></h1>
            <ol class="breadcrumb page-breadcrumb">
                <li><a href="<?php echo e(route('index')); ?>"><?php echo e(trans('index.home')); ?></a>
                </li>
                <?php
                if ($cat->parent_id !=0){
                   $p_name = \App\Category::find($cat->parent_id)['name_'.\Illuminate\Support\Facades\Lang::getLocale()];
                }
                ?>
                <?php if($cat->parent_id !=0): ?>
                <li><a href="<?php echo e(route('tags',str_replace(' ', '-',$p_name))); ?>"><?php echo e($p_name); ?></a>
                </li>
                <?php endif; ?>
                <li class="active"><?php echo e($cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></li>
            </ol>
            <ul class="category-selections clearfix">

                <li><span class="category-selections-sign"><?php echo e(trans('index.all_categories')); ?></span>
                    <?php $parent_cats = \App\Category::where('parent_id',0)->get(); ?>
                    <select class="category-selections-select" id="agileinfo-nav_search">
                        <?php $__currentLoopData = $parent_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <optgroup label="<?php echo e($parent_cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?>">
                            <?php $parent_cats_childs = \App\Category::where('parent_id',$parent_cat->id)->get(); ?>
                            <?php $__currentLoopData = $parent_cats_childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <option value="<?php echo e(str_replace(' ', '-',$child['name_'.\Illuminate\Support\Facades\Lang::getLocale()])); ?>"><?php echo e($child['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></option>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </optgroup>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </select>
                </li>
            </ul>
        </header>
        <div class="row" data-gutter="15">
            <?php if(isset($products)): ?>
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-3">
                <div class="product ">
                    <ul class="product-labels"></ul>
                    <div class="product-img-wrap">
                        <img class="product-img-primary" src="<?php echo e(asset($product->thumbnail)); ?>" alt="Image Alternative text" title="Image Title" />
                        <img class="product-img-alt" src="<?php echo e(asset($product->thumbnail)); ?>" alt="Image Alternative text" title="Image Title" />
                    </div>
                    <a class="product-link" href="<?php echo e(route('single_product',$product->id)); ?>"></a>
                    <div class="product-caption">

                        <h5 class="product-caption-title"><?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></h5>
                        <div class="product-caption-price"><span class="product-caption-price-new"><?php echo e($product->price.trans('index.price')); ?></span>
                        </div>

                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>

                <?php if(isset($texts)): ?>
                    <?php $__currentLoopData = $texts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $text): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-3">
                            <div class="product ">
                                <ul class="product-labels"></ul>
                                <div class="product-img-wrap">
                                    <img class="product-img-primary" src="<?php echo e(asset($text->text)); ?>" alt="Image Alternative text" title="Image Title" />
                                    <img class="product-img-alt" src="<?php echo e(asset($text->text)); ?>" alt="Image Alternative text" title="Image Title" />
                                </div>
                                <a class="product-link" href="<?php echo e(route('single_product',[$text->id,'text'])); ?>"></a>
                                <div class="product-caption">

                                    
                                    
                                    </div>

                                </div>
                            </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
        </div>


        <div class="row">

            <div class="col-md-6">
                <nav>
                    <?php if(isset($products)): ?>
                    <ul class="pagination category-pagination pull-right">
                        <?php for($x=1;$x<$products->lastPage();$x++): ?>
                        <li class="<?php echo e(($x==1)?"active":""); ?>"><a href="<?php echo e(\Illuminate\Support\Facades\URL::current().'?page='.$x); ?>"><?php echo e($x); ?></a>
                        </li>
                        <?php endfor; ?>
                        <li class="last"><a href="<?php echo e(\Illuminate\Support\Facades\URL::current().'?page='.$products->lastPage()); ?>"><i class="fa fa-long-arrow-right"></i></a>
                        </li>
                    </ul>
                    <?php endif; ?>

                        <?php if(isset($texts)): ?>
                            <ul class="pagination category-pagination pull-right">
                                <?php for($x=1;$x<$texts->lastPage();$x++): ?>
                                    <li class="<?php echo e(($x==1)?"active":""); ?>"><a href="<?php echo e(\Illuminate\Support\Facades\URL::current().'?page='.$x); ?>"><?php echo e($x); ?></a>
                                    </li>
                                <?php endfor; ?>
                                <li class="last"><a href="<?php echo e(\Illuminate\Support\Facades\URL::current().'?page='.$texts->lastPage()); ?>"><i class="fa fa-long-arrow-right"></i></a>
                                </li>
                            </ul>
                        <?php endif; ?>
                </nav>
            </div>
        </div>
    </div>
    <div class="gap"></div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>