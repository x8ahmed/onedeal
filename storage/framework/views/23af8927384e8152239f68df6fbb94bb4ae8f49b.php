<?php $__env->startSection('content'); ?>
    <?php
    $slides = \App\Slider::all();
    ?>
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Slider Start -->
        <div id="rs-slider" class="rs-slider rs-slider-one">
            <div class="bend niceties">
                <div id="nivoSlider" class="slides">
                    <?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <img src="<?php echo e(asset($slide->image_path)); ?>" alt="<?php echo e($slide->title); ?>" title="#<?php echo e($slide->id); ?>" />
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            <?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <!-- Slide 1 -->
                <div id="<?php echo e($slide->id); ?>" class="slider-direction">
                    <div class="display-table">
                        <div class="display-table-cell">
                            <div class="container">
                                <div class="slider-des">
                                    <h3 class="sl-sub-title"><?php echo e($slide->sub_title); ?></h3>
                                    <h1 class="sl-title"><?php echo e($slide->title); ?></h1>
                                    <div class="sl-desc margin-0">
                                        <?php echo $slide->description; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
        <!-- Slider End -->


        <!-- Services Start -->
        <section id="rs-services" class="rs-services-3 rs-service-style1 sec-color pt-100 pb-70">
            <div class="container">
                <div class="row rs-vertical-middle">
                    <div class="col-lg-4 col-md-12 mb-md-50">
                        <div class="service-title">
                            <h3>
                                <?php echo e(\App\Site_setting::where('name','services_head')->first()->value); ?>

                            </h3>
                            <p>
                                <?php echo e(\App\Site_setting::where('name','services_sub_head')->first()->value); ?>

                            </p>
                            <a href="#" class="readon">View Services</a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12">
                        <div class="row">
                            <?php $services = \App\Feature::where([
                            ['type','service'],['show',1]
                            ])->get(); ?>
                            <?php for($x=0;$x<count($services);$x++): ?>
                                <?php if($x%2==0&&$x==0): ?>
                            <div class="col-md-6 col-sm-12">
                                <?php endif; ?>
                                <?php if($x%2==0&&$x!=0): ?>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                <?php endif; ?>

                                <div class="common">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="icon-part">
                                                
                                                <i class="<?php echo e($services[$x]->picture); ?>" aria-hidden="true" class="icon-part"></i>

                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="text">
                                                <a href="javascript:void(0)"><h4><?php echo e($services[$x]->head); ?></h4></a>
                                                <p><?php echo $services[$x]->description; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                      <?php if($x==count($services)-1): ?>
                                        </div>
                                       <?php endif; ?>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
                <!-- 4th -->

            </div>
        </section>
        <!-- Services End -->

        <!-- Portfolio Start -->
        <section id="rs-portfolio2" class="rs-portfolio2 defutl-style sec-spacer">
            <div class="container">
                <div class="sec-title">
                    <h3>Our Latest Project</h3>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="20" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="3" data-md-device-nav="true" data-md-device-dots="false">
                            <?php $projects = \App\Feature::where([
                            ['type','project'],['show',1]
                            ])->get(); ?>
                            <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="portfolio-item">
                                <div class="portfolio-img">
                                    <img src="<?php echo e($project->image_path); ?>" alt="<?php echo e($project->head); ?>" />
                                </div>
                                <div class="portfolio-content">
                                    <div class="display-table">
                                        <div class="display-table-cell">
                                            <h4 class="p-title"><a href="<?php echo e(route('single_project',$project->id)); ?>"><?php echo e($project->head); ?></a></h4>
                                            <a href="<?php echo e(route('single_project',$project->id)); ?>" class="project-btn">View Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Portfolio End -->

        <!-- Expertise Area satar -->
        <div class="why-choose-us defult-style sec-color pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 mb-md-50">
                        <div class="video-section-area">
                            <div class="image-here">
                                <img src="<?php echo e(asset(\App\Site_setting::where('name','why_choose_us_picture')->first()->value)); ?>" alt="">
                                <div class="video-icon-here">
                                    <a class="popup-videos animated pulse" href="<?php echo e(\App\Site_setting::where('name','why_choose_us_video')->first()->value); ?>" title="Video Icon">
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                            <div class="overly-border"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 services-responsibiity">
                        <div class="service-res-inner">
                            <div class="sec-title">
                                <h3><?php echo e(\App\Site_setting::where('name','why_choose_us')->first()->value); ?></h3>
                            </div>
                            <?php $whys = \App\Feature::where([
                            ['type','why_choose_us'],['show','1']
                            ])->get(); ?>
                            <?php $__currentLoopData = $whys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $why): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="services-item">
                                <div class="service-mid-icon">
                                    <a href="#">
                                        <span class="service-mid-icon-container">
                                            <i class="<?php echo e($why->picture); ?>" aria-hidden="true" style="line-height: unset"></i>
                                        </span></a>
                                </div>
                                <div class="services-desc">
                                    <h3 class="services-title"><a href="javascript:void (0)"><?php echo e($why->head); ?></a></h3>
                                    <p><?php echo $why->description; ?></p>
                                </div>
                            </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Expertise Area end -->

        <!-- Team Start -->
        <section id="rs-team" class="rs-defult-team defult-style sec-spacer">
            <!-- Counter Up Section Start Here-->
            <div class="counter-top-area defult-style">
                <div class="container">
                    <div class="row rs-count">
                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration=".3s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="W" class="icon"></i>
                                <h3 class="rs-counter"><?php echo e(\App\Site_setting::where('name','clients_no')->first()->value); ?></h3>
                                <h4>Happy Client</h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->

                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6  wow fadeInUp" data-wow-duration=".7s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="C" class="icon"></i>
                                <h3 class="rs-counter"><?php echo e(\App\Site_setting::where('name','projects_done')->first()->value); ?></h3>
                                <h4>Project Done </h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->

                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration=".9s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="P" class="icon"></i>
                                <h3 class="rs-counter"><?php echo e(\App\Site_setting::where('name','awards')->first()->value); ?></h3>
                                <h4>Awards Won</h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->

                        <!-- COUNTER-LIST START -->
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="300ms">
                            <div class="rs-counter-list">
                                <i data-icon="&#xe001;" class="icon"></i>
                                <h3 class="rs-counter"><?php echo e(\App\Site_setting::where('name','exp')->first()->value); ?></h3>
                                <h4 class="last">Experience Year</h4>
                            </div>
                        </div>
                        <!-- COUNTER-LIST END -->
                    </div>
                </div>
            </div>
            <!-- Counter Down Section End Here-->
            <div class="container">
                <div class="sec-title">
                    <h3>Our Expert Advisor</h3>
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="3" data-md-device-nav="true" data-md-device-dots="false">
                  <?php $teams = \App\Testmonial::where('type','team')->get(); ?>
                  <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <!--team item start -->
                    <div class="team-item">
                        <div class="team-overlay">
                            <div class="team-img">
                                <img src="<?php echo e($team->image_path); ?>" alt="team Image" />
                            </div>
                            
                                
                                    
                                    
                                    
                                    
                                
                            
                        </div>
                        <div class="team-info">
                            <a href="javascript:void (0)"><h4 class="title"><?php echo e($team->name); ?></h4></a>
                            <span class="post"><?php echo e($team->title); ?></span>
                        </div>
                    </div>
                    <!--team item end -->
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </section>
        <!-- Team end -->

        <!-- customer Start -->
        <section id="rs-customer" class="rs-defult-customer rs-customer3 gray-color">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6 col-sm-12 mb-sm-50">
                        <!-- Contact Start -->
                        <section id="rs-contact" class="rs-contact">
                            <div class="contact-bg">
                                <div class="contact-form">
                                    <div class="sec-title">
                                        <h3 class="contact-title">Need a Quick Query</h3>
                                    </div>
                                    
                                    <form  method="post" action="<?php echo e(route('save_query')); ?>">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-field">
                                                    <input type="text" placeholder="Name" id="name" name="name" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-field">
                                                    <input type="email" placeholder="E-Mail" id="email" name="email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-field">
                                                    <input type="text" placeholder="Phone Number" id="phone_number" name="phone" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-field">
                                            <textarea placeholder="Your Message Here" rows="2" id="message" name="message" required></textarea>
                                        </div>
                                        <div class="form-button text-left">
                                            <button type="submit" class="readon">Submit Now</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                        <!-- Contact End -->
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="rs-carousel owl-carousel" data-loop="true" data-items="1" data-margin="30" data-autoplay="true" data-autoplay-timeout="7000" data-smart-speed="2000" data-dots="true" data-nav="false" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="1" data-md-device-nav="false" data-md-device-dots="true">
                        <?php $teams = \App\Testmonial::where('type','testimonial')->get(); ?>
                        <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <!-- Style 1 Start -->
                            <div class="customer-item">
                                <div class="customer-part">
                                    <div class="item-img">
                                        <img src="<?php echo e($team->image_path); ?>" alt="">
                                    </div>
                                    <div class="item-details">
                                        <p><?php echo e($team->message); ?></p>
                                    </div>
                                    <div class="item-author">
                                        <div class="item-name">
                                            <?php echo e($team->name); ?>

                                        </div>
                                        <div class="item-designation"><?php echo e($team->title); ?></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Style 1 End -->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- customar end -->

        <!-- Blog Start -->
        <section id="rs-blog" class="rs-blog sec-spacer">
            <div class="container">
                <div class="sec-title">
                    <h3>Latest News</h3>
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="3" data-md-device-nav="true" data-md-device-dots="false">
                    <?php
                        $blogs = \App\Blog::all();
                    function strWordCut($string,$length)
                            {
                                $str_len = strlen($string);
                                $string = strip_tags($string);

                                if ($str_len > $length) {

                                    // truncate string
                                    $stringCut = substr($string, 0, $length-15);
                                    $string = $stringCut.'.....'.substr($string, $str_len-10, $str_len-1);
                                }
                                return $string;
                            }
                    ?>
                    <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="blog-item">
                        <div class="blog-img">
                            <img src="<?php echo e($blog->image_path); ?>" alt="Blog Image">
                            <div class="blog-img-content">
                                <div class="display-table">
                                    <div class="display-table-cell">
                                        <a class="blog-link" href="<?php echo e(route('single_post',$blog->id)); ?>" title="Blog Link">
                                            <i class="fa fa-link"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-wrapper">
                            <div class="blog-meta">
                                <ul>
                                    
                                    <li><i class="fa fa-calendar"></i><span><?php echo e($blog->dateOfPublich); ?></span></li>
                                </ul>
                            </div>
                            <div class="blog-desc">
                                <a href="#"><?php echo e($blog->head); ?></a>
                                <p><?php echo strWordCut($blog->body,200); ?></p>
                            </div>
                            <a href="<?php echo e(route('single_post',$blog->id)); ?>" class="readon">Read More</a>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </section>
        <!-- Blog End -->

        <!-- Partner Start -->
        <div id="rs-defult-partner" class="rs-defult-partner sec-color pt-100 pb-100">
            <div class="container">
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="5" data-margin="30" data-autoplay="true" data-autoplay-timeout="8000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="2" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="5" data-md-device-nav="true" data-md-device-dots="false">
                    <?php $clients = \App\Client::all(); ?>
                    <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="partner-item">
                        <a href="javascript:void (0)"><img src="<?php echo e($client->image_path); ?>" alt="Partner Image"></a>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </div>
        <!-- Partner End -->
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('hepta.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>