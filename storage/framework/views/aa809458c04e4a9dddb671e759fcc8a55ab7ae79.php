<?php $__env->startSection('content'); ?>
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Breadcrumbs Start -->
        <div class="rs-breadcrumbs sec-color">
            <div class="breadcrumbs-image">
                <img src="<?php echo e(asset('hepta/images/breadcrumbs/project-slider.jpg')); ?>" alt="Breadcrumbs Image">
                <div class="breadcrumbs-inner">
                    <div class="container">
                        <div class="breadcrumbs-text">
                            <h1 class="breadcrumbs-title"><?php echo e($post->head); ?></h1>
                            <ul class="breadcrumbs-subtitle">
                                <li><a href="<?php echo e(route('index')); ?>"><i class="fa fa-home"></i>  Home</a></li>
                                
                                <li><?php echo e($post->head); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs End -->

        <!-- Blog Detail Start -->
        <div class="rs-blog-details sec-spacer">
            <div class="container">
                <div class="full-width-blog">
                    <div class="h-img">
                        <img src="<?php echo e($post->image_path); ?>" class="img-responsive img-thumbnail" alt="Blog Image">
                    </div>

                    <div class="h-info">
                        <ul class="h-meta">

                            <li>
                                    <span class="p-date">
                                        <i class="fa fa-calendar"></i><?php echo e($post->dateOfPublich); ?>

                                    </span>
                            </li>


                        </ul>
                    </div>

                    <div class="h-desc">
                        <?php echo $post->body; ?>

                    </div>


                </div>
            </div>
        </div>
        <!-- Blog Detail End -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('hepta.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>