
<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <?php $__currentLoopData = $billing_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $billing_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <form action="<?php echo e(route('deleteItem',$billing_account->id)); ?>" method="post" id="form-<?php echo e($billing_account->id); ?>">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('delete')); ?>

                        <input type="hidden" name="class_name" value="<?php echo e(get_class($billing_account)); ?>">
                    </form>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <!-- DOM dataTable -->
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">ارقام الحسابات</h4>
                            <a class="btn btn-primary pull-left" href="<?php echo e(route('create_billing_account')); ?>">اضف حساب جديد</a>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div class="table-responsive">
                                <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>اسم البنك</th>
                                        <th>Bank name</th>
                                        <th>رقم الحساب</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>اسم البنك</th>
                                        <th>Bank name</th>
                                        <th>رقم الحساب</th>
                                        <th>حذف</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php $__currentLoopData = $billing_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $billing_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($billing_account->id); ?></td>
                                            <td><?php echo e($billing_account->name_ar); ?></td>
                                            <td><?php echo e($billing_account->name_en); ?></td>
                                            <td><?php echo e($billing_account->account_number); ?></td>
                                            <td><button onclick="JSalert(<?php echo e($billing_account->id); ?>)"  id="<?php echo e($billing_account->id); ?>"  class="btn btn-danger">حذف</button></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>