<?php $__env->startSection('main'); ?>
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title" style="margin-bottom: 15px;">المميزات <small><?php echo e($features->total()); ?></small></h4>
                    <form action="<?php echo e(route('features')); ?>" method="get">
                        <div class="row">
                            
                                
                            
                            <div class=" col-lg-12 col-md-12 pull-left">
                                
                                    <a href="<?php echo e(route('features-create')); ?>" class=" btn btn-primary btn-sm"> <i class="fa fa-plus"></i>اضافة</a>
                          
                            </div>

                        </div>
                    </form>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="table-responsive">
                        <?php if($features->count() > 0 ): ?>
                            <table  class="table table-striped" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>

                                    <th>النوع</th>
                                    <th>الصورة /الايقونه</th>
                                    <th>العنوان </th>
                                    <th>الوصف</th>
                                    <th>التحكم</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $features; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $feature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($index + 1); ?></td>
                                        <td><img src="<?php echo e($feature->image_path); ?>" class="img-thumbnail" style="width: 250px"></td>
                                        <td><?php echo e($feature->type); ?></td>
                                        <td><?php echo e($feature->head); ?></td>
                                        <td><?php echo $feature->description; ?></td>
                                        <td>
                                                <a href="<?php echo e(route('features-edit',$feature->id)); ?>" class="btn btn-info btn-sm">تعديل</a>
                                                <form action="<?php echo e(route('features-destroy',$feature->id)); ?>" method="post" style="display: inline-block">
                                                    <?php echo e(csrf_field()); ?>

                                                    <?php echo e(method_field('delete')); ?>

                                                    <button type="submit" class="btn btn-danger delete btn-sm">حذف</button>
                                                </form>

                                        </td>


                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div style="text-align: center"><?php echo e($features->appends(request()->query())->links()); ?></div>

                        <?php else: ?>

                            <h3>عذرا لايوجد سجلات</h3>
                        <?php endif; ?>
                    </div>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>