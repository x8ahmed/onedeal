<?php $__env->startSection('main'); ?>
    <div class="container">
        <header class="page-header">
            <h1 class="page-title"><?php echo e($cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></h1>
            <ol class="breadcrumb page-breadcrumb">
                <li><a href="<?php echo e(route('index')); ?>"><?php echo e(trans('index.home')); ?></a>
                </li>
                <?php
                if ($cat->parent_id !=0){
                   $p_name = \App\Category::find($cat->parent_id)['name_'.\Illuminate\Support\Facades\Lang::getLocale()];
                }
                ?>
                <?php if($cat->parent_id !=0): ?>
                <li><a href="<?php echo e(route('tags',str_replace(' ', '-',$p_name))); ?>"><?php echo e($p_name); ?></a>
                </li>
                <?php endif; ?>
                <li class="active"><?php echo e($cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></li>
            </ol>
            <ul class="category-selections clearfix">

                <li><span class="category-selections-sign"><?php echo e(trans('index.all_categories')); ?></span>
                    <select class="category-selections-select">
                        <option selected>Newest First</option>
                        <option>Best Sellers</option>
                        <option>Trending Now</option>
                        <option>Best Raited</option>
                        <option>Price : Lowest First</option>
                        <option>Price : Highest First</option>
                        <option>Title : A - Z</option>
                        <option>Title : Z - A</option>
                    </select>
                </li>
            </ul>
        </header>
        <div class="row" data-gutter="15">
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-3">
                <div class="product ">
                    <ul class="product-labels"></ul>
                    <div class="product-img-wrap">
                        <img class="product-img-primary" src="<?php echo e(asset($product->thumbnail)); ?>" alt="Image Alternative text" title="Image Title" />
                        <img class="product-img-alt" src="<?php echo e(asset($product->thumbnail)); ?>" alt="Image Alternative text" title="Image Title" />
                    </div>
                    <a class="product-link" href="<?php echo e(route('single_product',$product->id)); ?>"></a>
                    <div class="product-caption">

                        <h5 class="product-caption-title"><?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></h5>
                        <div class="product-caption-price"><span class="product-caption-price-new"><?php echo e($product->price.trans('index.price')); ?></span>
                        </div>

                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>


        <div class="row">

            <div class="col-md-6">
                <nav>
                    <ul class="pagination category-pagination pull-right">
                        <?php for($x=1;$x<$products->lastPage();$x++): ?>
                        <li class="<?php echo e(($x==1)?"active":""); ?>"><a href="<?php echo e(\Illuminate\Support\Facades\URL::current().'?page='.$x); ?>"><?php echo e($x); ?></a>
                        </li>
                        <?php endfor; ?>
                        <li class="last"><a href="<?php echo e(\Illuminate\Support\Facades\URL::current().'?page='.$products->lastPage()); ?>"><i class="fa fa-long-arrow-right"></i></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="gap"></div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>