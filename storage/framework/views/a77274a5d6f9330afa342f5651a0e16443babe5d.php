<!DOCTYPE HTML>
<html>


<!-- Mirrored from remtsoy.com/tf_templates/the_box/demo_v1_6/index-layout-2.html by amir Website Copier/3.x [XR&CO'2014], Thu, 14 Feb 2019 15:46:09 GMT -->
<head>
    <title>موقع تذكار الشرق</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="TheBox - premium e-commerce template">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:500,300,700,400italic,400' rel='stylesheet' type='text/css'>
    <!-- <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'> -->
    <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'> -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo e(asset('new/css/bootstrap.css')); ?>">

    <script src="https://use.fontawesome.com/5c908493b6.js"></script>
    <link rel="stylesheet" href="<?php echo e(asset('new/css/styles.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('new/css/mystyles.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('new/css/switcher.css')); ?>" />
    <?php echo $__env->yieldContent('style'); ?>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>
    <style>
        #mapid { height: 300px; }
    </style>
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/bright-turquoise.css')); ?>" title="bright-turquoise" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/turkish-rose.css')); ?>" title="turkish-rose" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/salem.css')); ?>" title="salem" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/hippie-blue.css')); ?>" title="hippie-blue" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/mandy.css')); ?>" title="mandy" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/green-smoke.css')); ?>" title="green-smoke" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/horizon.css')); ?>" title="horizon" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/cerise.css')); ?>" title="cerise" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/brick-red.css')); ?>" title="brick-red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/de-york.css')); ?>" title="de-york" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/shamrock.css')); ?>" title="shamrock" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/studio.css')); ?>" title="studio" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/leather.css')); ?>" title="leather" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/denim.css')); ?>" title="denim" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="<?php echo e(asset('new/css/schemes/scarlet.css')); ?>" title="scarlet" media="all" />
    <?php if(\Illuminate\Support\Facades\Lang::getLocale()=='ar'): ?>
   <!--rtl -->
   <link href="<?php echo e(asset('new/css/bootstrap-rtl.min.css')); ?>" rel="stylesheet" type="text/css" media="all" />
   <link href="<?php echo e(asset('new/css/rtl.css')); ?>" rel="stylesheet" type="text/css" media="all" />
   
   <?php endif; ?>
   <link href="<?php echo e(asset('new/logo.png')); ?>" rel="shortcut icon" />

</head>

<body>
<div class="global-wrapper clearfix" id="global-wrapper">

    <div class="navbar-before mobile-hidden navbar-before-inverse">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p class="navbar-before-sign"><?php echo e(trans('index.header_text')); ?></p>
                    
                </div>
                <div class="col-md-4"><p id="current_date" style="color: #ffffff;text-align: center"></p></div>
                <div class="col-md-4">

                    <ul class="nav navbar-nav navbar-right navbar-right-no-mar">
                        <li>
                            <?php if(Lang::getLocale()=='ar'): ?>
                                <a href="<?php echo e(Laravellocalization::getLocalizedUrl('en')); ?>" class="lang">
                                    <i class="fa fa-globe"></i>
                                    English
                                </a>
                            <?php elseif(Lang::getLocale()=='en'): ?>
                                <a href="<?php echo e(Laravellocalization::getLocalizedUrl('ar')); ?>" class="lang">
                                    <i class="fa fa-globe"></i>
                                    عربى
                                </a>
                            <?php else: ?>
                                <a href="<?php echo e(Laravellocalization::getLocalizedUrl()=='ar'); ?>" class="lang">
                                    <i class="fa fa-globe"></i>
                                    عربى
                                </a>
                            <?php endif; ?>
                        </li>

                        
                        

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="mfp-with-anim mfp-hide mfp-dialog clearfix" id="nav-login-dialog">
        <h3 class="widget-title">Member Login</h3>
        <p>Welcome back, friend. Login to get started</p>
        <hr />
        <form>
            <div class="form-group">
                <label>Email or Username</label>
                <input class="form-control" type="text" />
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="form-control" type="text" />
            </div>
            <div class="checkbox">
                <label>
                    <input class="i-check" type="checkbox" />Remeber Me</label>
            </div>
            <input class="btn btn-primary" type="submit" value="Sign In" />
        </form>
        <div class="gap gap-small"></div>
        <ul class="list-inline">
            <li><a href="#nav-account-dialog" class="popup-text">Not Member Yet</a>
            </li>
            <li><a href="#nav-pwd-dialog" class="popup-text">Forgot Password?</a>
            </li>
        </ul>
    </div>
    <div class="mfp-with-anim mfp-hide mfp-dialog clearfix" id="nav-account-dialog">
        <h3 class="widget-title">Create TheBox Account</h3>
        <p>Ready to get best offers? Let's get started!</p>
        <hr />
        <form>
            <div class="form-group">
                <label>Email</label>
                <input class="form-control" type="text" />
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="form-control" type="text" />
            </div>
            <div class="form-group">
                <label>Repeat Password</label>
                <input class="form-control" type="text" />
            </div>
            <div class="form-group">
                <label>Phone Number</label>
                <input class="form-control" type="text" />
            </div>
            <div class="checkbox">
                <label>
                    <input class="i-check" type="checkbox" />Subscribe to the Newsletter</label>
            </div>
            <input class="btn btn-primary" type="submit" value="Create Account" />
        </form>
        <div class="gap gap-small"></div>
        <ul class="list-inline">
            <li><a href="#nav-login-dialog" class="popup-text">Already Memeber</a>
            </li>
        </ul>
    </div>
    <div class="mfp-with-anim mfp-hide mfp-dialog clearfix" id="nav-pwd-dialog">
        <h3 class="widget-title">Password Recovery</h3>
        <p>Enter Your Email and We Will Send the Instructions</p>
        <hr />
        <form>
            <div class="form-group">
                <label>Your Email</label>
                <input class="form-control" type="text" />
            </div>
            <input class="btn btn-primary" type="submit" value="Recover Password" />
        </form>
    </div>
    <nav class="navbar navbar-inverse navbar-main yamm">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#main-nav-collapse" area_expanded="false"><span class="sr-only">Main Menu</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo e(route('index')); ?>">
                    <img src="<?php echo e(asset('new/logo.png')); ?>" alt="logo" title="logo" />                </a>

            </div>
            <div class="collapse navbar-collapse" id="main-nav-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="#"><i class="fa fa-reorder"></i>&nbsp; <?php echo e(trans('index.all_categories')); ?><i class="drop-caret" data-toggle="dropdown"></i></a>
                        <ul class="dropdown-menu dropdown-menu-category">
                            <?php $parent_cats = \App\Category::where('parent_id',0)->get(); ?>
                            <?php $__currentLoopData = $parent_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <li><a href="<?php echo e(route('tags',str_replace(' ', '-',$parent_cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]))); ?>"><?php echo e($parent_cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></a>
                                <div class="dropdown-menu-category-section">
                                    <div class="dropdown-menu-category-section-inner">
                                        <div class="dropdown-menu-category-section-content">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h5 class="dropdown-menu-category-title"><?php echo e($parent_cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></h5>
                                                    <ul class="dropdown-menu-category-list">
                                                        <?php $parent_cats_childs = \App\Category::where('parent_id',$parent_cat->id)->get(); ?>
                                                        <?php $__currentLoopData = $parent_cats_childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <li><a href="<?php echo e(route('tags',str_replace(' ', '-',$child['name_'.\Illuminate\Support\Facades\Lang::getLocale()]))); ?>"><?php echo e($child['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></a>
                                                                
                                                            </li>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <img class="dropdown-menu-category-section-theme-img" src="<?php echo e(asset($parent_cat->picture)); ?>" alt="Image Alternative text" title="Image Title" />
                                    </div>
                                </div>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </ul>
                    </li>
                </ul>
                <form action="<?php echo e(url('search')); ?>" method="get" class="navbar-form navbar-left navbar-main-search" role="search">
                    <div class="form-group">
                        <input class="form-control" type="text" name="searchWord" placeholder="<?php echo e(trans('index.search')); ?>" />
                    </div>
                    <button type="submit" class="fa fa-search navbar-main-search-submit" href="#"></button>
                </form>
                <ul class="nav navbar-nav navbar-right user-logged">
                    <?php if(!\Auth::id()): ?>
                        <li><a href="<?php echo e(route('login')); ?>"  ><?php echo e(trans('index.sign_in')); ?></a>
                        </li>
                        <li><a href="<?php echo e(route('register')); ?>"  ><?php echo e(trans('index.register')); ?></a>
                        </li>
                    <?php else: ?>
                        <li>
                            
                            <div class="dropdown">
                                <a id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user mr-2"></i><?php echo e(\Auth::user()->name); ?></a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><?php echo e(trans('index.logout')); ?></a>
                                    <a class="dropdown-item" href="<?php echo e(route('my_orders')); ?>"><?php echo e(trans('index.my_orders')); ?></a>
                                    <a class="dropdown-item" href="<?php echo e(route('update_profile')); ?>"><?php echo e(trans('index.update_profile')); ?></a>
                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                </div>
                            </div><!--end dropdown class -->

                            <!--<i class="fas fa-map-marker mr-2"></i>Select Location</a>-->
                        </li>
                    <?php endif; ?>
                        <li><a href="<?php echo e(route('index')); ?>"  ><?php echo e(trans('index.home')); ?></a>


                        <li>
                        <a class="fa fa-shopping-cart my-cart-icon" href="javascript:void(0)"><span class="badge badge-notify my-cart-badge"></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


<?php echo $__env->yieldContent('main'); ?>

    <footer class="main-footer">
        <div class="container">
            <div class="row row-col-gap" data-gutter="60">
                <div class="col-md-4">
                    <h4 class="widget-title-sm">
                        <img src="<?php echo e(asset('new/logo (1).png')); ?>" class="f-logo">
                    </h4>
                    
                        
                            
                        
                        
                            
                        
                        
                            
                        
                        
                            
                        
                        
                            
                        
                    
                    <ul class="d-social">
                        
                        
                        

                            
                            

                        
                        
                    </ul>
                </div>
                <div class="col-md-4">
                    <h4 class="widget-title-sm"><?php echo e(trans('index.all_categories')); ?></h4>
                    <ul class="main-footer-tag-list">
                        <?php $__currentLoopData = $parent_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="<?php echo e(route('tags',str_replace(' ','-',$parent_cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]))); ?>"><?php echo e($parent_cat['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></a>
                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
                <div class="col-md-4" >
                    <h4 class="widget-title-sm"><?php echo e(trans('index.hq')); ?></h4>
                    <div id="mapid"></div>
                </div>
            </div>
            <!-- <ul class="main-footer-links-list">
                <li><a href="#">About Us</a>
                </li>
                <li><a href="#">contact Us</a>
                </li>
            </ul> -->
        </div>
    </footer>
    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="copyright-text"><?php echo trans('index.copyright'); ?></p>
                </div>
                <!-- <div class="col-md-6">
                    <ul class="payment-icons-list">
                        <li>
                            <img src="img/payment/visa-straight-32px.png" alt="Image Alternative text" title="Pay with Visa" />
                        </li>
                        <li>
                            <img src="img/payment/mastercard-straight-32px.png" alt="Image Alternative text" title="Pay with Mastercard" />
                        </li>
                        <li>
                            <img src="img/payment/paypal-straight-32px.png" alt="Image Alternative text" title="Pay with Paypal" />
                        </li>
                        <li>
                            <img src="img/payment/visa-electron-straight-32px.png" alt="Image Alternative text" title="Pay with Visa-electron" />
                        </li>
                        <li>
                            <img src="img/payment/maestro-straight-32px.png" alt="Image Alternative text" title="Pay with Maestro" />
                        </li>
                        <li>
                            <img src="img/payment/discover-straight-32px.png" alt="Image Alternative text" title="Pay with Discover" />
                        </li>
                    </ul>
                </div> -->
            </div>
        </div>
    </div>
</div>
<script src="<?php echo e(asset('new/js/jquery.js')); ?>"></script>
<script src="<?php echo e(asset('new/js/bootstrap.js')); ?>"></script>
<script src="<?php echo e(asset('new/js/icheck.js')); ?>"></script>
<script src="<?php echo e(asset('new/js/ionrangeslider.js')); ?>"></script>
<script src="<?php echo e(asset('new/js/jqzoom.js')); ?>"></script>
<script src="<?php echo e(asset('new/js/card-payment.js')); ?>"></script>
<script src="<?php echo e(asset('new/js/owl-carousel.js')); ?>"></script>
<script src="<?php echo e(asset('new/js/magnific.js')); ?>"></script>
<script src="<?php echo e(asset('new/js/custom.js')); ?>"></script>


<script src="<?php echo e(asset('new/js/switcher.js')); ?>"></script>
<script src="<?php echo e(asset('new/js/jquery.mycart.js')); ?>"></script>
<script>
    $(function () {

        var goToCartIcon = function($addTocartBtn){
            var $cartIcon = $(".my-cart-icon");
            var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
            $addTocartBtn.prepend($image);
            var position = $cartIcon.position();
            $image.animate({
                top: position.top,
                left: position.left
            }, 500 , "linear", function() {
                $image.remove();
            });
        }

        $('.my-cart-btn').myCart({
            classCartIcon: 'my-cart-icon',
            classCartBadge: 'my-cart-badge',
            affixCartIcon: true,
            checkoutCart: function(products) {
                $.each(products, function(){
                    console.log(this);
                });
            },
            clickOnAddToCart: function($addTocart){
                goToCartIcon($addTocart);
            },
            getDiscountPrice: function(products) {
                var total = 0;
                $.each(products, function(){
                    total += this.quantity * this.price;
                });
                return total * 0.5;
            }
        });

    });

</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<?php if(\Session::has('message')): ?>
    <script>
        swal("<?php echo e(\Session::get('message')); ?>", "", "success");
        //ProductManager.clearProduct();
        localStorage.clear();

    </script>
<?php endif; ?>
    
    
            
            


    
        

        

        
            
            
            
            
            
        
        
        
            
            
            
            
        


    

<script>
    $('#agileinfo-nav_search').change(function () {
        window.location.replace('<?php echo e(url('tags')); ?>/'+$('#agileinfo-nav_search option:selected').val());
    });

    function doDate()
    {
        var str = "";
        <?php if(\Illuminate\Support\Facades\Lang::getLocale()=='ar'): ?>
        var days = new Array("الاحد", "الاثنين", "الثلاثاء", "الاربعاء", "الخميس", "الجمعه", "السبت");
        var months = new Array("يناير", "فبراير", "مارس", "ابريل", "مايو", "يونية", "يوليو", "اغسطس", "سبتمير", "اكتوبر", "نوفمبر", "ديسمبر");
            <?php else: ?>
        var days = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        <?php endif; ?>



        var now = new Date();

        str += days[now.getDay()] + ", " + now.getDate() + " " + months[now.getMonth()] + " " + now.getFullYear() + " " + now.getHours() +":" + now.getMinutes() + ":" + now.getSeconds();
        document.getElementById("current_date").innerHTML = str;
    }

    setInterval(doDate, 1000);

</script>

</body>


<!-- Mirrored from remtsoy.com/tf_templates/the_box/demo_v1_6/index-layout-2.html by amir Website Copier/3.x [XR&CO'2014], Thu, 14 Feb 2019 15:50:37 GMT -->
</html>
