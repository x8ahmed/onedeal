<?php $__env->startSection('main'); ?>

    <div class="row">
        <div class="col-md-12 pull-right">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">تعديل العملاء</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        
                        
                        
                    </div>
                    <?php echo $__env->make('admin.layouts.massege', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <form action="<?php echo e(route('blog-update',$blog->id)); ?>" method="post" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('put')); ?>


                        <div class="form-group">
                            <label>الصورة المصغرة</label>
                            <input type="file" name="thumbnail" class="form-control image">
                        </div>

                        <div class="form-group">
                            <img src="<?php echo e($blog->image_path); ?>" style="width: 120px;" class="image-reviw"/>
                        </div>
                        <div class="form-group">
                            <label> العنوان</label>
                            <input type="text" name="head" class="form-control" value="<?php echo e($blog->head); ?>">
                        </div>

                        <div class="form-group">
                            <label> المحتوي</label>
                            <textarea type="text" name="body" class="form-control ckeditor"><?php echo e($blog->body); ?></textarea>
                        </div>

                        <div class="form-group">
                            <label> تاريخ النشر</label>
                            <input type="date" name="dateOfPublich" class="form-control" value="<?php echo e($blog->dateOfPublich); ?>">
                        </div>


                        <button type="submit" class="btn btn-primary btn-md">تعديل</button>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->



    </div><!-- .row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>