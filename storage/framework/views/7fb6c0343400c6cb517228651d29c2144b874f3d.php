
<?php $__env->startSection('main'); ?>
    <?php $product_pictures =   isset($product)? \App\ProductPicture::where('product_id',$product->id)->get() : [0=>['picture'=>$text->text]] ; ?>

    <?php $product = isset($product)?$product:$text; ?>


    <div class="container">
        <header class="page-header">
            <ol class="breadcrumb page-breadcrumb">
                <li>
                    <a href="<?php echo e(route('index')); ?>"><?php echo e(trans('home')); ?></a>
                </li>
                <li><a href="<?php echo e(route('tags',str_replace(' ','-',\App\Category::where('id',\App\Category::find($product->category_id)->parent_id)->first()['name_'.\Illuminate\Support\Facades\Lang::getLocale()]))); ?>"><?php echo e(\App\Category::where('id',\App\Category::find($product->category_id)->parent_id)->first()['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></a>
                </li>
                <li><a href="<?php echo e(route('tags',str_replace(' ','-',\App\Category::find($product->category_id)['name_'.\Illuminate\Support\Facades\Lang::getLocale()]))); ?>"><?php echo e(\App\Category::find($product->category_id)['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></a>
                </li>
                <li class="active"><?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></li>
            </ol>
        </header>

        <div class="row">
            <div class="col-md-6">
                <div class="product-page-product-wrap jqzoom-stage jqzoom-stage-lg">
                    <div class="clearfix">
                        <a href="<?php echo e(asset($product_pictures[0]['picture'])); ?>" id="jqzoom" data-rel="gal-1">
                            <img src="<?php echo e(asset($product_pictures[0]['picture'])); ?>" alt="Image Alternative text" title="Image Title" />
                        </a>
                    </div>
                </div>
                <ul class="jqzoom-list">
                    <?php $__currentLoopData = $product_pictures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_picture): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                        <a class="<?php echo e(($loop->first)?'zoomThumbActive':''); ?>" href="javascript:void(0)" data-rel="{gallery:'gal-1', smallimage: '<?php echo e(asset($product_picture['picture'])); ?>', largeimage: '<?php echo e(asset($product_picture['picture'])); ?>'}">
                            <img src="<?php echo e(asset($product_picture['picture'])); ?>" alt="Image Alternative text" title="Image Title" />
                        </a>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </ul>
            </div>
            <?php if(!isset($text)): ?>
            <div class="col-md-6">
                <div class="_box-highlight">

                    <h1><?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></h1>
                    <p class="product-page-price"><?php echo e($product->price.trans('index.price')); ?></p>
                    
                    <p class="product-page-desc-lg"><?php echo $product['description_'.\Illuminate\Support\Facades\Lang::getLocale()]; ?></p>

                    <ul class="product-page-actions-list">
                        <?php if($product->quantity !=0): ?>
                            <button class="btn btn-lg btn-primary my-cart-btn"
                                    data-id="<?php echo e($product->id); ?>" data-name="<?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?>"

                                    data-price="<?php echo e($product->price); ?>" data-quantity="1" data-image="<?php echo e(asset($product->thumbnail)); ?>">
                                <?php echo e(trans('index.add_to_cart')); ?>

                            </button>
                            <?php else: ?>
                            <button type="button" class="btn btn-lg btn-danger"><?php echo e(trans('index.out_of_stock')); ?></button>
                        <?php endif; ?>


                        
                        

                    </ul>
                    <div class="gap gap-small"></div>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="gap"></div>
        <?php if(!isset($text)): ?>
        <h3 class="widget-title"><?php echo e(trans('index.similar_products')); ?></h3>
        <?php $similar_products = \App\Product::where('category_id',$product->category_id)->orderBy('id','desc')->take(12)->get() ?>
        <div class="row">
            <?php $__currentLoopData = $similar_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $similar_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-3">
                <div class="product product-sm-left ">
                    <ul class="product-labels"></ul>
                    <div class="product-img-wrap">
                        <img class="product-img" src="<?php echo e(asset($similar_product->thumbnail)); ?>" alt="Image Alternative text" title="Image Title" />
                    </div>
                    <a class="product-link" href="<?php echo e(route('single_product',$similar_product->id)); ?>"></a>
                    <div class="product-caption">

                        <h5 class="product-caption-title"><?php echo e($similar_product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></h5>
                        <div class="product-caption-price"><span class="product-caption-price-new"><?php echo e($similar_product->price.trans('index.price')); ?></span>
                        </div>

                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
            <?php endif; ?>

    </div>
    <div class="gap"></div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>


    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>