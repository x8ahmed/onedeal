
<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">تعديل تصنيف </h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="<?php echo e(route('store_update_category',$category->id)); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <?php echo e(method_field('PUT')); ?>

                                <div class="form-group">
                                    <label for="parent_id">اختار التصنيف الرئيسى</label>
                                    <div>
                                        <select class="form-control" id="parent_id" name="parent_id" required>
                                            <option value="0">بدون</option>
                                            <?php $parents = \App\Category::where('parent_id',0)->get() ?>
                                            <?php $__currentLoopData = $parents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($parent->id); ?>" <?php echo e(($parent->id==$category->parent_id)?'selected':''); ?>><?php echo e($parent->name_ar); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="name_ar">الاسم باللغة العربية</label>
                                    <input type="text" value="<?php echo e($category->name_ar); ?>" name="name_ar" id="name_ar" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="name_en">الاسم باللغة الانجليزية</label>
                                    <input type="text" value="<?php echo e($category->name_en); ?>" name="name_en" id="name_en" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="picture">صورة التصنيف</label>
                                    <span class="text-danger">عند اضافة صورة سيتم حذف الصورة واضافة الصورة الجديدة اذا اردت عدم تغيير الصورة لا تضف صورة</span>
                                    <img src="<?php echo e(asset($category->picture)); ?>" class="img-responsive d-block" style="width: 150px;height: 150px">
                                    <input type="file" name="picture" id="picture" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="cat_type">نوع التصنيف</label>
                                    <div>
                                        <label>
                                            <input type="radio" name="cat_type" id="cat_type" value="product" required <?php echo e(($category->cat_type=='product')?'checked':''); ?>>
                                            منتج
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" name="cat_type" id="cat_type" value="text" <?php echo e(($category->cat_type=='text')?'checked':''); ?>>
                                            نص
                                        </label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-md">تعديل</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>