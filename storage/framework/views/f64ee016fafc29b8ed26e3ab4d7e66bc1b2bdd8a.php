<?php $__env->startSection('main'); ?>
    <!-- banner-2 -->
    <div class="page-head_agile_info_w3l">

    </div>
    <!-- //banner-2 -->
    <!-- page -->
    <div class="services-breadcrumb">
        <div class="agile_inner_breadcrumb">
            <div class="container">
                <ul class="w3_short">
                    <li>
                        <a href="index.html">الرئيسية</a>
                        <!--<a href="index.html">Home</a>-->
                        <i>|</i>
                    </li>
                    <li>الدفع</li>
                    <!--<li>Checkout</li>-->
                </ul>
            </div>
        </div>
    </div>
    <!-- //page -->
    <!-- checkout page -->
    <div class="privacy py-sm-5 py-4">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <span>الدفع</span>
                <!--<span>C</span>heckout-->
            </h3>
            <!-- //tittle heading -->
            <div class="checkout-right">
                <!--<h4 class="mb-sm-4 mb-3">Your shopping cart contains:-->
                <h4 class="mb-sm-4 mb-3" id="products_number">سلة المشتريات تحتوى على:
                    <span>3 منتجات</span>
                    <!--<span>3 Products</span>-->
                </h4>
                <div class="table-responsive">
                    <table class="timetable_sub" id="products">
                        <thead>
                        <!--<tr>-->
                        <!--<th>SL No.</th>-->
                        <!--<th>Product</th>-->
                        <!--<th>Quality</th>-->
                        <!--<th>Product Name</th>-->

                        <!--<th>Price</th>-->
                        <!--<th>Remove</th>-->
                        <!--</tr>-->
                        <tr>
                            <th>اسم المنتج</th>
                            <th>الكمية</th>
                            <th>السعر</th>
                            <th>الاجمالى</th>
                            <th>النص المراد كتابته</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="3">الاجمالى</th>

                            <th id="total_footer">الاجمالى</th>
                            <th>النص المراد كتابته</th>
                        </tr>
                        </tfoot>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>وسيلة الشحن</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <select class="form-control" id="shipping" style="margin-bottom: 10px">
                                    <?php $shippings = \App\Shipping::where('parent_id',0)->get(); ?>
                                    <?php $__currentLoopData = $shippings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shipping): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($shipping->id); ?>" data-shipping-price="<?php echo e($shipping->price); ?>"><?php echo e($shipping->type); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <select class="form-control" id="cities" style="display: none">
                                    <?php $shippings = \App\Shipping::where('parent_id',5)->get(); ?>
                                    <?php $__currentLoopData = $shippings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shipping): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($shipping->id); ?>" data-shipping-price="<?php echo e($shipping->price); ?>"><?php echo e($shipping->type); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>    </div>
                            <div class="col-md-6">
                                <h4 id="shipping_price">سعر الشحن 50 ريال</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>الضريبة</h4>
                    </div>
                    <div class="col-md-6">
                        <h4>
                            <?php $tax = \App\Tax::first(); ?>
                            <?php echo e($tax->tax); ?>%
                        </h4>

                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>الاجمالى بعد الضريبة</h4>
                    </div>
                    <div class="col-md-6" id="total_after_tax">
                        <h4>1820</h4>

                    </div>
                </div>

                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>كود الخصم</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="coupon_value">
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-primary" id="coupon_submit">تحقق</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;display: none">
                    <div class="col-md-6">
                        <h4>الاجمالى بعد الخصم</h4>
                    </div>
                    <div class="col-md-6" id="total_after_coupon">
                        <h4></h4>

                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <h4>وسيلة الدفع</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <select required class="form-control" id="payment" style="margin-bottom: 10px">
                                        <option selected disabled="disabled">اختر وسيلة الدفع</option>
                                        <option value="cash">نقدا فى الفرع</option>
                                        <option value="transfer">تحويل لاحد حسابات المؤسسة</option>
                                </select>
                                <select class="form-control" id="accounts">
                                    <?php $accounts = \App\BillingAccount::all(); ?>
                                    <?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($account->id); ?>"><?php echo e($account['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>ارفق صورة الايصال</label>
                                <input type="file" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="checkout-left">
                <div class="address_form_agile mt-sm-5 mt-4">
                    <!--<h4 class="mb-sm-4 mb-3">Add a new Details</h4>-->
                    <form action="payment.html" method="post" class="creditly-card-form agileinfo_form">
                        <div class="creditly-wrapper wthree, w3_agileits_wrapper">
                            <!--<div class="information-wrapper">-->
                            <!--<div class="first-row">-->
                            <!--<div class="controls form-group">-->
                            <!--<input class="billing-address-name form-control" type="text" name="name" placeholder="Full Name" required="">-->
                            <!--</div>-->
                            <!--<div class="w3_agileits_card_number_grids">-->
                            <!--<div class="w3_agileits_card_number_grid_left form-group">-->
                            <!--<div class="controls">-->
                            <!--<input type="text" class="form-control" placeholder="Mobile Number" name="number" required="">-->
                            <!--</div>-->
                            <!--</div>-->
                            <!--<div class="w3_agileits_card_number_grid_right form-group">-->
                            <!--<div class="controls">-->
                            <!--<input type="text" class="form-control" placeholder="Landmark" name="landmark" required="">-->
                            <!--</div>-->
                            <!--</div>-->
                            <!--</div>-->
                            <!--<div class="controls form-group">-->
                            <!--<input type="text" class="form-control" placeholder="Town/City" name="city" required="">-->
                            <!--</div>-->
                            <!--<div class="controls form-group">-->
                            <!--<select class="option-w3ls">-->
                            <!--<option>Select Address type</option>-->
                            <!--<option>Office</option>-->
                            <!--<option>Home</option>-->
                            <!--<option>Commercial</option>-->

                            <!--</select>-->
                            <!--</div>-->
                            <!--</div>-->
                            <!--<button class="submit check_out btn">Delivery to this Address</button>-->
                            <!--</div>-->
                        </div>
                    </form>
                    <div class="checkout-right-basket">
                        <a href="payment.html">تأكيد الشراء
                            <!--<a href="payment.html">Make a Payment-->
                            <!--<span class="far fa-hand-point-right"></span>-->
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="s_text" value="<?php echo e(\App\SuggestedText::all()); ?>">
    <!-- //checkout page -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        var items = paypals.minicarts.cart.items();
        var total = 0;
        $('#products tbody').empty();
        $('#products_number span').empty();
        $('#products_number span').append('<span>'+items.length+' منتجات</span>');
        for (var x =0;x<items.length;x++){
            console.log(items[x]._total);
            $('#products').append('<tr class="rem1">\n' +
                '                            <td class="invert">'+items[x]._data.item_name+'</td>\n' +
                '                            <td class="invert">\n' +
                '                                <div class="quantity">\n' +
                '                                    <div class="quantity-select">\n' +
                // '                                        <div class="entry value-minus">&nbsp;</div>\n' +
                '                                        <div>\n' +
                '                                            <span>'+items[x]._data.quantity+'</span>\n' +
                '                                        </div>\n' +
                // '                                        <div class="entry value-plus active">&nbsp;</div>\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '                            </td>\n' +
                '                            <td class="invert">'+items[x]._data.amount+'</td>\n' +
                '                            <td class="invert">'+items[x]._data.quantity*items[x]._data.amount+'</td>\n' +
                '                            <td class="invert">' +
                '<select class="form-control suggested_text"><option selected disabled>نصوص مقترحة</option></select>' +
                '</td>\n' +

                '                        </tr>');
            total+=items[x]._data.quantity*items[x]._data.amount;
        }//end for
        $('#total_footer').text(total);
        $('#total_after_tax h4').text(total+total*Number("<?php echo e($tax->tax); ?>")/100);
        var total_after_tax = total+total*Number("<?php echo e($tax->tax); ?>")/100;
        console.log(paypals.minicarts.cart.items());
        var text = JSON.parse($('#s_text').val());
        for (var x =0;x<text.length;x++){
            $('.suggested_text').append('<option>'+text[x].text+'</option>');
        }//end for
        $('.suggested_text').append('<option value="none">اخرى</option>');
        $('.suggested_text').change(function () {
            if($('.suggested_text option:selected').val()=='none'){$(this).hide();$(this).parent().append('<textarea class="form-control" rows="3" id="comment"></textarea>');}
        });
        $('#coupon_submit').click(function () {
            $.ajax({
                url:"<?php echo e(route('check_coupon')); ?>",
                data:{coupon:$('#coupon_value').val()},
                success:function (response) {
                    if(response.status){
                        if (response.data.type == 'price'){
                            $('#total_after_coupon').parent().show();
                            $('#coupon_value').addClass('border-success');
                            $('#coupon_value').addClass('text-success');
                            $('#coupon_value').attr('readonly',true);
                            $('#total_after_coupon h4').text(total_after_tax-response.data.value);
                            //console.log(total_after_tax-response.data.value);
                        }else if(response.data.type == 'percentage'){
                            $('#total_after_coupon').parent().show();
                            $('#coupon_value').addClass('border-success');
                            $('#coupon_value').addClass('text-success');
                            $('#coupon_value').attr('readonly',true);
                            $('#total_after_coupon h4').text(total_after_tax-(response.data.value/100)*total_after_tax);
                        }
                    }else {
                        $('#coupon_value').css('border-color','red');
                        $('#coupon_value').css('color','red');
                    }
                }
            });
        });
        $('#shipping').change(function () {
            if ($('#shipping option:selected').val()==5){
                $('#cities').show();
                $('#shipping_price').text('سعر الشحن'+$('#cities option:selected').data('shipping-price')+' ريال');

            }else {
                $('#shipping_price').text('سعر الشحن'+$('#shipping option:selected').data('shipping-price')+' ريال');
                $('#cities').hide();
            }
        });
        $('#cities').change(function () {
            $('#shipping_price').text('سعر الشحن'+$('#cities option:selected').data('shipping-price')+' ريال');

        });
    </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>