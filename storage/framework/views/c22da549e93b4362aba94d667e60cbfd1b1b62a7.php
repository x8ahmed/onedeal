<?php $__env->startSection('main'); ?>

    <div class="row">
        <div class="col-md-12 pull-right">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">تعديل اسلايدر</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        
                        
                        
                    </div>
                    <?php echo $__env->make('admin.layouts.massege', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <form action="<?php echo e(route('slider-update',$slider->id)); ?>" method="post" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('put')); ?>


                        <div class="form-group">
                            <label for="first_name">الصورة</label>
                            <input type="file" class="form-control img-responsive img-thumbnail image" name="image">
                        </div>
                        <div class="form-group">
                            <img src="<?php echo e($slider->image_path); ?>" style="width: 120px;" class="image-reviw"/>
                        </div>
                        <div class="form-group">
                            <label for="sub_title">العنوان الفرعي</label>
                            <input type="text" class="form-control" name="sub_title" value="<?php echo e($slider->sub_title); ?>">
                        </div>

                        <div class="form-group">
                            <label for="first_name">العنوان الرئيسي</label>
                            <input type="text" class="form-control" name="title" value="<?php echo e($slider->title); ?>">
                        </div>

                        <div class="form-group">
                            <label for="first_name">الوصف</label>
                            <textarea class="form-control ckeditor" name="description">
                            <?php echo e($slider->description); ?>

                        </textarea>
                        </div>



                        <button type="submit" class="btn btn-primary btn-md">اضافة</button>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>