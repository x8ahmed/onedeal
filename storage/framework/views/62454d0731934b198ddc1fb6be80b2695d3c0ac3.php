<?php $__env->startSection('main'); ?>
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">اعدادات الموقع</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <form method="post" action="<?php echo e(route('save_site_settings')); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $setting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="form-group">
                                    <label><?php echo e($setting->display_name); ?></label>
                                    <?php if($setting->input_type=='file'&&$setting->value!=null): ?>
                                        <img src="<?php echo e(asset($setting->value)); ?>" class="img-responsive img-thumbnail" style="width: 200px;display: block;">
                                    <?php endif; ?>
                                    <input type="<?php echo e($setting->input_type); ?>" value="<?php echo e($setting->value); ?>" name="<?php echo e($setting->name); ?>" class="form-control">
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <button type="submit" class="btn btn-primary btn-md">تأكيد</button>
                            </form>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- .row -->


        </section><!-- #dash-content -->
    </div><!-- .wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>