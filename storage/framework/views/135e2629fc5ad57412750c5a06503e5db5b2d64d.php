<?php $__env->startSection('main'); ?>
    <div class="owl-carousel owl-loaded owl-nav-dots-inner" data-options='{"items":1,"loop":true,"autoplay":true,"autoplayTimeout":5000}'>
        <?php $__currentLoopData = $ads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="owl-item">
            <div class="slider-item" style="background-color:#E3D8FF;">
                    <div class="slider-item-inner" style="background-image: url(<?php echo e(asset($ad->picture)); ?>)">
                        <?php $arr = ['mkv','ogg','mp4','flac','webM']; ?>
                        <?php if(in_array(explode('.',$ad->picture)[count(explode('.',$ad->picture))-1],$arr)): ?>
                            <video height="100%" width="100%" autoplay loop muted playsinline>
                                <source src="<?php echo e(asset($ad->picture)); ?>" type="video/mp4">
                                <source src="<?php echo e(asset($ad->picture)); ?>" type="video/ogg">
                                <source src="<?php echo e(asset($ad->picture)); ?>" type="video/webM">
                                <source src="<?php echo e(asset($ad->picture)); ?>" type="video/flac">
                            </video>
                        <?php endif; ?>
                    </div>
            </div>
        </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </div>
    <div class="gap"></div>
    <div class="container">
        <div class="row" data-gutter="15">
            <?php $categories = \App\Category::where('parent_id',0)->get(); ?>
            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-6">
                <div class="banner banner-o-hid" style="background-image:url(<?php echo e(asset($category->picture)); ?>);">
                    <a class="banner-link" href="<?php echo e(route('tags',str_replace(' ','-',$category['name_'.\Illuminate\Support\Facades\Lang::getLocale()]))); ?>"></a>
                    <div class="banner-caption-left">
                        <h5 class="banner-title"><?php echo e($category['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></h5>
                        
                        <p class="banner-shop-now"><?php echo e(trans('index.shop_now')); ?> <i class="fa fa-caret-right"></i>
                        </p>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="gap"></div>
        <h3 class="widget-title"><?php echo e(trans('index.new_products')); ?></h3>
        <div class="owl-carousel owl-loaded owl-nav-out" data-options='{"items":5,"loop":true,"nav":true}'>
          <?php $products = \App\Product::orderBy('id','desc')->get() ?>
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="owl-item">
                <div class="product  owl-item-slide">
                    <ul class="product-labels"></ul>
                    <div class="product-img-wrap">
                        <img class="product-img" src="<?php echo e(asset($product->thumbnail)); ?>" alt="<?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?>" title="<?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?>" />
                    </div>
                    <a class="product-link" href="<?php echo e(route('single_product',$product->id)); ?>"></a>
                    <div class="product-caption">
                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                        <h5 class="product-caption-title"><?php echo e($product['name_'.\Illuminate\Support\Facades\Lang::getLocale()]); ?></h5>
                        <div class="product-caption-price"><span class="product-caption-price-new"><?php echo e($product->price.trans('index.price')); ?></span>
                        </div>
                        
                            
                        
                    </div>
                </div>
            </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
        <div class="gap"></div>
        <h3 class="widget-title"><?php echo e(trans('index.new_text')); ?></h3>
        <div class="owl-carousel owl-loaded owl-nav-out" data-options='{"items":5,"loop":true,"nav":true}'>
            <?php $texts = \App\SuggestedText::orderBy('id','desc')->get(); ?>
            <?php $__currentLoopData = $texts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $text): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="owl-item">
                    <div class="product  owl-item-slide">
                        <ul class="product-labels"></ul>
                        <div class="product-img-wrap">
                            <img class="product-img" src="<?php echo e(asset($text->text)); ?>" />
                        </div>
                        <a class="product-link" href="<?php echo e(route('single_product',[$text->id,'text'])); ?>"></a>
                        <div class="product-caption">
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            </div>
                            
                            
                            
                        </div>
                    </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>
    <div class="gap"></div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>