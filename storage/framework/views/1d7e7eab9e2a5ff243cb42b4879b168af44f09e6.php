<?php $__env->startSection('main'); ?>

    <div class="row">
        <div class="col-md-12 pull-right">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">تعديل العملاء</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        
                        
                        
                    </div>
                    <?php echo $__env->make('admin.layouts.massege', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <form action="<?php echo e(route('pages-update',$page->id)); ?>" method="post" >
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('put')); ?>


                        <div class="form-group">
                            <label> العنوان</label>
                            <input type="text" name="name" class="form-control" value="<?php echo e($page->name); ?>">
                        </div>

                        <div class="form-group">
                            <label> المحتوي</label>
                            <textarea type="text" name="content" class="form-control ckeditor"><?php echo e($page->content); ?></textarea>
                        </div>


                        <button type="submit" class="btn btn-primary btn-md">تعديل</button>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->



    </div><!-- .row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>