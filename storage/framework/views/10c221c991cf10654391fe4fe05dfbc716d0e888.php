<?php $__env->startSection('content'); ?>
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Breadcrumbs Start -->
        <div class="rs-breadcrumbs sec-color">
            <div class="breadcrumbs-image">
                <img src="<?php echo e(asset('hepta/images/breadcrumbs/service.jpg')); ?>" alt="Breadcrumbs Image">
                <div class="breadcrumbs-inner">
                    <div class="container">
                        <div class="breadcrumbs-text">
                            <h1 class="breadcrumbs-title">Services</h1>
                            <ul class="breadcrumbs-subtitle">
                                <li><a href="<?php echo e(route('index')); ?>"><i class="fa fa-home"></i>  Home</a></li>
                                <li>Services</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs End -->

        <!-- HOW WE WORK Start -->
        <div id="how-we-work" class="how-we-work defult-style sec-spacer">
            <div class="container">
                <div class="sec-title extra-none">
                    <h3>We Help Your Business!</h3>
                    <p class="width-70">Duis autem vel eum iriure dolor in hendrerit and vulputate velit esse molest esse diten aese eros et acccumsan et iusto velit esse molestie.</p>
                </div>
                <div class="work-sec-gallery">
                    <div class="row">
                        <?php
                        $projects = \App\Feature::where('type','project')->orderBy('id','desc')->get();
                            function strWordCut($string,$length)
                            {
                                $str_len = strlen($string);
                                $string = strip_tags($string);

                                if ($str_len > $length) {

                                    // truncate string
                                    $stringCut = substr($string, 0, $length-15);
                                    $string = $stringCut.'.....'.substr($string, $str_len-10, $str_len-1);
                                }
                                return $string;
                            }
                        ?>
                        <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-lg-4 col-md-6 mb-30">
                            <div class="work-column">
                                <div class="common-box">
                                    <img src="<?php echo e($project->image_path); ?>" alt="Work Section Image">
                                </div>
                                <div class="work-gallery-caption">
                                    <h4><a href="<?php echo e(route('single_project',$project->id)); ?>"><?php echo e($project->head); ?></a></h4>
                                    <p><?php echo strWordCut($project->description,150); ?></p>
                                </div>
                            </div>
                        </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- HOW WE WORK END -->



        <!-- Partner Start -->
        <div id="rs-defult-partner" class="rs-defult-partner pt-100 pb-100">
            <div class="container">
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="5" data-margin="30" data-autoplay="true" data-autoplay-timeout="8000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="2" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="5" data-md-device-nav="true" data-md-device-dots="false">
                    <?php $clients = \App\Client::all(); ?>
                    <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="partner-item">
                            <a href="javascript:void (0)"><img src="<?php echo e($client->image_path); ?>" alt="Partner Image"></a>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </div>
        <!-- Partner End -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('hepta.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>