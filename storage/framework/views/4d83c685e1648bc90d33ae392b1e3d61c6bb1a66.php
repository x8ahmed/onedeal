
<?php $__env->startSection('main'); ?>


    <!-- checkout page -->
        <div class="privacy orderss py-sm-5 py-4">
            <div class="container py-xl-4 py-lg-2">
                <!-- tittle heading -->
                <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                    طلباتى
                    <!--<span>C</span>heckout-->
                </h3>
                <!-- //tittle heading -->
                <div class="checkout-right">
                    <!--<h4 class="mb-sm-4 mb-3">Your shopping cart contains:-->

                    <div class="table-responsive">
                        <table class="table timetable_sub table-bordered" id="products">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>حالة الطلب</th>
                                <th>وسيلة الشحن</th>
                                <th>وسيلة الدفع</th>
                                <th>صورة الايصال</th>
                                <th>الاجمالى</th>
                                <th colspan="4">المنتجات</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>اسم المنتج</th>
                                <th>الكمية</th>
                                <th>النص المطلوب</th>
                                <th>ملاحظات</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $order_products = \App\OrderProduct::where('order_id',$order->id)->get(); ?>
                                <?php $__currentLoopData = $order_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <?php if($loop->first): ?>
                                            <td rowspan="<?php echo e(count($order_products)); ?>"><?php echo e($order->id); ?></td>
                                            <td rowspan="<?php echo e(count($order_products)); ?>">
                                                <?php if($order->status == 0): ?>
                                                    <?php echo e('قيد المعالجة'); ?>

                                                <?php elseif($order->status == 1): ?>
                                                    <?php echo e('جارى الشحن'); ?>

                                                <?php elseif($order->status == 2): ?>
                                                    <?php echo e('تم الاستلام'); ?>

                                                <?php endif; ?>
                                            </td>
                                            <td rowspan="<?php echo e(count($order_products)); ?>"><?php echo e(\App\Shipping::find($order->shipping_type)->type); ?></td>
                                            <td rowspan="<?php echo e(count($order_products)); ?>"><?php echo e($order->payment_method); ?></td>
                                            <td rowspan="<?php echo e(count($order_products)); ?>"><?php echo ($order->receipt)?'<a href="'.asset($order->receipt).'" target="_blank">صورة الايصال</a>':'لا يوجد'; ?>  </td>
                                            <td rowspan="<?php echo e(count($order_products)); ?>"><?php echo e($order->total_after_coupon); ?></td>
                                        <?php endif; ?>
                                        <td><?php echo e(\App\Product::find($order_product->product_id)->name_ar); ?></td>
                                        <td><?php echo e($order_product->quantity); ?></td>
                                        <td><?php echo (is_numeric($order_product->text))?'<a href="'.asset(\App\SuggestedText::find($order_product->text)->text).'" target="_blank">النص</a>':'<a href="'.asset($order_product->text).'" target="_blank">النص</a>'; ?></td>
                                        <td><?php echo e($order_product->notes); ?></td>
                                        
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>