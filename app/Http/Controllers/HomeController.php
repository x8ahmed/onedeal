<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function checkout(Request $request){
        return view('checkout');
    }//end checkout
    public function user_checkout(Request $request){
        $order = new \App\Order();
        $order->user_id = \Auth::id();
        if ($request->shipping_type != 5){
            $order->shipping_type = $request->shipping_type;
        }else{
            $order->shipping_type = $request->shipping_cities;
        }
        $order->total_after_tax = $request->total_after_tax;
        $order->coupon = $request->coupon;
        $order->payment_method = $request->payment_method;
        $order->other_notes = $request->other_notes;
        $coupon = \App\Coupon::where('name',$request->coupon)->first();
        if ($coupon){
            //true
            if ($coupon->type == 'price'){
                $order->total_after_coupon = $request->total_after_tax-$coupon->value;
            }else if($coupon->type == 'percentage'){
                $order->total_after_coupon = $request->total_after_tax - (($coupon->value/100)*$request->total_after_tax);

            }
         }else{
            $order->total_after_coupon = $request->total_after_tax;
        }
        if ($request->hasFile('receipt')) {
            $picture_name = 'uploads/'.time().str_shuffle('abcdef').'.'.$request->file('receipt')->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($request->file('receipt'))->save(public_path("$picture_name"));
            $order->receipt = $picture_name;
        }//end if
        $order->status = 0;
        $order->save();
        if ($order->save()){
            //save order products
            for ($x=0;$x<count($request->product_id);$x++){
                $order_product = new \App\OrderProduct();
                $order_product->order_id = $order->id;
                $order_product->product_id = $request->product_id[$x];
                $order_product->quantity = $request->quantity[$x];
                $order_product->notes = $request->notes[$x];
                if($request->suggested_text[$x] != 'none'){
                    $order_product->text = $request->suggested_text[$x];
                }else{
                    if ($request->file('user_text')[$x]) {
                        $picture_name = 'uploads/'.time().str_shuffle('abcdef').'.'.$request->file('user_text')[$x]->getClientOriginalExtension();
                        \Intervention\Image\Facades\Image::make($request->file('user_text')[$x])->save(public_path("$picture_name"));
                        $order_product->text = $picture_name;
                    }//end if

                }
                if ($request->file('attachements')[$x]) {
                    $picture_name = 'uploads/'.time().str_shuffle('abcdef').'.'.$request->file('attachements')[$x]->getClientOriginalExtension();
                    \Intervention\Image\Facades\Image::make($request->file('attachements')[$x])->save(public_path("$picture_name"));
                    $order_product->attachements = $picture_name;
                }//end if
                $order_product->save();
            }//end for

        }
        Session::flash('message','تم اضافة طلبك بنجاح');
        return redirect()->route('index');


    }//end user_checkout
    public function my_orders(){
        $orders = \App\Order::where('user_id',\Auth::id())->get();
        return view('orders',['orders'=>$orders]);
    }
    public function update_profile(){
        return view('auth.update_profile');
    }//end update_profile
    public function update_user_profile(Request $request){

        $vaildation = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ($request->password == '')?['']:['required', 'string', 'min:6', 'confirmed'],
        ]);


        $user = \Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        if ($request->password != ''){
            $user->password = bcrypt($request->password);
        }
        $user->save();
        Session::flash('message',trans('index.profile_update_successfully'));
        return redirect()->back();

    }//end update_user_profile
}
