<?php

namespace App\Http\Controllers;

use App\AD;
use App\Blog;
use App\Category;
use App\Coupon;
use App\Feature;
use App\Product;
use App\Question;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function index(){
//        $ads = AD::all();
//        $products = Product::all();
        return view('welcome');
//        return view('welcome',['ads'=>$ads,'products'=>$products]);
    }//end index
    public function save_query(Request $request){
        $query = new Question();
        $query->name = $request->name;
        $query->email = $request->email;
        $query->phone = $request->phone;
        $query->message = $request->message;
        $query->save();
        return redirect()->back();
    }//end save_query
    public function about(){
        return view('hepta.about');
    }//end about
    public function projects(){
        return view('hepta.projects');
    }//end projects
    public function single_project($id){
        $project = Feature::findorfail($id);
        return view('hepta.single_project',['project'=>$project]);
    }//end single_project
    public function services(){
        return view('hepta.services');
    }//end services
    public function single_post($id){
        $post = Blog::findorfail($id);
        return view('hepta.single_post',['post'=>$post]);
    }//end single_post
    public function check_coupon(Request $request){
        $coupon = Coupon::where('name',$request->coupon)->first();
        if ($coupon){
            return response(['status'=>true,'data'=>$coupon]);
        }else{
            return response(['status'=>false]);

        }
    }//end check_coupon
    public function search(Request $request){
        $results = Product::where('name_ar','like','%'.$request->searchWord.'%')->orWhere('name_en','like','%'.$request->searchWord.'%')->get();
        return view('search',['products'=>$results]);
    }
    public function tag($tag){
        $cat = Category::where('name_ar',str_replace('-', ' ',$tag))->orWhere('name_en',str_replace('-', ' ',$tag))->first();
        if (isset($cat)&& $cat->parent_id == 0){
            //get childs
            $cats = Category::where('parent_id',$cat->id)->pluck('id')->toArray();
            if ($cat->cat_type =='product'){
                $products = Product::whereIn('category_id',$cats)->paginate(12);
                return view('category',['cat'=>$cat,'products'=>$products]);

            }elseif ($cat->cat_type =='text'){
                $texts = \App\SuggestedText::whereIn('category_id',$cats)->paginate(12);
                return view('category',['cat'=>$cat,'texts'=>$texts]);

            }
//            $ads = AD::all();
        }else{
            if ($cat->cat_type =='product') {
                $products = Product::where('category_id', $cat->id)->paginate(12);
                return view('category',['cat'=>$cat,'products'=>$products]);

            }elseif ($cat->cat_type =='text'){
                $texts = \App\SuggestedText::where('category_id', $cat->id)->paginate(12);
                return view('category',['cat'=>$cat,'texts'=>$texts]);

            }
//            $ads = AD::all();
//            return view('category',['cat'=>$cat,'products'=>$products,'texts'=>$texts]);
        }
    }//end tag
    public function single_product($id,$type=''){
        if ($type=='text'){
            $text = \App\SuggestedText::findorfail($id);
            return view('single_product',['text'=>$text]);
        }
        $product = Product::findorfail($id);
        return view('single_product',['product'=>$product]);
    }//end single_product
}
