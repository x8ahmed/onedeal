<?php

namespace App\Http\Controllers;

use App\AD;
use App\BillingAccount;
use App\Blog;
use App\Category;
use App\Client;
use App\Coupon;
use App\Feature;
use App\Page;
use App\Product;
use App\ProductPicture;
use App\Project_information;
use App\Question;
use App\Shipping;
use App\Site_setting;
use App\Slider;
use App\SuggestedText;
use App\Tax;
use App\Testmonial;
use Faker\Provider\Color;
use Hamcrest\Core\SetTest;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    public function dashboard(){
        return view('admin.dashboard');
    }//end dashboard
    //site_settings
    public function site_settings(){
        $settings = Site_setting::all();
        return view('admin.site_settings',['settings'=>$settings]);
    }//end site_settings
    public function save_site_settings(Request $request){
        $input = $request->except('_token');
        foreach($input as $key=>$value){
            $setting = Site_setting::where('name',$key)->first();
            if ($setting->input_type=='file'&&$request->hasFile($key)) {
                $picture_name = 'uploads/'.time().str_shuffle('abcdef').'.'.$request->file($key)->getClientOriginalExtension();
                \Intervention\Image\Facades\Image::make($request->file($key))->save(public_path("$picture_name"));
                $setting->value = $picture_name;
            }else{
                $setting->value = $input[$key];
            }
            $setting->save();
        }
//        $setting = Site_setting::where('')
        return redirect()->back();
    }//end save_site_settings

        #================================Slider==============================#
    public function indexSlider(){
        $sliders = Slider::paginate(5);
        return view('admin.slider.index',compact('sliders'));
    }

    public function createSlider(){
        return view('admin.slider.create');
    }

    public function storeSlider(Request $request){
        $request->validate([
           'image'=>'required',
           'sub_title'=>'required',
           'title'=>'required',
           'description'=>'required',
        ]);
        $add = new Slider();
        if ($request->image){
            Image::make($request->image)->save(public_path('uploads/sliders/'.$request->image->hashName()));
            $add->image = $request->image->hashName();
        }
        $add->sub_title = $request->sub_title;
        $add->title = $request->title;
        $add->description = $request->description;
        $add->save();
        session()->flash('success',__('تم الاضافة بنجاح'));
        return redirect()->route('slider');

    }
    public function editSlider(Slider $slider,$id){
        $slider = Slider::find($id);
        return view('admin.slider.edit',compact('slider'));
    }
    public function updateSlider(Request $request,$id){
        $request->validate([
            'sub_title'=>'required',
            'title'=>'required',
            'description'=>'required',
        ]);
        $add =Slider::find($id);
        if ($request->image){
            Image::make($request->image)->save(public_path('uploads/sliders/'.$request->image->hashName()));
            $add->image = $request->image->hashName();
        }
        $add->sub_title = $request->sub_title;
        $add->title = $request->title;
        $add->description = $request->description;
        $add->save();
        session()->flash('success',__('تم التعديل بنجاح'));
        return redirect()->route('slider');

    }
    public function destroySlider($id){
        $add =Slider::find($id);
        $add->delete();

        session()->flash('success',__('تم الحذف بنجاح'));
        return redirect()->route('slider');
    }

    #=====================================features============================#

    #================================features==============================#
    public function indexfeatures(){
        $features = Feature::paginate(5);
        return view('admin.features.index',compact('features'));
    }

    public function createfeatures(){
        return view('admin.features.create');
    }

    public function storefeatures(Request $request)
    {
        $request->validate([
            'image' => 'required',
            'type' => 'required',
            'head' => 'required',
            'description' => 'required',
        ]);
        $add = new Feature();
        if ($request->hasFile('image')) {
            Image::make($request->image)->save(public_path('uploads/featuers/' . $request->image->hashName()));
            $add->picture = $request->image->hashName();
        } elseif ($request->icon) {
            $add->picture = $request->icon;
        }
        $add->head = $request->head;
        $add->type = $request->type;
        $add->show = $request->show;
        $add->description = $request->description;
        $add->save();

         if ($request->client != null){
            $add = new Project_information();
            $add->client = $request->client;
            $add->completed_date = $request->completed_date;
            $add->contract_type = $request->contract_type;
            $add->project_location = $request->project_location;
            $add->save();

            }

        session()->flash('success', __('تم الاضافة بنجاح'));
        return redirect()->route('features');
    }

    public function editfeatures($id){
        $feature = Feature::find($id);
        $project = Project_information::find($id);
        return view('admin.features.edit',compact('feature','project'));
    }
    public function updatefeatures(Request $request,$id){
        $request->validate([
            'type' => 'required',
            'head' => 'required',
            'description' => 'required',
        ]);
        $add = Feature::find($id);
        if ($request->hasFile('image')) {
            Image::make($request->image)->save(public_path('uploads/featuers/' . $request->image->hashName()));
            $add->picture = $request->image->hashName();
        } elseif ($request->icon) {
            $add->picture = $request->icon;
        }
        $add->head = $request->head;
        $add->type = $request->type;
        $add->show = $request->show;
        $add->description = $request->description;
        $add->save();

        if ($request->client != null){
            $add =Project_information::find($id);
            $add->client = $request->client;
            $add->completed_date = $request->completed_date;
            $add->contract_type = $request->contract_type;
            $add->project_location = $request->project_location;
            $add->save();

        }

        session()->flash('success', __('تم التعديل بنجاح'));
        return redirect()->route('features');


    }
    public function destroyfeatures($id){
        $add =Feature::find($id);
        $add->delete();

        session()->flash('success',__('تم الحذف بنجاح'));
        return redirect()->route('features');
    }

    #================================clients==============================#
    public function indexclients(){
        $clients = Client::paginate(5);
        return view('admin.clients.index',compact('clients'));
    }

    public function createclients(){
        return view('admin.clients.create');
    }

    public function storeclients(Request $request){
        $request->validate([
            'name'=>'required',
            'logo'=>'required',

        ]);
        $add = new Client();
        if ($request->logo){
            Image::make($request->logo)->save(public_path('uploads/clients/'.$request->logo->hashName()));
            $add->logo = $request->logo->hashName();
        }
        $add->name = $request->name;

        $add->save();
        session()->flash('success',__('تم الاضافة بنجاح'));
        return redirect()->route('clients');

    }
    public function editclients(Slider $slider,$id){
        $client = Client::find($id);
        return view('admin.clients.edit',compact('client'));
    }
    public function updateclients(Request $request,$id){
        $request->validate([
            'name'=>'required',

        ]);
        $add =Client::find($id);
        if ($request->logo){
            Image::make($request->logo)->save(public_path('uploads/clients/'.$request->logo->hashName()));
            $add->logo = $request->logo->hashName();
        }
        $add->name = $request->name;

        $add->save();
        session()->flash('success',__('تم التعديل بنجاح'));
        return redirect()->route('clients');

    }
    public function destroyclients($id){
        $add =Client::find($id);
        $add->delete();

        session()->flash('success',__('تم الحذف بنجاح'));
        return redirect()->route('clients');
    }



    #================================testimonials==============================#
    public function indextestimonials(){
        $testimonials = Testmonial::paginate(5);
        return view('admin.testimonials.index',compact('testimonials'));
    }

    public function createtestimonials(){
        return view('admin.testimonials.create');
    }

    public function storetestimonials(Request $request){
        $request->validate([
            'name'=>'required',
            'picture'=>'required',
            'title'=>'required',

        ]);
        $add = new Testmonial();
        if ($request->picture){
            Image::make($request->picture)->save(public_path('uploads/testimonials/'.$request->picture->hashName()));
            $add->picture = $request->picture->hashName();
        }
        $add->name = $request->name;
        $add->title = $request->title;

        $add->save();
        session()->flash('success',__('تم الاضافة بنجاح'));
        return redirect()->route('testimonials');

    }
    public function edittestimonials(Slider $slider,$id){
        $testimonials = Testmonial::find($id);
        return view('admin.testimonials.edit',compact('testimonials'));
    }
    public function updatetestimonials(Request $request,$id){
        $request->validate([
            'name'=>'required',
            'title'=>'required',

        ]);
        $add =Testmonial::find($id);
        if ($request->picture){
            Image::make($request->picture)->save(public_path('uploads/testimonials/'.$request->picture->hashName()));
            $add->picture = $request->picture->hashName();
        }
        $add->name = $request->name;
        $add->title = $request->title;

        $add->save();
        session()->flash('success',__('تم التعديل بنجاح'));
        return redirect()->route('testimonials');

    }
    public function destroytestimonials($id){
        $add =Testmonial::find($id);
        $add->delete();

        session()->flash('success',__('تم الحذف بنجاح'));
        return redirect()->route('testimonials');
    }



    #================================questions==============================#
    public function indexquestions(){
        $questions = Question::paginate(5);
        return view('admin.question.index',compact('questions'));
    }

    public function createquestions(){
        return view('admin.question.create');
    }

    public function storequestions(Request $request){
        $request->validate([
            'name'=>'required',
            'picture'=>'required',
            'title'=>'required',
            'message'=>'required',

        ]);
        $add = new Question();
        if ($request->picture){
            Image::make($request->picture)->save(public_path('uploads/questions/'.$request->picture->hashName()));
            $add->picture = $request->picture->hashName();
        }
        $add->name = $request->name;
        $add->title = $request->title;
        $add->message = $request->message;

        $add->save();
        session()->flash('success',__('تم الاضافة بنجاح'));
        return redirect()->route('questions');

    }
    public function editquestions(Slider $slider,$id){
        $questions =Question::find($id);
        return view('admin.question.edit',compact('questions'));
    }
    public function updatequestions(Request $request,$id){
        $request->validate([
            'name'=>'required',
            'title'=>'required',
            'message'=>'required',

        ]);
        $add =Question::find($id);
        if ($request->picture){
            Image::make($request->picture)->save(public_path('uploads/questions/'.$request->picture->hashName()));
            $add->picture = $request->picture->hashName();
        }
        $add->name = $request->name;
        $add->title = $request->title;
        $add->message = $request->message;
        $add->save();
        session()->flash('success',__('تم التعديل بنجاح'));
        return redirect()->route('questions');

    }
    public function destroyquestions ($id){
        $add =Question::find($id);
        $add->delete();

        session()->flash('success',__('تم الحذف بنجاح'));
        return redirect()->route('questions');
    }




    #================================blogs==============================#
    public function indexblog(){
        $blogs = Blog::paginate(10);
        return view('admin.blog.index',compact('blogs'));
    }

    public function createblog(){
        return view('admin.blog.create');
    }

    public function storeblog(Request $request){
        $request->validate([
            'thumbnail'=>'required',
            'head'=>'required',
            'body'=>'required',
            'dateOfPublich'=>'required',

        ]);
        $add = new Blog();
        if ($request->thumbnail){
            Image::make($request->thumbnail)->save(public_path('uploads/blog/'.$request->thumbnail->hashName()));
            $add->thumbnail = $request->thumbnail->hashName();
        }
        $add->head = $request->head;
        $add->body = $request->body;
        $add->dateOfPublich = $request->dateOfPublich;

        $add->save();
        session()->flash('success',__('تم الاضافة بنجاح'));
        return redirect()->route('blog');

    }
    public function editblog(Slider $slider,$id){
        $blog =Blog::find($id);
        return view('admin.blog.edit',compact('blog'));
    }
    public function updateblog(Request $request,$id){
        $request->validate([
            'head'=>'required',
            'body'=>'required',
            'dateOfPublich'=>'required',

        ]);
        $add =Blog::find($id);
        if ($request->thumbnail){
            Image::make($request->thumbnail)->save(public_path('uploads/blog/'.$request->thumbnail->hashName()));
            $add->thumbnail = $request->thumbnail->hashName();
        }
        $add->head = $request->head;
        $add->body = $request->body;
        $add->dateOfPublich = $request->dateOfPublich;
        $add->save();
        session()->flash('success',__('تم التعديل بنجاح'));
        return redirect()->route('blog');

    }
    public function destroyblog ($id){
        $add =Blog::find($id);
        $add->delete();

        session()->flash('success',__('تم الحذف بنجاح'));
        return redirect()->route('blog');
    }

    #================================pages==============================#
    public function indexpages(){
        $pages = Page::paginate(5);
        return view('admin.page.index',compact('pages'));
    }

    public function createpages(){
        return view('admin.page.create');
    }

    public function storepages(Request $request){
        $request->validate([
            'name'=>'required',
            'content'=>'required',
        ]);
        $add = new Page();
        $add->name = $request->name;
        $add->content = $request->input('content');
        $add->save();
        session()->flash('success',__('تم الاضافة بنجاح'));
        return redirect()->route('pages');

    }
    public function editpages(Slider $slider,$id){
        $page =Page::find($id);
        return view('admin.page.edit',compact('page'));
    }
    public function updatepages(Request $request,$id){
        $request->validate([
            'name'=>'required',
            'content'=>'required',

        ]);
        $add =Page::find($id);
        $add->name = $request->name;
        $add->content = $request->input('content');
        $add->save();
        session()->flash('success',__('تم التعديل بنجاح'));
        return redirect()->route('pages');

    }
    public function destroypages($id){
        $add =Page::find($id);
        $add->delete();

        session()->flash('success',__('تم الحذف بنجاح'));
        return redirect()->route('pages');
    }
}
