<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testmonial extends Model
{
    protected $appends = ['image_path'];

    public function getImagePathAttribute(){

        return asset('uploads/testimonials/'.$this->picture);
    }
}
