<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $appends = ['image_path'];

    public function getImagePathAttribute(){

        return asset('uploads/featuers/'.$this->picture);
    }
}
