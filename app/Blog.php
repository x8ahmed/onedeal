<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $appends = ['image_path'];

    public function getImagePathAttribute(){

        return asset('uploads/blog/'.$this->thumbnail);
    }
}
