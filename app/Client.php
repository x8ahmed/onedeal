<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $appends = ['image_path'];

    public function getImagePathAttribute(){

        return asset('uploads/clients/'.$this->logo);
    }
}
