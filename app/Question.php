<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {

        return asset('uploads/questions/' . $this->picture);
    }
}
