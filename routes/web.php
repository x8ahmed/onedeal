<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//
//    return view('welcome');
//});


Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('admin')->middleware(['auth','CheckAdmin'])->group(function (){
    Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');


    //site_settings
    Route::get('/site_settings', 'AdminController@site_settings')->name('site_settings');
    Route::post('/save_site_settings', 'AdminController@save_site_settings')->name('save_site_settings');



    // slider area
    Route::get('/slider', 'AdminController@indexSlider')->name('slider');
    Route::get('/slider/create', 'AdminController@createSlider')->name('slider-create');
    Route::get('/slider/{id}/edit', 'AdminController@editSlider')->name('slider-edit');
    Route::post('/slider/store', 'AdminController@storeSlider')->name('slider-store');
    Route::put('/slider/{id}/edit', 'AdminController@updateSlider')->name('slider-update');
    Route::delete('/slider/{id}', 'AdminController@destroySlider')->name('slider-destroy');

    // features area

    Route::get('/features', 'AdminController@indexfeatures')->name('features');
    Route::get('/features/create', 'AdminController@createfeatures')->name('features-create');
    Route::get('/features/{id}/edit', 'AdminController@editfeatures')->name('features-edit');
    Route::post('/features/store', 'AdminController@storefeatures')->name('features-store');
    Route::put('/features/{id}/edit', 'AdminController@updatefeatures')->name('features-update');
    Route::delete('/features/{id}', 'AdminController@destroyfeatures')->name('features-destroy');


    // clients area

    Route::get('/clients', 'AdminController@indexclients')->name('clients');
    Route::get('/clients/create', 'AdminController@createclients')->name('clients-create');
    Route::get('/clients/{id}/edit', 'AdminController@editclients')->name('clients-edit');
    Route::post('/clients/store', 'AdminController@storeclients')->name('clients-store');
    Route::put('/clients/{id}/edit', 'AdminController@updateclients')->name('clients-update');
    Route::delete('/clients/{id}', 'AdminController@destroyclients')->name('clients-destroy');


    // testimonials area

    Route::get('/testimonials', 'AdminController@indextestimonials')->name('testimonials');
    Route::get('/testimonials/create', 'AdminController@createtestimonials')->name('testimonials-create');
    Route::get('/testimonials/{id}/edit', 'AdminController@edittestimonials')->name('testimonials-edit');
    Route::post('/testimonials/store', 'AdminController@storetestimonials')->name('testimonials-store');
    Route::put('/testimonials/{id}/edit', 'AdminController@updatetestimonials')->name('testimonials-update');
    Route::delete('/testimonials/{id}', 'AdminController@destroytestimonials')->name('testimonials-destroy');


    // questions area

    Route::get('/questions', 'AdminController@indexquestions')->name('questions');
    Route::get('/questions/create', 'AdminController@createquestions')->name('questions-create');
    Route::get('/questions/{id}/edit', 'AdminController@editquestions')->name('questions-edit');
    Route::post('/questions/store', 'AdminController@storequestions')->name('questions-store');
    Route::put('/questions/{id}/edit', 'AdminController@updatequestions')->name('questions-update');
    Route::delete('/questions/{id}', 'AdminController@destroyquestions')->name('questions-destroy');



    // blog area

    Route::get('/blog', 'AdminController@indexblog')->name('blog');
    Route::get('/blog/create', 'AdminController@createblog')->name('blog-create');
    Route::get('/blog/{id}/edit', 'AdminController@editblog')->name('blog-edit');
    Route::post('/blog/store', 'AdminController@storeblog')->name('blog-store');
    Route::put('/blog/{id}/edit', 'AdminController@updateblog')->name('blog-update');
    Route::delete('/blog/{id}', 'AdminController@destroyblog')->name('blog-destroy');


    // pages area

    Route::get('/pages', 'AdminController@indexpages')->name('pages');
    Route::get('/pages/create', 'AdminController@createpages')->name('pages-create');
    Route::get('/pages/{id}/edit', 'AdminController@editpages')->name('pages-edit');
    Route::post('/pages/store', 'AdminController@storepages')->name('pages-store');
    Route::put('/pages/{id}/edit', 'AdminController@updatepages')->name('pages-update');
    Route::delete('/pages/{id}', 'AdminController@destroypages')->name('pages-destroy');














    //ads
    Route::get('/ads', 'AdminController@ads')->name('ads');
    Route::get('/create_ad', 'AdminController@create_ad')->name('create_ad');
    Route::post('/save_ad', 'AdminController@save_ad')->name('save_ad');
    Route::delete('/delete/{id}', 'AdminController@deleteItem')->name('deleteItem');//delete
    //categories
    Route::get('/categories', 'AdminController@categories')->name('categories');
    Route::get('/create_category', 'AdminController@create_category')->name('create_category');
    Route::post('/save_category', 'AdminController@save_category')->name('save_category');
    Route::get('/update_category/{id}', 'AdminController@update_category')->name('update_category');
    Route::put('/store_update_category/{id}', 'AdminController@store_update_category')->name('store_update_category');
    //suggested_text
    Route::get('/suggested_text', 'AdminController@suggested_text')->name('suggested_text');
    Route::get('/create_text', 'AdminController@create_text')->name('create_text');
    Route::post('/save_text', 'AdminController@save_text')->name('save_text');
    //products
    Route::get('/products', 'AdminController@products')->name('products');
    Route::get('/create_product', 'AdminController@create_product')->name('create_product');
    Route::get('/update_product/{id}', 'AdminController@update_product')->name('update_product');
    Route::put('/save_update_product/{id}', 'AdminController@save_update_product')->name('save_update_product');
    Route::post('/save_product', 'AdminController@save_product')->name('save_product');
    Route::delete('/delete-product/{id}', 'AdminController@deleteProduct')->name('deleteProduct');//delete
    Route::get('/change_product_quantity/{id}/{quantity}', 'AdminController@change_product_quantity')->name('change_product_quantity');//delete
    //tax
    Route::get('/tax', 'AdminController@tax')->name('tax');
    Route::post('/save_tax', 'AdminController@save_tax')->name('save_tax');
    //coupons
    Route::get('/coupons', 'AdminController@coupons')->name('coupons');
    Route::get('/create_coupon', 'AdminController@create_coupon')->name('create_coupon');
    Route::post('/save_coupon', 'AdminController@save_coupon')->name('save_coupon');
    //shipping
    Route::get('/shipping', 'AdminController@shipping')->name('shipping');
    Route::get('/create_shipping', 'AdminController@create_shipping')->name('create_shipping');
    Route::post('/save_shipping', 'AdminController@save_shipping')->name('save_shipping');
    Route::get('/update_shipping/{id}', 'AdminController@update_shipping')->name('update_shipping');
    Route::put('/save_update_shipping/{id}', 'AdminController@save_update_shipping')->name('save_update_shipping');
    //billing_accounts
    Route::get('/billing_accounts', 'AdminController@billing_accounts')->name('billing_accounts');
    Route::get('/create_billing_account', 'AdminController@create_billing_account')->name('create_billing_account');
    Route::post('/save_billing_account', 'AdminController@save_billing_account')->name('save_billing_account');
    //orders
    Route::get('/orders', 'AdminController@orders')->name('orders');
    Route::get('/start_shipping/{id}/{status}', 'AdminController@start_shipping')->name('start_shipping');
    Route::get('/bill/{order_id}', 'AdminController@bill')->name('bill');
    //contacts
    Route::get('/contacts', 'AdminController@contacts')->name('contacts');
    Route::post('/save_contact', 'AdminController@save_contact')->name('save_contact');
    Route::post('/update_contact/{id}', 'AdminController@update_contact')->name('update_contact');







});

Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Auth::routes();

    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Route::get('/', 'GuestController@index')->name('index');
    Route::get('/about', 'GuestController@about')->name('about');
    Route::get('/projects', 'GuestController@projects')->name('projects');
    Route::get('/services', 'GuestController@services')->name('services');
    Route::get('/single_project/{id}', 'GuestController@single_project')->name('single_project');
    Route::get('/single_post/{id}', 'GuestController@single_post')->name('single_post');

    Route::get('/search', 'GuestController@search')->name('search');
    Route::get('/tags/{tag}', 'GuestController@tag')->name('tags');
    Route::get('/product/{id}/{type?}', 'GuestController@single_product')->name('single_product');



    Route::any('/checkout', 'HomeController@checkout')->name('checkout');
    Route::post('/user_checkout', 'HomeController@user_checkout')->name('user_checkout');
    Route::get('/my_orders', 'HomeController@my_orders')->name('my_orders');
    Route::get('/update_profile', 'HomeController@update_profile')->name('update_profile');
    Route::post('/update_user_profile', 'HomeController@update_user_profile')->name('update_user_profile');


});

Route::post('/save_query', 'GuestController@save_query')->name('save_query');

Route::get('/start', function () {
    return view('hepta.layouts.master');
//\Illuminate\Support\Facades\Artisan::call('config:cache');
//\Illuminate\Support\Facades\Artisan::call('make:migration',['name'=>'create_order_products_table']);
//\Illuminate\Support\Facades\Artisan::call('make:model',['name'=>'Contact']);
//\Illuminate\Support\Facades\Artisan::call('migrate');
//\Illuminate\Support\Facades\Artisan::call('db:seed');
    //return view('welcome');
});